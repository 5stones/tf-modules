## [6.7.1](https://gitlab.com/5stones/tf-modules/compare/v6.7.0...v6.7.1) (2024-09-18)


### Bug Fixes

* **ci:** fix incorrect variable name in ci module ([1e6a5c9](https://gitlab.com/5stones/tf-modules/commit/1e6a5c9))



# [6.7.0](https://gitlab.com/5stones/tf-modules/compare/v6.6.0...v6.7.0) (2024-09-18)



# [6.6.0](https://gitlab.com/5stones/tf-modules/compare/v6.5.2...v6.6.0) (2024-09-18)


### Features

* **ci:** add platform arguments to ci build to facility multi-platform builds and build machines ([e229e2a](https://gitlab.com/5stones/tf-modules/commit/e229e2a))



## [6.5.2](https://gitlab.com/5stones/tf-modules/compare/v6.5.1...v6.5.2) (2024-09-03)


### Bug Fixes

* **hetzner/rke2:** prevent recreation of agents on version updates ([73fc36d](https://gitlab.com/5stones/tf-modules/commit/73fc36d))



## [6.5.1](https://gitlab.com/5stones/tf-modules/compare/v6.5.0...v6.5.1) (2024-09-03)


### Bug Fixes

* **hetzner/rke2:** fix ipv6 lookup during boot by installing jq ([ba2026c](https://gitlab.com/5stones/tf-modules/commit/ba2026c))



# [6.5.0](https://gitlab.com/5stones/tf-modules/compare/v6.4.0...v6.5.0) (2024-08-13)


### Features

* **k8s/app:** add cert_cluster_issuer variable for cluster-wide issuers ([e576ea2](https://gitlab.com/5stones/tf-modules/commit/e576ea2))



# [6.4.0](https://gitlab.com/5stones/tf-modules/compare/v6.3.0...v6.4.0) (2024-08-09)


### Bug Fixes

* **hetzner/rke2:** fix hccm helm values ([dffe52a](https://gitlab.com/5stones/tf-modules/commit/dffe52a))
* **hetzner/rke2:** fix ips and networking ([ede0ecd](https://gitlab.com/5stones/tf-modules/commit/ede0ecd))


### Features

* **hetzner/rke2:** add agents and servers, and improve network situation ([49aa0d1](https://gitlab.com/5stones/tf-modules/commit/49aa0d1))
* **hetzner/rke2:** add hetzner resources for infrastructure and primary server ([3a580de](https://gitlab.com/5stones/tf-modules/commit/3a580de))
* **hetzner/rke2:** create module to launch an rke2 cluster in hetzner (wip) ([f751bc1](https://gitlab.com/5stones/tf-modules/commit/f751bc1))
* **k8s/app:** allow overriding securityContext.capabilities to support Baseline policy pods ([9a95464](https://gitlab.com/5stones/tf-modules/commit/9a95464))


### Performance Improvements

* **hetzner/rke2:** add cilium config with ipv4 native routing cidr ([ee60df5](https://gitlab.com/5stones/tf-modules/commit/ee60df5))



# [6.2.0](https://gitlab.com/5stones/tf-modules/compare/v6.1.1...v6.2.0) (2024-04-26)


### Features

* **k8s/app:** add variables for arch, node_selectors, tolerations, and storage ([d18f3fc](https://gitlab.com/5stones/tf-modules/commit/d18f3fc))



## [6.1.1](https://gitlab.com/5stones/tf-modules/compare/v6.1.0...v6.1.1) (2024-02-07)


### Bug Fixes

* **k8s/aws-ebs-csi-driver:** fix annotation for the default snapshot class ([36ba2f3](https://gitlab.com/5stones/tf-modules/commit/36ba2f3))



# [6.1.0](https://gitlab.com/5stones/tf-modules/compare/v6.0.3...v6.1.0) (2024-02-07)


### Features

* **k8s/aws-ebs-csi-driver:** add option to create storage and snapshot classes ([3169005](https://gitlab.com/5stones/tf-modules/commit/3169005))



## [6.0.3](https://gitlab.com/5stones/tf-modules/compare/v6.0.2...v6.0.3) (2024-02-06)


### Performance Improvements

* **k8s/app:** add 60s delay to liveness probe to prevent shutdowns during startup ([5fb5f43](https://gitlab.com/5stones/tf-modules/commit/5fb5f43))



## [6.0.2](https://gitlab.com/5stones/tf-modules/compare/v6.0.1...v6.0.2) (2024-02-06)


### Performance Improvements

* **aws/rke2:** update default ubuntu version to 22.04 with rke2 config ([73ec4f5](https://gitlab.com/5stones/tf-modules/commit/73ec4f5))



## [6.0.1](https://gitlab.com/5stones/tf-modules/compare/v6.0.0...v6.0.1) (2024-02-05)


### Bug Fixes

* **aws/rke2:** fix accessing ECR images after the in-tree cloud provider removal ([122ce74](https://gitlab.com/5stones/tf-modules/commit/122ce74))



# [6.0.0](https://gitlab.com/5stones/tf-modules/compare/v5.2.1...v6.0.0) (2024-01-11)


### Features

* **aws/rke2:** switch from in-tree cloud provider to external ([1d4f36e](https://gitlab.com/5stones/tf-modules/commit/1d4f36e))


### BREAKING CHANGES

* **aws/rke2:** "in-tree" is no longer an option for the cloud_provider variable. When upgrading,
the out of tree provider needs to be installed manually, but the manifest can be found in the
manifests folder and added to rke2 in /var/lib/rancher/rke2/server/manifests/.



## [5.2.1](https://gitlab.com/5stones/tf-modules/compare/v5.2.0...v5.2.1) (2023-09-27)


### Bug Fixes

* **aws/rke2:** fix params for additional agents ([7163b60](https://gitlab.com/5stones/tf-modules/commit/7163b60))



# [5.2.0](https://gitlab.com/5stones/tf-modules/compare/v5.1.0...v5.2.0) (2023-09-21)



# [5.1.0](https://gitlab.com/5stones/tf-modules/compare/v5.0.1...v5.1.0) (2023-09-01)


### Features

* **k8s/app:** add ability to schedule cron jobs ([e12cbe0](https://gitlab.com/5stones/tf-modules/commit/e12cbe0))



## [5.0.1](https://gitlab.com/5stones/tf-modules/compare/v5.0.0...v5.0.1) (2023-08-31)


### Bug Fixes

* **aws/lb/target-group:** fix error because of renamed lb_target_group_arn ([26aca8d](https://gitlab.com/5stones/tf-modules/commit/26aca8d))



# [5.0.0](https://gitlab.com/5stones/tf-modules/compare/v4.3.0...v5.0.0) (2023-08-30)


### Features

* **k8s/app:** harden security_context for the restricted Pod Security Standard ([07f95ef](https://gitlab.com/5stones/tf-modules/commit/07f95ef))


### BREAKING CHANGES

* **k8s/app:** This may not work on k8s 1.24 with security policies, because some polices prevent
setting certain fields. In other cases, this could add unexpected security restrictions, but can
still be overridden with the security_context variable.



# [4.3.0](https://gitlab.com/5stones/tf-modules/compare/v4.2.0...v4.3.0) (2023-08-01)


### Features

* **k8s/namespace:** add pod security labels and make the default network policy optional ([c0fa9d0](https://gitlab.com/5stones/tf-modules/commit/c0fa9d0))



# [4.2.0](https://gitlab.com/5stones/tf-modules/compare/v4.1.0...v4.2.0) (2023-06-01)


### Features

* **k8s/app:** add variable to run jobs before updating the deployment ([2bc1426](https://gitlab.com/5stones/tf-modules/commit/2bc1426))



# [4.1.0](https://gitlab.com/5stones/tf-modules/compare/v4.0.0...v4.1.0) (2023-03-28)


### Features

* **k8s/n8n:** add redis_ha option for high-availability ([1e810f8](https://gitlab.com/5stones/tf-modules/commit/1e810f8))
* **k8s/n8n/ingress:** define a k8s ingress with optional oauth2-proxy ([b3777cb](https://gitlab.com/5stones/tf-modules/commit/b3777cb))
* **k8s/redis:** switch to bitnami helm and support ha through sentinels and haproxy ([c7de884](https://gitlab.com/5stones/tf-modules/commit/c7de884))


### Performance Improvements

* **k8s/app,k8s/n8n:** remove role and role bindings ([bc894fb](https://gitlab.com/5stones/tf-modules/commit/bc894fb))


### BREAKING CHANGES

* **k8s/app,k8s/n8n:** If someone was using the k8s api to watch the secrets or configmaps they would need
to define a role and role binding.



## [3.0.1](https://gitlab.com/5stones/tf-modules/compare/v3.0.0...v3.0.1) (2023-03-01)


### Bug Fixes

* **k8s/keycloak:** fix start command for default entrypoint ([866560d](https://gitlab.com/5stones/tf-modules/commit/866560d))



# [3.0.0](https://gitlab.com/5stones/tf-modules/compare/v2.33.0...v3.0.0) (2023-02-28)


### Features

* **aws/ses/smtp-user:** create module to create smtp credentials for ses ([683b790](https://gitlab.com/5stones/tf-modules/commit/683b790))
* **k8s/keycloak:** update for Keycloak's switch to quarkus on version 17 ([17145e3](https://gitlab.com/5stones/tf-modules/commit/17145e3))


### BREAKING CHANGES

* **k8s/keycloak:** Keycloak versions before 17 are no longer supported.



## [2.32.1](https://gitlab.com/5stones/tf-modules/compare/v2.32.0...v2.32.1) (2023-01-24)


### Bug Fixes

* **aws/rke2:** prevent shrinking volumes that were manually resized ([f6bbedf](https://gitlab.com/5stones/tf-modules/commit/f6bbedf))



# [2.32.0](https://gitlab.com/5stones/tf-modules/compare/v2.31.0...v2.32.0) (2022-12-07)



# [2.31.0](https://gitlab.com/5stones/tf-modules/compare/v2.30.5...v2.31.0) (2022-12-06)


### Features

* **k8s/aws-ebs-csi-driver:** create module to install the aws-ebs-csi-driver ([c01eb47](https://gitlab.com/5stones/tf-modules/commit/c01eb47))



## [2.30.5](https://gitlab.com/5stones/tf-modules/compare/v2.30.4...v2.30.5) (2022-12-06)



## [2.30.4](https://gitlab.com/5stones/tf-modules/compare/v2.30.3...v2.30.4) (2022-12-06)


### Bug Fixes

* **aws/vpc/nat-instance:** fix persisting iptables rules between restarts ([d95a9ea](https://gitlab.com/5stones/tf-modules/commit/d95a9ea))


### Performance Improvements

* **aws/rke2:** update volume types for agents to gp3 ([dd80407](https://gitlab.com/5stones/tf-modules/commit/dd80407))



## [2.30.2](https://gitlab.com/5stones/tf-modules/compare/v2.30.1...v2.30.2) (2022-07-08)



## [2.30.1](https://gitlab.com/5stones/tf-modules/compare/v2.30.0...v2.30.1) (2022-04-12)


### Bug Fixes

* **aws/rke2:** fix autoscaler version and nginx service monitor ([2fe2d01](https://gitlab.com/5stones/tf-modules/commit/2fe2d01))



# [2.30.0](https://gitlab.com/5stones/tf-modules/compare/v2.29.4...v2.30.0) (2022-02-23)


### Features

* **k8s/app:** add ability to create a basic ingress ([75fc80d](https://gitlab.com/5stones/tf-modules/commit/75fc80d))



## [2.29.4](https://gitlab.com/5stones/tf-modules/compare/v2.29.3...v2.29.4) (2022-01-10)


### Bug Fixes

* **aws/vpc/nat-instance:** fix port forwarding rules ([14f18d4](https://gitlab.com/5stones/tf-modules/commit/14f18d4))



## [2.29.3](https://gitlab.com/5stones/tf-modules/compare/v2.29.2...v2.29.3) (2021-12-16)


### Performance Improvements

* **aws/rke2:** reserve resources for kubelet ([766fc36](https://gitlab.com/5stones/tf-modules/commit/766fc36))



## [2.29.2](https://gitlab.com/5stones/tf-modules/compare/v2.29.1...v2.29.2) (2021-12-15)


### Performance Improvements

* **aws/rke2:** reserve resources for system processes ([da5cfd6](https://gitlab.com/5stones/tf-modules/commit/da5cfd6))



## [2.29.1](https://gitlab.com/5stones/tf-modules/compare/v2.29.0...v2.29.1) (2021-12-13)


### Bug Fixes

* **k8s/app:** open network policy to allow ingresses to ClusterIP services ([f5b9ecd](https://gitlab.com/5stones/tf-modules/commit/f5b9ecd))



# [2.29.0](https://gitlab.com/5stones/tf-modules/compare/v2.28.0...v2.29.0) (2021-12-01)


### Features

* **aws/rke2:** add option for additional agent autoscaling groups ([408eb57](https://gitlab.com/5stones/tf-modules/commit/408eb57))



# [2.28.0](https://gitlab.com/5stones/tf-modules/compare/v2.27.0...v2.28.0) (2021-11-29)


### Features

* **aws/vpc/nat-instance:** add port forwarding option ([01629bc](https://gitlab.com/5stones/tf-modules/commit/01629bc))



# [2.27.0](https://gitlab.com/5stones/tf-modules/compare/v2.26.0...v2.27.0) (2021-11-17)


### Features

* **k8s:** add module to create letsencrypt issuers for cert-manager ([0417c98](https://gitlab.com/5stones/tf-modules/commit/0417c98))



# [2.26.0](https://gitlab.com/5stones/tf-modules/compare/v2.25.2...v2.26.0) (2021-11-12)


### Features

* **k8s/keycloak:** add variables for service_type and image_pull_secret ([7996c81](https://gitlab.com/5stones/tf-modules/commit/7996c81))



## [2.25.2](https://gitlab.com/5stones/tf-modules/compare/v2.25.1...v2.25.2) (2021-11-11)


### Bug Fixes

* **k8s/namespace:** fix labels ([8a1586a](https://gitlab.com/5stones/tf-modules/commit/8a1586a))



## [2.25.1](https://gitlab.com/5stones/tf-modules/compare/v2.25.0...v2.25.1) (2021-10-04)


### Features

* **k8s/ingress:** add a basic ingress module ([df0be8d](https://gitlab.com/5stones/tf-modules/commit/df0be8d))
* **k8s/namespace:** add variable for additional labels and namespaces outside of rancher ([a482fe3](https://gitlab.com/5stones/tf-modules/commit/a482fe3))
* **k8s/redis:** remove forced naming convention for redis modules ([13627db](https://gitlab.com/5stones/tf-modules/commit/13627db))



# [2.23.0](https://gitlab.com/5stones/tf-modules/compare/v2.22.0...v2.23.0) (2021-09-30)


### Features

* **k8s/redis:** abstract redis into a dedicated module ([7db977c](https://gitlab.com/5stones/tf-modules/commit/7db977c))



# [2.22.0](https://gitlab.com/5stones/tf-modules/compare/v2.21.2...v2.22.0) (2021-09-29)


### Features

* **k8s/keycloak:** add module to to deploy keycloak to k8s ([3ed1bb9](https://gitlab.com/5stones/tf-modules/commit/3ed1bb9))



## [2.21.2](https://gitlab.com/5stones/tf-modules/compare/v2.21.1...v2.21.2) (2021-09-15)



## [2.21.1](https://gitlab.com/5stones/tf-modules/compare/v2.21.0...v2.21.1) (2021-08-27)


### Bug Fixes

* **k8s/ecr-cred-helper:** fix permission to update registry secret ([c876213](https://gitlab.com/5stones/tf-modules/commit/c876213))



# [2.21.0](https://gitlab.com/5stones/tf-modules/compare/v2.20.1...v2.21.0) (2021-08-19)


### Features

* **k8s/app:** add variable for annotations to support fluentbit parsers ([0b3f4a5](https://gitlab.com/5stones/tf-modules/commit/0b3f4a5))



## [2.20.1](https://gitlab.com/5stones/tf-modules/compare/v2.20.0...v2.20.1) (2021-08-10)


### Bug Fixes

* **k8s/n8n/alb-ingress:** fix AAAA route53 record type ([4c077f6](https://gitlab.com/5stones/tf-modules/commit/4c077f6))



# [2.20.0](https://gitlab.com/5stones/tf-modules/compare/v2.19.0...v2.20.0) (2021-08-10)


### Features

* **k8s/n8n:** add topology_spread_constraints variable and default ([b1406d6](https://gitlab.com/5stones/tf-modules/commit/b1406d6))
* **k8s/n8n:** add variable to enable prometheus monitoring ([7f451d6](https://gitlab.com/5stones/tf-modules/commit/7f451d6))


### Performance Improvements

* **k8s/n8n:** adjust default resource limits based on production overload ([170396b](https://gitlab.com/5stones/tf-modules/commit/170396b))



# [2.19.0](https://gitlab.com/5stones/tf-modules/compare/v2.18.0...v2.19.0) (2021-08-09)


### Features

* **k8s/app:** add topology_spread_constraints variable with default spread by hostname ([bc8dc2c](https://gitlab.com/5stones/tf-modules/commit/bc8dc2c))



# [2.18.0](https://gitlab.com/5stones/tf-modules/compare/v2.17.1...v2.18.0) (2021-08-06)


### Features

* **k8s/app:** add service_labels and ignore some annotations ([2489115](https://gitlab.com/5stones/tf-modules/commit/2489115))



## [2.17.1](https://gitlab.com/5stones/tf-modules/compare/v2.17.0...v2.17.1) (2021-08-03)


### Bug Fixes

* **k8s/n8n/alb-ingress/outputs.tf:** fix aws_alb reference ([e688967](https://gitlab.com/5stones/tf-modules/commit/e688967))



# [2.17.0](https://gitlab.com/5stones/tf-modules/compare/v2.16.0...v2.17.0) (2021-08-03)


### Features

* **k8s/n8n/alb-ingress/outputs.tf:** add alb and target_group to outputs ([9d053c2](https://gitlab.com/5stones/tf-modules/commit/9d053c2))



# [2.16.0](https://gitlab.com/5stones/tf-modules/compare/v2.15.0...v2.16.0) (2021-07-22)


### Features

* **k8s/app:** add image_pull_secret variable to the service account ([ecb9dab](https://gitlab.com/5stones/tf-modules/commit/ecb9dab))
* **k8s/aws-access-key:** add module to give pods access to aws ([2b5e237](https://gitlab.com/5stones/tf-modules/commit/2b5e237))
* **k8s/ecr-cred-helper:** add module to access ecr outside of aws ([1aed60e](https://gitlab.com/5stones/tf-modules/commit/1aed60e))



# [2.15.0](https://gitlab.com/5stones/tf-modules/compare/v2.14.1...v2.15.0) (2021-06-22)


### Features

* **k8s/app:** add run_as variable to override the user and group ([c307a91](https://gitlab.com/5stones/tf-modules/commit/c307a91))



## [2.14.1](https://gitlab.com/5stones/tf-modules/compare/v2.14.0...v2.14.1) (2021-06-08)


### Bug Fixes

* **aws/rke2:** fix running version v1.20.7+rke2r1 and above on agents ([346050a](https://gitlab.com/5stones/tf-modules/commit/346050a))



# [2.14.0](https://gitlab.com/5stones/tf-modules/compare/v2.13.0...v2.14.0) (2021-05-28)


### Features

* **k8s/app:** create module to deploy a simple http app to k8s ([13136e6](https://gitlab.com/5stones/tf-modules/commit/13136e6))



# [2.13.0](https://gitlab.com/5stones/tf-modules/compare/v2.12.0...v2.13.0) (2021-05-24)


### Bug Fixes

* **aws/rke2:** fix version variable name ([20aaf9a](https://gitlab.com/5stones/tf-modules/commit/20aaf9a))


### Features

* **aws/rke2:** add variable for rke2 version ([209316c](https://gitlab.com/5stones/tf-modules/commit/209316c))



# [2.12.0](https://gitlab.com/5stones/tf-modules/compare/v2.11.5...v2.12.0) (2021-04-21)


### Features

* **k8s/n8n:** add module to deploy n8n to k8s ([f9a6b5e](https://gitlab.com/5stones/tf-modules/commit/f9a6b5e))



## [2.11.5](https://gitlab.com/5stones/tf-modules/compare/v2.11.4...v2.11.5) (2021-04-09)


### Performance Improvements

* **aws/rke2:** set vm.max_map_count for elasticsearch ([e09a9be](https://gitlab.com/5stones/tf-modules/commit/e09a9be))



## [2.11.4](https://gitlab.com/5stones/tf-modules/compare/v2.11.3...v2.11.4) (2021-04-06)


### Bug Fixes

* **k8s/elasticsearch-logging-cleaner:** use uid to fix security errors ([240409a](https://gitlab.com/5stones/tf-modules/commit/240409a))



## [2.11.3](https://gitlab.com/5stones/tf-modules/compare/v2.11.2...v2.11.3) (2021-04-05)


### Bug Fixes

* **aws/rke2:** ignore changes to source_dest_check ([dd02907](https://gitlab.com/5stones/tf-modules/commit/dd02907))



## [2.11.2](https://gitlab.com/5stones/tf-modules/compare/v2.11.1...v2.11.2) (2021-04-05)


### Bug Fixes

* **k8s/elasticsearch-logging-cleaner:** add service account ([4881174](https://gitlab.com/5stones/tf-modules/commit/4881174))



## [2.11.1](https://gitlab.com/5stones/tf-modules/compare/v2.11.0...v2.11.1) (2021-03-30)


### Bug Fixes

* **aws:** ignore target group additions to k8s asgs ([00ee1c0](https://gitlab.com/5stones/tf-modules/commit/00ee1c0))



# [2.11.0](https://gitlab.com/5stones/tf-modules/compare/v2.10.1...v2.11.0) (2021-03-29)


### Features

* **aws/rke2:** create module to deploy rke2 to aws ([27d2b19](https://gitlab.com/5stones/tf-modules/commit/27d2b19))



## [2.10.1](https://gitlab.com/5stones/tf-modules/compare/v2.10.0...v2.10.1) (2021-03-29)


### Bug Fixes

* **aws/k3s,aws/microk8s:** add prometheus node port ([09a1949](https://gitlab.com/5stones/tf-modules/commit/09a1949))



# [2.10.0](https://gitlab.com/5stones/tf-modules/compare/v2.9.1...v2.10.0) (2021-03-19)


### Features

* **aws/microk8s:** add variable to disable ha (and use etcd) ([fcd9dbd](https://gitlab.com/5stones/tf-modules/commit/fcd9dbd))



## [2.9.1](https://gitlab.com/5stones/tf-modules/compare/v2.9.0...v2.9.1) (2021-03-19)


### Bug Fixes

* **aws/k3s:** fix join command for agents and fix cloud-provider config ([14e7367](https://gitlab.com/5stones/tf-modules/commit/14e7367))



# [2.9.0](https://gitlab.com/5stones/tf-modules/compare/v2.8.1...v2.9.0) (2021-03-16)


### Features

* **aws/k3s:** add module for launching a k3s cluster in aws ([329400e](https://gitlab.com/5stones/tf-modules/commit/329400e))



## [2.8.1](https://gitlab.com/5stones/tf-modules/compare/v2.8.0...v2.8.1) (2021-03-15)


### Bug Fixes

* **aws/vpc/nat-instance:** ignore changes to prevent replacing unnecessarily ([82560a0](https://gitlab.com/5stones/tf-modules/commit/82560a0))



# [2.8.0](https://gitlab.com/5stones/tf-modules/compare/v2.7.1...v2.8.0) (2021-01-25)


### Features

* **aws/vpc/nat-instance:** update nat to use Ubuntu or Amazon Linux 2 ([f5305b9](https://gitlab.com/5stones/tf-modules/commit/f5305b9))



## [2.7.1](https://gitlab.com/5stones/tf-modules/compare/v2.7.0...v2.7.1) (2020-12-17)


### Bug Fixes

* **aws/microk8s:** add hook to leave cluster on shutdown and remove unnecessary code ([22fb076](https://gitlab.com/5stones/tf-modules/commit/22fb076))



# [2.7.0](https://gitlab.com/5stones/tf-modules/compare/v2.6.0...v2.7.0) (2020-12-04)


### Features

* **aws/lb/target-group:** ability to add path_pattern condition ([8af01fc](https://gitlab.com/5stones/tf-modules/commit/8af01fc))



# [2.6.0](https://gitlab.com/5stones/tf-modules/compare/v2.5.3...v2.6.0) (2020-11-19)


### Features

* **k8s/elasticsearch-logging-cleaner:** create module to cleanup old elasticsearch logs ([b837cdd](https://gitlab.com/5stones/tf-modules/commit/b837cdd))



## [2.5.3](https://gitlab.com/5stones/tf-modules/compare/v2.5.2...v2.5.3) (2020-11-11)


### Bug Fixes

* **aws/microk8s:** fix networking and dns across nodes ([9ca9cbc](https://gitlab.com/5stones/tf-modules/commit/9ca9cbc))



## [2.5.2](https://gitlab.com/5stones/tf-modules/compare/v2.5.1...v2.5.2) (2020-11-05)


### Bug Fixes

* **aws/microk8s:** fix permissions for autoscaling ([ae98593](https://gitlab.com/5stones/tf-modules/commit/ae98593))



## [2.5.1](https://gitlab.com/5stones/tf-modules/compare/v2.5.0...v2.5.1) (2020-11-05)


### Bug Fixes

* **aws/microk8s:** fix dns resolution for private hosted zones ([05b16e1](https://gitlab.com/5stones/tf-modules/commit/05b16e1))



# [2.5.0](https://gitlab.com/5stones/tf-modules/compare/v2.4.1...v2.5.0) (2020-10-20)


### Features

* **aws/microk8s:** configure for aws cloud provider ([8296df4](https://gitlab.com/5stones/tf-modules/commit/8296df4))



## [2.4.1](https://gitlab.com/5stones/tf-modules/compare/v2.4.0...v2.4.1) (2020-10-19)


### Bug Fixes

* **aws/microk8s:** implement workarounds to allow for addons like ebs volumes ([c8928e8](https://gitlab.com/5stones/tf-modules/commit/c8928e8))



# [2.4.0](https://gitlab.com/5stones/tf-modules/compare/v2.3.3...v2.4.0) (2020-10-15)


### Features

* **aws/microk8s:** create module to launch a MicroK8s cluster in AWS ([4468a6f](https://gitlab.com/5stones/tf-modules/commit/4468a6f))



## [2.3.3](https://gitlab.com/5stones/tf-modules/compare/v2.3.2...v2.3.3) (2020-09-30)


### Bug Fixes

* **build.sh:** try get-login-password before get-login ([c3320e0](https://gitlab.com/5stones/tf-modules/commit/c3320e0))



## [2.3.2](https://gitlab.com/5stones/tf-modules/compare/v2.3.1...v2.3.2) (2020-07-31)


### Bug Fixes

* **target-group:** fix listener rule condition ([40eab79](https://gitlab.com/5stones/tf-modules/commit/40eab79))



## [2.3.1](https://gitlab.com/5stones/tf-modules/compare/v2.3.0...v2.3.1) (2020-07-16)


### Bug Fixes

* **vpc:** fix using regions other than the default ([f994d43](https://gitlab.com/5stones/tf-modules/commit/f994d43))



# [2.3.0](https://gitlab.com/5stones/tf-modules/compare/v2.2.0...v2.3.0) (2020-07-14)


### Bug Fixes

* **launch-template:** stop recreate when spot is false ([16bb819](https://gitlab.com/5stones/tf-modules/commit/16bb819))


### Features

* **business-hours:** create module to shutdown an autoscaling group during non-business hours ([f5d582c](https://gitlab.com/5stones/tf-modules/commit/f5d582c))
* **vpc:** create modules for a public/private vpc and nat ([6764e55](https://gitlab.com/5stones/tf-modules/commit/6764e55))



## [2.1.1](https://gitlab.com/5stones/tf-modules/compare/v2.1.0...v2.1.1) (2019-10-04)


### Bug Fixes

* **docker-image:** fix issue with incorrect order in if conditional ([2b72fbe](https://gitlab.com/5stones/tf-modules/commit/2b72fbe))



# [2.1.0](https://gitlab.com/5stones/tf-modules/compare/v2.0.3...v2.1.0) (2019-10-03)


### Bug Fixes

* **ci/docker-image:** fix path for docker build ([9ee6442](https://gitlab.com/5stones/tf-modules/commit/9ee6442))
* **launch-template:** fix yaml syntax for multiple ssh keys ([4a1d3f2](https://gitlab.com/5stones/tf-modules/commit/4a1d3f2))


### Features

* **ci:** Add the ability to optionally target a specific stage in the dockerfile ([65073f4](https://gitlab.com/5stones/tf-modules/commit/65073f4))



## [2.0.1](https://gitlab.com/5stones/tf-modules/compare/v2.0.0...v2.0.1) (2019-06-28)


### Bug Fixes

* **target-group:** fix health_check fields ([0eac061](https://gitlab.com/5stones/tf-modules/commit/0eac061))
* **target-group:** fix health_check syntax ([6bf7fd0](https://gitlab.com/5stones/tf-modules/commit/6bf7fd0))
* fix automatically upgraded files for 0.12 ([edcb2b4](https://gitlab.com/5stones/tf-modules/commit/edcb2b4))
* **ci:** fix docker_path variable ([a611fcb](https://gitlab.com/5stones/tf-modules/commit/a611fcb))
* **ci/docker-image:** delay creation of resources that depend on a pushed image ([15d693f](https://gitlab.com/5stones/tf-modules/commit/15d693f))
* **ci/main.tf:** Update docker-image path for terraform v0.12.2 ([b88bed2](https://gitlab.com/5stones/tf-modules/commit/b88bed2))
* **ecs/task-execution-role:** fix role for a list of resources ([597a125](https://gitlab.com/5stones/tf-modules/commit/597a125))
* **target-group:** allow target groups for root domain name ([21d7d2f](https://gitlab.com/5stones/tf-modules/commit/21d7d2f))


### chore

* run `terraform 0.12upgrade` on each module ([c625511](https://gitlab.com/5stones/tf-modules/commit/c625511))


### Code Refactoring

* **bin/deploy:** remove deploy script ([f579dbd](https://gitlab.com/5stones/tf-modules/commit/f579dbd))


### Features

* **ci:** create module to deploy docker projects ([f506e2d](https://gitlab.com/5stones/tf-modules/commit/f506e2d))
* **ecs/autoscaling:** create module for autoscaling services ([fc9012e](https://gitlab.com/5stones/tf-modules/commit/fc9012e))
* **target-group:** allow customization of health check ([0351b9f](https://gitlab.com/5stones/tf-modules/commit/0351b9f))


### Performance Improvements

* **ecs/launch-template:** reserve memory for system processes and update pull behavior ([0ffd2ea](https://gitlab.com/5stones/tf-modules/commit/0ffd2ea))


### BREAKING CHANGES

* **bin/deploy:** Deploys will need to switch to using only `terraform apply` and ci module
* terraform 0.12 is a major release and files are not compatible with 0.11



# [1.0.0](https://gitlab.com/5stones/tf-modules/compare/1bceda0...v1.0.0) (2019-02-21)


### Bug Fixes

* **deploy:** fix docker image tag ([26f2c9d](https://gitlab.com/5stones/tf-modules/commit/26f2c9d))


### Features

* **aws:** add modules for target groups and task execution roles ([1bceda0](https://gitlab.com/5stones/tf-modules/commit/1bceda0))
* **aws:** create launch template with efs mount ([601415a](https://gitlab.com/5stones/tf-modules/commit/601415a))
* **deploy:** create deploy script for terraform projects ([204350b](https://gitlab.com/5stones/tf-modules/commit/204350b))
* **efs-provisioner:** add modules for installing the efs-provisioner and volume claims ([c276067](https://gitlab.com/5stones/tf-modules/commit/c276067))



