data "external" "git" {
  program = ["${path.module}/git.sh"]
}

module "docker_image" {
  source   = "./docker-image"
  path     = var.docker_path
  repo     = var.docker_repo
  target   = var.docker_target
  platform = var.docker_platform
  tags = (
    var.version_tags == "all" ? data.external.git.result.docker_tags
    : var.version_tags == "simple" ? trimspace("${data.external.git.result.exact_semver} ${data.external.git.result.dev_tags}")
    : data.external.git.result.dev_tags
  )
}
