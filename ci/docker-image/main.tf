locals {
  tag_list = split(" ", trimspace(var.tags))
  image    = "${var.repo}:${local.tag_list[0]}"
}

resource "null_resource" "image" {
  count = var.repo == "" ? 0 : 1

  triggers = {
    path     = var.path
    repo     = var.repo
    tags     = var.tags
    platform = var.platform
    target   = var.target
  }

  provisioner "local-exec" {
    command = "${path.module}/build.sh '${var.path}'"
    environment = {
      REPO     = var.repo
      TAGS     = var.tags
      PLATFORM = var.platform
      TARGET   = var.target
    }
  }
}
