output "image" {
  description = "The pushed image, using the first tag given"
  value       = var.repo == "" ? "" : local.image
  depends_on  = [null_resource.image]
}

output "tag" {
  description = "The pushed image, using the first tag given"
  value       = var.repo == "" ? "" : local.tag_list[0]
  depends_on  = [null_resource.image]
}
