variable "repo" {
  description = "Set to build and push a docker image"
  default     = ""
}

variable "path" {
  description = "The path for the docker build"
  default     = "../.."
}

variable "platform" {
  description = "A comma sparated list of docker platforms to target (ex. 'linux/amd64,linux/arm64')"
  default     = "linux/amd64"
}

variable "tags" {
  description = "A space sparated list of docker tags to build and push"
  default     = "latest"
}

variable "target" {
  description = "A string corresponding with the name of the stage you would like the build to target"
  default     = ""
}
