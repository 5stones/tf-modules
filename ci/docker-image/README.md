# Builds and pushes a docker image at a given path.

Uses a bash script and `docker` to build a container image and push it to a repository, so it can be deployed.
If the image is in AWS ECR, it will login with the aws cli before pushing images.

This may be able to be replaced by [kreuzwerker/docker](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs/resources/registry_image),
which is referenced in official docs.

```tf
module "ci" {
  source = "git::https://gitlab.com/5stones/tf-modules//ci/docker-image"

  # what image to build and push
  repo = "example/image"

  # a space sparated list of docker tags to build and push
  #tags = "latest"

  # where to build the image
  #path = "../.."

  # a comma sparated list of docker platforms to target (ex. 'linux/amd64,linux/arm64')
  #platform = "linux/amd64"

  # a string corresponding with the name of the stage you would like the build to target
  #target = ""
}
```
