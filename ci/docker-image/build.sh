#!/bin/bash

# exit on any error
set -e

echo "Building docker image $REPO"
# if a target is given then build that specific target, else do an untargeted build
if [[ -z "${TARGET}" ]]; then
  docker build \
    --platform $PLATFORM \
    --pull `printf " -t $REPO:%s" $TAGS` "$1"
else
  docker build \
    --platform $PLATFORM \
    --pull --target $TARGET `printf " -t $REPO:%s" $TAGS` "$1"
fi

# login when using aws ecr
if [[ "$REPO" == *.ecr.*.amazonaws.com/* ]]; then
  echo "\nLogging in to AWS docker"
  region=`echo "$REPO" | grep 'ecr.[^.]*.amazonaws.com' -o | awk -F. '{print $2}'`

  if aws --version | grep 'aws-cli/2.*'; then
    aws --region  $region ecr get-login-password | docker login --username AWS --password-stdin $REPO
  else
    `aws ecr --region $region get-login | sed 's/-e none//'`
  fi

fi

# push docker tags
for t in $TAGS; do
  echo "\nPushing docker tag: $t"
  docker push "$REPO:$t"
done
