variable "docker_repo" {
  description = "Set to build and push a docker image"
  default     = ""
}

variable "docker_path" {
  description = "The path for the docker build"
  default     = "../.."
}

variable "docker_target" {
  description = "The target stage for the docker build"
  default     = ""
}

variable "docker_platform" {
  description = "A comma sparated list of docker platforms to target (ex. 'linux/amd64,linux/arm64')"
  default     = "linux/amd64"
}

variable "version_tags" {
  description = "Include versions from the git tag for the image tags (all, simple, none)"
  default     = "all"
}
