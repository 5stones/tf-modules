#!/bin/bash
# read git to generate version info for terraform

# pull out semver tags without v
SEMVER_REGEX='\bv?((([0-9]+)\.([0-9]+)\.([0-9]+)(-([0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*))?)(\+([0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*))?)\b'
exact_semver=`git describe --no-match --exact-match --abbrev=0 --match '*[0-9]*.[0-9]*.[0-9]*' 2>/dev/null | egrep -o "$SEMVER_REGEX" | sed 's/^v//'`
semver=`git describe --no-match --abbrev=0 --match '*[0-9]*.[0-9]*.[0-9]*' 2>/dev/null | egrep -o "$SEMVER_REGEX" | sed 's/^v//'`

sha=`git rev-parse HEAD`
build_time=`date -u +"%Y%m%dT%H%M%SZ"`

if [ -n "$sha" ]; then
  version="$sha"
else
  version="$build_time"
fi
dev_tags="$version latest"


if [ -n "$exact_semver" ]; then
  if echo "$exact_semver" | grep '\-' > /dev/null; then
    # prerelease
    versions="$exact_semver"
  else
    # normal release
    versions="$exact_semver $(echo "$exact_semver" | awk -F. '{ print $1"."$2" "$1 }')"
  fi
  version="$exact_semver"
  docker_tags="$versions $dev_tags"
else
  versions="$version"
  docker_tags="$dev_tags"
fi

# output json for terraform external data
cat << EOF
{
  "build_time": "$build_time",
  "sha": "$sha",
  "exact_semver": "$exact_semver",
  "semver": "$semver",
  "version": "$version",
  "versions": "$versions",
  "docker_tags": "$docker_tags",
  "dev_tags": "$dev_tags"
}
EOF
