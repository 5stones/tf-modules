output "version" {
  description = "A version tag for the build (a semver, sha, or build time)"
  value       = data.external.git.result.version
}

output "sha" {
  description = "The sha of the last git commit"
  value       = data.external.git.result.sha
}

output "git_info" {
  description = "Info on the git state (sha, semver, exact_semver)"
  value       = data.external.git.result
}

output "docker_image" {
  description = "The pushed docker image for a build (if any)"
  value       = module.docker_image.image
}

output "docker_tag" {
  description = "The primary tag for the built docker image (sha or build time, if any)"
  value       = module.docker_image.tag
}
