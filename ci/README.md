# Determines a version from git, and can build and push a container image

## Get version info from git

```tf
module "ci" {
  source = "git::https://gitlab.com/5stones/tf-modules//ci"
}
```

### Example outputs

* `sha`: current commit
    * `82f7615bee7f3f3000a98af084330dfc23cb494a`
* `version`: semver or commit sha with a fallback to build time
    * `7.0.1`
    * `82f7615bee7f3f3000a98af084330dfc23cb494a`
    * `20250107T223154Z`
* `git_info`: object containing version and commit info
    * ```json
      {
        "build_time": "20250108T153119Z",
        "sha": "82f7615bee7f3f3000a98af084330dfc23cb494a",
        "exact_semver": "7.0.1",
        "semver": "7.0.1",
        "version": "7.0.1",
        "versions": "7.0.1 7.0 7",
        "docker_tags": "7.0.1 7.0 7 82f7615bee7f3f3000a98af084330dfc23cb494a latest",
        "dev_tags": "82f7615bee7f3f3000a98af084330dfc23cb494a latest"
      }
      ```


## Build and push a docker image based on git commit info

```tf
module "ci" {
  source      = "git::https://gitlab.com/5stones/tf-modules//ci"
  docker_repo = "example/image"

  # tag with major, minor, and exact semver
  #version_tags = "full"
  # tag with exact semver
  #version_tags = "simple"
  # no version tags (only sha and "latest")
  #version_tags = "none"
}
```

### Example outputs

* `docker_tag`: the tag to reference the built image
    * `7.0.1`
    * `82f7615bee7f3f3000a98af084330dfc23cb494a`
* `docker_image`: the docker repo and image tag to reference the built image
    * `example/image:7.0.1`
    * `example/image:82f7615bee7f3f3000a98af084330dfc23cb494a`
