output "vpc_id" {
  value = local.vpc_id
}

output "alb_arn" {
  value = data.aws_alb.selected.arn
}

output "fqdn" {
  value = aws_route53_record.a.name
}

output "arn" {
  value = aws_alb_target_group.main.arn
}

