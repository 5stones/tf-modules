# lookup existing resources in aws
data "aws_alb" "selected" {
  name = var.alb
}

data "aws_alb_listener" "selected" {
  load_balancer_arn = data.aws_alb.selected.arn
  port              = 443
}

data "aws_route53_zone" "selected" {
  name = var.zone
}

locals {
  vpc_id = data.aws_alb.selected.vpc_id # get vpc_id from alb
  fqdn   = "${var.hostname}${var.hostname == "" ? "" : "."}${var.zone}"
}

# define the target group for the service
resource "aws_alb_target_group" "main" {
  name                 = var.name
  port                 = var.port
  protocol             = "HTTP"
  vpc_id               = local.vpc_id
  deregistration_delay = var.deregistration_delay
  health_check {
    enabled             = lookup(var.health_check, "enabled", null)
    healthy_threshold   = lookup(var.health_check, "healthy_threshold", null)
    interval            = lookup(var.health_check, "interval", null)
    matcher             = lookup(var.health_check, "matcher", null)
    path                = lookup(var.health_check, "path", null)
    port                = lookup(var.health_check, "port", null)
    protocol            = lookup(var.health_check, "protocol", null)
    timeout             = lookup(var.health_check, "timeout", null)
    unhealthy_threshold = lookup(var.health_check, "unhealthy_threshold", null)
  }
}

# define a https listener rule
resource "aws_alb_listener_rule" "main" {
  listener_arn = data.aws_alb_listener.selected.arn

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.main.arn
  }

  condition {
    host_header {
      values = [local.fqdn]
    }
  }

  dynamic "condition" {
    for_each = var.path_patterns != [] ? ["path_pattern"] : []
    content {
      path_pattern {
        values = var.path_patterns
      }
    }
  }
}

# define the dns A record (ipv4)
resource "aws_route53_record" "a" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = local.fqdn
  type    = "A"

  alias {
    name                   = data.aws_alb.selected.dns_name
    zone_id                = data.aws_alb.selected.zone_id
    evaluate_target_health = false
  }
}

# define the dns AAAA record for ipv6 if dualstack
resource "aws_route53_record" "aaaa" {
  #count = "${data.aws_alb.selected.ip_address_type == "dualstack" ? 1 : 0}"  # this property is missing
  count = var.ipv6 ? 1 : 0

  zone_id = data.aws_route53_zone.selected.zone_id
  name    = local.fqdn
  type    = "AAAA"

  alias {
    name                   = data.aws_alb.selected.dns_name
    zone_id                = data.aws_alb.selected.zone_id
    evaluate_target_health = false
  }
}

# Create a the autoscaling target group attachment (if set)
resource "aws_autoscaling_attachment" "main" {
  count                  = var.autoscaling_group_name != "" ? 1 : 0
  autoscaling_group_name = var.autoscaling_group_name
  lb_target_group_arn    = aws_alb_target_group.main.arn
}

