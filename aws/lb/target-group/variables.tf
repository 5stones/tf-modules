variable "name" {
  description = "Name of the new target group"
}

variable "alb" {
  description = "Name of the alb to add to"
}

variable "hostname" {
  description = "DNS entry to add (without zone)"
}

variable "zone" {
  description = "Name of the dns zone to modify"
}

# optional variables

variable "health_check" {
  default = {}
}

variable "port" {
  default = "80"
}

variable "deregistration_delay" {
  default = "60"
}

variable "ipv6" {
  default = true
}

variable "autoscaling_group_name" {
  default     = ""
  description = "If set, will attach an autoscaling group"
}

variable "path_patterns" {
  default     = []
  description = "If not empty, will add a path_pattern condition and only handle request to those paths"
}
