locals {
  vpc_security_group_ids = concat([aws_security_group.main.id], var.vpc_security_group_ids)

  ipv6_cidr_blocks = data.aws_vpc.main.ipv6_cidr_block == null ? [] : [data.aws_vpc.main.ipv6_cidr_block]
}

resource "aws_security_group" "main" {
  name   = var.name
  vpc_id = data.aws_vpc.main.id

  # Allow external traffic if ingress is enabled with external IPs
  ingress {
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = var.http_from.cidr_blocks
    ipv6_cidr_blocks = var.http_from.ipv6_cidr_blocks
    description      = "http"
  }
  ingress {
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = var.http_from.cidr_blocks
    ipv6_cidr_blocks = var.http_from.ipv6_cidr_blocks
    description      = "https"
  }


  # VPC Traffic to NodePorts (AWS Load Balancers)
  ingress {
    protocol         = "tcp"
    from_port        = 30000
    to_port          = 32767
    cidr_blocks      = [data.aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = local.ipv6_cidr_blocks
    description      = "NodePort Services"
  }


  # Management Traffic into the cluster
  ingress {
    protocol         = "tcp"
    from_port        = 16443
    to_port          = 16443
    cidr_blocks      = concat([data.aws_vpc.main.cidr_block], var.manage_from.cidr_blocks)
    ipv6_cidr_blocks = concat(local.ipv6_cidr_blocks, var.manage_from.ipv6_cidr_blocks)
    description      = "API server"
  }
  ingress {
    protocol         = "tcp"
    from_port        = 22
    to_port          = 22
    cidr_blocks      = concat([data.aws_vpc.main.cidr_block], var.ssh_from.cidr_blocks)
    ipv6_cidr_blocks = concat(local.ipv6_cidr_blocks, var.ssh_from.ipv6_cidr_blocks)
    description      = "SSH"
  }


  # Traffic inside the cluster #

  # Control plane
  ingress {
    protocol    = "tcp"
    from_port   = 10257
    to_port     = 10257
    self        = true
    description = "kube-controller"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 10259
    to_port     = 10259
    self        = true
    description = "kube-scheduler"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 12379
    to_port     = 12379
    self        = true
    description = "etcd"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 19001
    to_port     = 19001
    self        = true
    description = "dqlite"
  }

  # Nodes
  ingress {
    protocol    = "tcp"
    from_port   = 10250
    to_port     = 10250
    self        = true
    description = "kubelet"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 10255
    to_port     = 10255
    self        = true
    description = "kubelet-readonly"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 9796
    to_port     = 9796
    self        = true
    description = "Prometheus metrics"
  }

  # Networking
  ingress {
    protocol    = "udp"
    from_port   = 4789
    to_port     = 4789
    self        = true
    description = "Calico networking with VXLAN"
  }
  ingress {
    protocol    = "udp"
    from_port   = 8472
    to_port     = 8472
    self        = true
    description = "Flannel networking with VXLAN"
  }

  # Clustering
  ingress {
    protocol    = "tcp"
    from_port   = 25000
    to_port     = 25000
    self        = true
    description = "cluster-agent for microk8s clustering"
  }


  # Egress #

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = local.tags
}
