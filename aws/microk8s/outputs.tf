output "aws_autoscaling_group" {
  value = aws_autoscaling_group.main
}

output "aws_launch_template" {
  value = aws_launch_template.main
}

output "aws_iam_role" {
  value = aws_iam_role.main
}
