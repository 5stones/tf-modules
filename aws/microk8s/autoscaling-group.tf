
resource "aws_autoscaling_group" "main" {
  name     = var.name
  min_size = var.autoscaling_range[0]
  max_size = var.autoscaling_range[1]

  vpc_zone_identifier = var.subnets

  enabled_metrics = var.enabled_metrics

  mixed_instances_policy {
    instances_distribution {
      on_demand_base_capacity                  = var.on_demand_base_capacity
      on_demand_percentage_above_base_capacity = var.on_demand_percentage_above_base_capacity
      spot_instance_pools                      = 2
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.main.id
        version            = "$Latest"
      }

      dynamic "override" {
        for_each = var.instance_types
        content {
          instance_type = override.value
        }
      }
    }
  }

  tags = [
    for key, value in local.tags : {
      key                 = key
      value               = value
      propagate_at_launch = true
    }
  ]

  health_check_grace_period = 0
  wait_for_capacity_timeout = "0"
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [target_group_arns]
  }
}
