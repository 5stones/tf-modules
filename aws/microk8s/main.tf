locals {
  resource_name = replace(var.name, "/[^\\w+=,.@]+/", "-")
  cluster_id    = var.cluster_id != "" ? var.cluster_id : lower(local.resource_name)
  base_tags = {
    Name = var.name
    # used by the aws cloud provider
    "kubernetes.io/cluster/${local.cluster_id}" = "owned"
  }
  tags = merge(local.base_tags, var.tags)
}

data "aws_vpc" "main" {
  id = data.aws_subnet.main[0].vpc_id
}

data "aws_subnet" "main" {
  count = 1
  id    = var.subnets[count.index]
}
