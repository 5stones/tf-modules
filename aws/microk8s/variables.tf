variable "name" {
  description = "The name of the launch template, autoscaling group, role, and security group"
}

variable "cluster_id" {
  description = "The K8s cluster id used by the AWS cloud provider to identify AWS resources (defaults to the name with dashes)"
  default     = ""
}

variable "region" {
  description = "AWS region"
}

variable "key_name" {
  description = "ssh key name for access to instances"
}

variable "subnets" {
  description = "The VPC subnets to launch instances in"
}

variable "channel" {
  description = "Which snap channel of MicroK8s to install"
  default     = "latest/stable"
}

variable "ha" {
  default = true
}

variable "addons" {
  default = ["dns"]
}

variable "ebs_storage" {
  description = "Whether or not to enable ebs_storage and the EBS CSI Driver (note: check compatibility with k8s version)"
  default     = false
}

variable "ubuntu_version" {
  default = "20.04"
}

variable "description" {
  description = "The description of the launch template"
  default     = ""
}

variable "instance_types" {
  description = ""
  default     = ["t3a.medium", "t3.medium"]
}

variable "tags" {
  default = {}
}

variable "vpc_security_group_ids" {
  default = []
}

variable "monitoring" {
  description = "Launch template's enhanced monitoring"
  default     = true
}

variable "ssh_keys" {
  description = "Additional ssh keys to add to authorized_keys"
  default     = []
}

variable "ssh_from" {
  default = {
    cidr_blocks      = []
    ipv6_cidr_blocks = []
  }
}

variable "manage_from" {
  default = {
    cidr_blocks      = []
    ipv6_cidr_blocks = []
  }
}

variable "http_from" {
  default = {
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

variable "enabled_metrics" {
  default = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}

variable "on_demand_base_capacity" {
  default = 1
}

variable "on_demand_percentage_above_base_capacity" {
  default = 0
}

variable "autoscaling_range" {
  default = [1, 3]
}

variable "volume_size" {
  default = 20
}
