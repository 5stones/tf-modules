locals {
  is_arm = length(regexall("(?:a1|gd?)\\.", var.instance_types[0])) > 0

  addons = setunion(var.addons, var.ebs_storage ? ["dns"] : [])

  user_data = <<USERDATA
#!/bin/bash
apt update -y && apt upgrade -y && apt install -y awscli

AWS_AZ=`ec2metadata --availability-zone`
export AWS_DEFAULT_REGION=`echo $AWS_AZ | sed 's/[a-z]$//'`
echo "AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION" >> /etc/environment
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf

# add additional ssh keys
${join("\n", formatlist("echo '%s' >> /home/ubuntu/.ssh/authorized_keys", var.ssh_keys))}

# install and start microk8s
snap install microk8s --classic --channel=${var.channel}
usermod -a -G microk8s ubuntu
echo "alias kubectl='microk8s.kubectl'" >> /etc/bash.bashrc
${var.ha ? "" : "microk8s disable ha"}
microk8s stop

# enable AWS cloud provider code inside core k8s (deprecated, but is still necessary until AWS functionality is implemented elsewhere)
echo "--cloud-provider=aws" >> /var/snap/microk8s/current/args/kube-apiserver
echo "--cloud-provider=aws" >> /var/snap/microk8s/current/args/kube-controller-manager
echo "--cloud-provider=aws" >> /var/snap/microk8s/current/args/kubelet
${local.csi_feature_gates}

microk8s start

# find other node ips by security group
CLUSTER_IPS=`aws ec2 describe-instances \
  --filter "Name=instance.group-id,Values=${aws_security_group.main.id}" \
  --query 'Reservations[].Instances[].NetworkInterfaces[0].PrivateIpAddress' \
  --max-items 3 \
  --output text`

microk8s status --wait-ready

JOIN_IPS=`echo "$CLUSTER_IPS" | xargs -n1 echo | grep -v "$(ec2metadata --local-ipv4)"`
if [ ! -z "$JOIN_IPS" ]; then
  # join the microk8s cluster
  JOIN_CMDS=`printf "microk8s join %s:25000/${random_password.token.result}\n" $JOIN_IPS | paste -sd " || " -`
  if $JOIN_CMDS; then
    # successfully joined cluster
    microk8s status --wait-ready
  fi
else
  # first node in cluster
  microk8s enable ${join(" ", local.addons)}
  ${local.storage_block}
fi

microk8s.kubectl label node `hostname -f` \
  failure-domain.kubernetes.io/region=$AWS_DEFAULT_REGION \
  failure-domain.kubernetes.io/zone=$AWS_AZ \
  kubernetes.io/instance-type=`ec2metadata --instance-type`

# leave cluster on shutdown, but not restart
echo "microk8s leave" > /etc/rc0.d/K00microk8s-leave
chmod +x /etc/rc0.d/K00microk8s-leave

# configure to allow other instances to join with the token (valid for 10 years)
microk8s add-node -t ${random_password.token.result} -l 315360000
USERDATA

  # add ebs volumes as the default storage class
  storage_block = !var.ebs_storage ? "" : <<STORAGE
microk8s.kubectl apply -k 'github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master'
microk8s.kubectl patch storageclass microk8s-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
cat << SC | microk8s.kubectl apply -f -
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: ebs
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: ebs.csi.aws.com
volumeBindingMode: WaitForFirstConsumer
allowVolumeExpansion: true
SC
STORAGE

  # allow csi driver for ebs (isn't supported for k8s v1.19)
  csi_feature_gates = !var.ebs_storage ? "" : <<GATES
mv /var/lib/kubelet /var/lib/kubelet.org
ln -s /var/snap/microk8s/common/var/lib/kubelet /var/lib/kubelet
echo "--feature-gates=CSINodeInfo=true,CSIDriverRegistry=true,CSIBlockVolume=true" >> /var/snap/microk8s/current/args/kubelet
echo "--feature-gates=CSINodeInfo=true,CSIDriverRegistry=true,CSIBlockVolume=true,VolumeSnapshotDataSource=true" >> /var/snap/microk8s/current/args/kube-apiserver
GATES
}

resource "aws_launch_template" "main" {
  name        = local.resource_name
  description = var.description

  #default_version = true

  image_id      = data.aws_ami.ubuntu.id
  instance_type = var.instance_types[0]

  key_name = var.key_name
  monitoring {
    enabled = var.monitoring
  }

  iam_instance_profile {
    arn = aws_iam_instance_profile.main.arn
  }
  ebs_optimized = true

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_type           = "gp2"
      volume_size           = var.volume_size
      delete_on_termination = true
    }
  }

  user_data = base64encode(local.user_data)

  vpc_security_group_ids = local.vpc_security_group_ids

  tag_specifications {
    resource_type = "instance"
    tags          = local.tags
  }
  tag_specifications {
    resource_type = "volume"
    tags          = local.tags
  }
}

resource "random_password" "token" {
  length  = 32
  number  = false
  special = false
}

# find the most recent ubuntu image
data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  name_regex = "^ubuntu/images/hvm-ssd/ubuntu-[a-z]+-${var.ubuntu_version}[.0-9]*-${local.is_arm ? "arm64" : "amd64"}-server-"

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
