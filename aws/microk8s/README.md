# MicroK8s in AWS

Creates a [MicroK8s](https://microk8s.io/docs) cluster using an autoscaling group, where each instance installs MicroK8s on launch and then joins the cluster.

```
module "microk8s" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/microk8s"

  # required config
  name     = "Testing MicroK8s"
  region   = "us-east-1"
  key_name = "somekey"
  subnets  = [
    "subnet-12345678",
    "subnet-9abcef12",
  ]
}
```


## Instance Size

The defaults follow Canonical's recommendation of 20G of disk space and 4G of ram (t3a.medium).
It can run with as little as 1G of ram (micro) but leaves very little resources for a workload.


## Autoscaling

Currently there is no automatic scaling built in but can be added using one of the following:
* A [cluster autoscaler](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler/cloudprovider/aws).
* A simple CPU [autoscaling policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) using the `aws_autoscaling_group` output.
