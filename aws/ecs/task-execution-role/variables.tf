variable "name" {
  description = "The name of the iam role"
}

variable "ssm_resources" {
  description = "The arn resource pattern(s) for access to ssm parameters"
  default     = ["*"]
}

variable "cloudwatch_resources" {
  description = "The arn resource pattern(s) for access to ssm parameters"
  default     = ["*"]
}
