# create ecs task execution role with access to task secrets
resource "aws_iam_role" "main" {
  name = var.name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

}

# add permission to secrets
resource "aws_iam_role_policy" "main" {
  name = "${var.name}-secrets"
  role = aws_iam_role.main.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ssm:GetParameters",
            "Resource": ${jsonencode(var.ssm_resources)}
        },{
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": ${jsonencode(var.cloudwatch_resources)}
        }
    ]
}
EOF
}

# attach default AmazonECSTaskExecutionRolePolicy
resource "aws_iam_role_policy_attachment" "taskExecution" {
  role       = aws_iam_role.main.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
