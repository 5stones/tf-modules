output "id" {
  value = aws_launch_template.main.id
}

output "arn" {
  value = aws_launch_template.main.arn
}
