locals {
  efs_block = <<EOF
# mount efs
- file_system_id_01=${var.efs_id}
- efs_directory=/mnt/efs
- mkdir -p $${efs_directory}
- echo "$${file_system_id_01}:/ $${efs_directory} efs tls,_netdev" >> /etc/fstab
- mount -a -t efs defaults
EOF

  user_data = <<EOF
#cloud-config
repo_update: true
repo_upgrade: all

packages:
- amazon-efs-utils

runcmd:
# ecs
- echo ECS_CLUSTER=${var.cluster} >> /etc/ecs/ecs.config
- echo ECS_BACKEND_HOST= >> /etc/ecs/ecs.config
- echo ECS_RESERVED_MEMORY=160 >> /etc/ecs/ecs.config
- echo ECS_IMAGE_PULL_BEHAVIOR=once >> /etc/ecs/ecs.config
- echo ECS_ENABLE_SPOT_INSTANCE_DRAINING=true >> /etc/ecs/ecs.config

${var.efs_id == "" ? "" : local.efs_block}

# add additional ssh keys
${join("\n", formatlist("- echo '%s' >> /home/ec2-user/.ssh/authorized_keys", var.ssh_keys))}
EOF
}

# find the most recent ECS image
data "aws_ami" "ecs" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = [
      "amzn-ami-*.*.*-amazon-ecs-optimized",
      "amzn2-ami-ecs-hvm-2.*.*-x86_64-ebs",
    ]
  }
}

resource "aws_launch_template" "main" {
  name        = var.name
  description = var.description

  #default_version = true

  image_id      = data.aws_ami.ecs.id
  instance_type = var.instance_type

  key_name = var.key_name
  monitoring {
    enabled = var.monitoring
  }

  iam_instance_profile {
    arn = var.iam_instance_profile.arn
    name = var.iam_instance_profile.name
  }
  ebs_optimized = true

  user_data = base64encode(local.user_data)

  vpc_security_group_ids = var.vpc_security_group_ids

  dynamic "instance_market_options" {
    for_each = var.spot ? ["spot"] : []
    content {
      market_type = "spot"
    }
  }

  tag_specifications {
    resource_type = "instance"
    tags          = var.tags
  }
  tag_specifications {
    resource_type = "volume"
    tags          = var.tags
  }
}
