variable "name" {
  description = "The name of the launch template"
  default     = ""
}

variable "description" {
  description = "The description of the launch template"
  default     = ""
}

variable "cluster" {
  description = "ecs cluster name"
}

variable "key_name" {
  description = "ssh key name for access to instances"
}

variable "instance_type" {
  default = "t3.small"
}

variable "tags" {
  default = {}
}

variable "iam_instance_profile" {
  type = object({
    arn  = string
    name = string
  })
  default = {
    arn  = null
    name = "ecsInstanceRole"
  }
}

variable "vpc_security_group_ids" {
  default = []
}

variable "spot" {
  description = "Whether or not to use spot instances"
  type        = bool
  default     = true
}

variable "monitoring" {
  default = true
}

variable "efs_id" {
  description = "The id of the efs volume to mount on every container instance"
  default     = ""
}

variable "ssh_keys" {
  description = "Additional ssh keys to add to authorized_keys"
  default     = []
}
