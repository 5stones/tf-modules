locals {
  min = var.range[0]
  max = var.range[1]
}

resource "aws_appautoscaling_target" "main" {
  count              = local.min != local.max ? 1 : 0
  service_namespace  = "ecs"
  resource_id        = "service/${var.cluster}/${var.service}"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity       = local.min
  max_capacity       = local.max
}

resource "aws_appautoscaling_policy" "main" {
  count             = local.min != local.max ? 1 : 0
  service_namespace = "ecs"
  resource_id       = aws_appautoscaling_target.main[0].resource_id

  name               = var.policy_name
  scalable_dimension = "ecs:service:DesiredCount"
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = var.target
  }
}

