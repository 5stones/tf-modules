Defines autoscaling on an ECS Service based on average CPU.

Note: If `range[0] = range[1]`, no autoscaling resources will be defined.
