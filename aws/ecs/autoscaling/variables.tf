variable "cluster" {
  description = "The name of the ECS cluster"
}

variable "service" {
  description = "The name of the ECS service in the cluster"
}

variable "target" {
  default = 70
}

variable "range" {
  default = [1, 1]
}

variable "policy_name" {
  default = "CPU-Tracking"
}

