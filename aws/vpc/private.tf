resource "aws_subnet" "private" {
  for_each = {
    a = 128
    b = 144
    c = 160
  }
  vpc_id     = aws_vpc.main.id
  cidr_block = "${var.network_prefix}.${each.value}.0/20"

  availability_zone = "${var.region}${each.key}"

  ipv6_cidr_block                 = replace(aws_vpc.main.ipv6_cidr_block, "00::/56", "8${each.key}::/64")
  map_public_ip_on_launch         = false
  assign_ipv6_address_on_creation = true

  tags = {
    Name        = "${var.name} private ${each.key}"
    Environment = var.env
    Tier        = "private"
  }
}

resource "aws_default_route_table" "private" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  # egress only ipv6
  route {
    ipv6_cidr_block        = "::/0"
    egress_only_gateway_id = aws_egress_only_internet_gateway.main.id
  }

  # nat instance
  dynamic "route" {
    for_each = module.nat_instance.enabled ? [module.nat_instance.instance] : []
    content {
      cidr_block  = "0.0.0.0/0"
      instance_id = route.value.id
    }
  }

  # aws managed nat gateway
  dynamic "route" {
    for_each = aws_nat_gateway.main
    content {
      cidr_block     = "0.0.0.0/0"
      nat_gateway_id = route.value.id
    }
  }

  tags = {
    Name        = "${var.name} private"
    Environment = var.env
    Tier        = "private"
  }
}
