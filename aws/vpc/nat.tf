# ipv6 egress
resource "aws_egress_only_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name        = var.name
    Environment = var.env
  }
}

module "nat_instance" {
  source = "./nat-instance"

  enabled = var.nat != null && var.nat != "gateway"

  key_name                  = try(var.nat.key_name, null)
  custom_security_group_ids = try(var.nat.custom_security_group_ids, [])
  ssh_port                  = try(var.nat.ssh_port, 22)
  ssh_cidr_blocks           = try(var.nat.ssh_cidr_blocks, [])
  ssh_ipv6_cidr_blocks      = try(var.nat.ssh_ipv6_cidr_blocks, [])
  ssh_keys                  = try(var.nat.ssh_keys, [])
  instance_type             = try(var.nat.instance_type, "t3a.micro")
  efs_id                    = try(var.nat.efs_id, "")
  eip                       = try(var.nat.eip, true)
  forward_ports             = try(var.nat.forward_ports, {})
  open_ports                = try(var.nat.open_ports, {})

  region    = var.region
  subnet_id = aws_subnet.public["a"].id

  tags = merge({
    Name        = "${var.name} nat"
    Environment = var.env
  }, try(var.nat.tags, {}))
}

resource "aws_nat_gateway" "main" {
  count = var.nat == "gateway" ? 1 : 0

  allocation_id = aws_eip.nat_gateway[0].id
  subnet_id     = aws_subnet.public["a"].id

  tags = {
    Name        = var.name
    Environment = var.env
  }

  depends_on = [aws_internet_gateway.main]
}
resource "aws_eip" "nat_gateway" {
  count = var.nat == "gateway" ? 1 : 0
  vpc   = true
}
