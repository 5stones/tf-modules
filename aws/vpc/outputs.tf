output "vpc" {
  value = aws_vpc.main
}

output "public_subnets" {
  value = aws_subnet.public
}

output "private_subnets" {
  value = aws_subnet.private
}

output "nat" {
  value = module.nat_instance.enabled ? module.nat_instance.instance : try(aws_eip.nat_gateway[0], null)
}

output "nat_ip" {
  value = module.nat_instance.enabled ? module.nat_instance.ip : try(aws_eip.nat_gateway[0].public_ip, null)
}
