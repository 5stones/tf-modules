variable "region" {
  description = "The aws region"
}

variable "subnet_id" {
  description = "The subnet with external external access for the NAT instance"
}

variable "key_name" {
  description = "ssh key name for access to instances"
}


variable "enabled" {
  description = "Allows conditional creation"
  default     = true
}

variable "custom_security_group_ids" {
  description = "Pass an array of vpc security group ids if you want don't want to autogenerate one"
  default     = []
}

variable "ssh_port" {
  default = 22
}

variable "ssh_cidr_blocks" {
  default = []
}

variable "ssh_ipv6_cidr_blocks" {
  default = []
}

variable "ssh_keys" {
  description = "Additional ssh keys to add to authorized_keys"
  default     = []
}

variable "tags" {
  default = {
    Name = "NAT"
  }
}

variable "eip" {
  description = "Automatically associate with an elastic ip?"
  default     = true
}

variable "instance_type" {
  default = "t4g.micro"
}

variable "distro" {
  description = "Ubuntu or Amazon"
  default     = "Ubuntu"

  validation {
    condition     = (var.distro == "Ubuntu" || var.distro == "Amazon")
    error_message = "Only Ubuntu and Amazon supported."
  }
}

variable "ubuntu_version" {
  default = "20.04"
}

variable "efs_id" {
  description = "The id of the efs volume to mount (optional, only supported with Amazon Linux)"
  default     = ""
}

variable "forward_ports" {
  description = "A mapping of ports to forward to private subnets, where external traffic routes through this instance (example { 80: 10.0.0.100:8080 })"
  default     = {}
}

variable "snat_forward_ports" {
  description = "A mapping of ports to forward to public subnets, rewriting the source ips, so the response will return (example { 80: 10.0.0.100:8080 })"
  default     = {}
}

variable "open_ports" {
  description = "A mapping of ports to open and their descriptions"
  default     = {}
}
