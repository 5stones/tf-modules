resource "aws_security_group" "main" {
  count = var.enabled && length(var.custom_security_group_ids) == 0 ? 1 : 0

  vpc_id = data.aws_vpc.main.id

  ingress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = [data.aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = data.aws_vpc.main.ipv6_cidr_block == null ? [] : [data.aws_vpc.main.ipv6_cidr_block]
    description      = "Internal"
  }

  ingress {
    protocol         = "tcp"
    from_port        = var.ssh_port
    to_port          = 22
    cidr_blocks      = var.ssh_cidr_blocks
    ipv6_cidr_blocks = var.ssh_ipv6_cidr_blocks
    description      = "SSH"
  }

  dynamic "ingress" {
    for_each = merge(var.open_ports, var.snat_forward_ports, var.forward_ports)
    content {
      protocol         = "tcp"
      from_port        = ingress.key
      to_port          = ingress.key
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description      = ingress.value
    }
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = var.tags
}
