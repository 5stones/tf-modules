locals {
  is_arm = length(regexall("(?:a1|gd?)\\.", var.instance_type)) > 0

  distros = {
    Amazon = {
      owner      = "amazon"
      name_regex = "^amzn2-ami-hvm-[0-9.]+-${local.is_arm ? "arm64" : "x86_64"}-gp2$"
    }
    Ubuntu = {
      owner      = "099720109477" # Canonical
      name_regex = "^ubuntu/images/hvm-ssd/ubuntu-[a-z]+-${var.ubuntu_version}[.0-9]*-${local.is_arm ? "arm64" : "amd64"}-server-"
    }
  }

  use_efs   = (var.efs_id != "" && var.distro == "Amazon")
  efs_block = <<EFS
# mount efs
- file_system_id_01=${var.efs_id}
- efs_directory=/mnt/efs
- mkdir -p $${efs_directory}
- echo "$${file_system_id_01}:/ $${efs_directory} efs tls,_netdev" >> /etc/fstab
- mount -a -t efs defaults
EFS

  # list of tuples of port forwarding destinations [[ip, port],...]
  forward_dest = {
    snat     = [for p, d in var.snat_forward_ports : strcontains(d, ":") ? split(":", d) : [d, p]]
    internal = [for p, d in var.forward_ports : strcontains(d, ":") ? split(":", d) : [d, p]]
  }

  rules = concat(
    # nat for traffic from internal network going out to the internet
    ["iptables -t nat -A POSTROUTING -o \"$INTERFACE\" -s \"${data.aws_vpc.main.cidr_block}\" -j MASQUERADE"],

    # port-forward based on destination to this ip
    [for f, t in merge(var.snat_forward_ports, var.forward_ports) : "iptables -t nat -A PREROUTING -i \"$INTERFACE\" -d \"$LOCAL_IPV4\" -p tcp --dport ${f} -j DNAT --to-destination ${t}"],

    # works for any subnet, but changes the source ip, so that packets come back here
    [for d in local.forward_dest.snat : "iptables -t nat -A POSTROUTING -o \"$INTERFACE\" -p tcp -d ${d[0]} --dport ${d[1]} -j SNAT --to-source \"$LOCAL_IPV4\""],
    # works for private subnets, by allowing forwarding without changing the source ip, since traffic comes back here anyway
    [for d in local.forward_dest.internal : "iptables -A FORWARD -o \"$INTERFACE\" -p tcp -d ${d[0]} --dport ${d[1]} -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT"],

    # TODO port-forwarding with ipv6, for subnets using an egress-only gateway?
  )
}

provider "aws" {
  region = var.region
}

resource "aws_eip" "main" {
  count    = var.enabled && var.eip ? 1 : 0
  instance = aws_instance.nat[0].id
  vpc      = true
}

resource "aws_instance" "nat" {
  count = var.enabled ? 1 : 0

  ami           = data.aws_ami.nat.id
  instance_type = var.instance_type
  monitoring    = false
  subnet_id     = var.subnet_id
  ebs_optimized = true

  vpc_security_group_ids = length(var.custom_security_group_ids) > 0 ? var.custom_security_group_ids : [aws_security_group.main[0].id]

  key_name = var.key_name
  tags     = var.tags

  source_dest_check = false

  user_data = <<USERDATA
#cloud-config
package_update: true
package_upgrade: true
packages:
${local.use_efs ? "- amazon-efs-utils" : ""}

write_files:
- path: /etc/sysctl.d/99-nat.conf
  content: |
    net.ipv4.ip_forward = 1
    net.ipv4.conf.all.send_redirects = 0
- path: /etc/network/if-pre-up.d/iptablesload
  permissions: '0755'
  content: |
    #!/bin/sh
    iptables-restore < /etc/iptables.rules
    exit 0

ssh_authorized_keys:
${join("\n", formatlist("- %s", var.ssh_keys))}

runcmd:
- sysctl -p /etc/sysctl.d/99-nat.conf
- INTERFACE=$(ip route | awk '/^default/ {printf $5}')
- LOCAL_IPV4=$(ec2metadata --local-ipv4)
${join("\n", formatlist("- %s", local.rules))}
- iptables-save > /etc/iptables.rules

${local.use_efs ? local.efs_block : ""}
USERDATA

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [ami, disable_api_termination]
  }
}

data "aws_ami" "nat" {
  owners     = [local.distros[var.distro]["owner"]]
  name_regex = local.distros[var.distro]["name_regex"]

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  most_recent = true
}

data "aws_vpc" "main" {
  id = data.aws_subnet.main.vpc_id
}

data "aws_subnet" "main" {
  id = var.subnet_id
}
