Launch an EC2 instance using the newest Amazon NAT image, for a lower cost NAT & SSH tunnel.

note: This creates the NAT instance but not the VPC routing rules, private subnets, or a DNS entry.
