output "enabled" {
  value = var.enabled
}

output "instance" {
  value = var.enabled ? aws_instance.nat[0] : null
}

output "ip" {
  value = try(aws_eip.main[0].public_ip, aws_instance.nat[0].public_ip, null)
}
