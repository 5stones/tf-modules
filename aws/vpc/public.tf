resource "aws_subnet" "public" {
  for_each = {
    a = 0
    b = 16
    c = 32
  }
  vpc_id     = aws_vpc.main.id
  cidr_block = "${var.network_prefix}.${each.value}.0/20"

  availability_zone               = "${var.region}${each.key}"
  ipv6_cidr_block                 = replace(aws_vpc.main.ipv6_cidr_block, "00::/56", "0${each.key}::/64")
  map_public_ip_on_launch         = true
  assign_ipv6_address_on_creation = true

  tags = {
    Name        = "${var.name} public ${each.key}"
    Environment = var.env
    Tier        = "public"
  }

  lifecycle {
    ignore_changes = [
      # if we're importing, let's ignore this so it doesn't attempt to replace it. it can be deleted manually
      cidr_block,
    ]
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.main.id
  }

  tags = {
    Name        = "${var.name} public"
    Environment = var.env
    Tier        = "public"
  }
}

resource "aws_route_table_association" "public" {
  for_each       = aws_subnet.public
  subnet_id      = each.value.id
  route_table_id = aws_route_table.public.id
}
