Creates a vpc for a given environment with three public and private subnets.

## Default Security Group

The default security group allows access from internal IPs.
Additional ingress rules can be added with `ingress = [...]`.

## NAT

By default, the private subnets will have no IPv4 access.
To use the AWS managed NAT gateway use `nat = "gateway"`.
To create an EC2 NAT instance, include configuration for the nat-instance module (excluding subnet_id).

```
  nat = {
    instance_type = "t3a.micro"
    key_name = "your-ssh-key-name"
    ssh_cidr_blocks = [
      "198.51.100.1/32", # Your public IPv4 cidr
    ]
  }
```
