variable "region" {
}

variable "name" {
}

variable "env" {
}

variable "network_prefix" {
  default = "10.0"
}

variable "ingress" {
  description = "Additional ingress rules for the default security group"
  default     = []
}

variable "nat" {
  description = "null, gateway, or a config object for nat-instance module (except subnet_id)"
  default     = null
}
