locals {
  agent_cloud_config = <<-USERDATA
  ${local.base_cloud_config}
  runcmd:
  ${local.base_runcmd}
  - curl -sfL https://get.k3s.io | ${local.k3s_env} sh -s - agent ${local.agent_args} --server "https://${local.server_instances[0].private_ip}:6443"
  USERDATA

  agent_args = join(" ", [
    local.kubelet_args,
    local.node_labels,
  ])

  agent_tags = merge(local.tags, {
    Name = "${var.name} Agent"
  })
}

resource "aws_launch_template" "agent" {
  name        = "${local.resource_name}-Agent"
  description = var.description

  #default_version = true

  image_id      = data.aws_ami.ubuntu.id
  instance_type = var.instance_types[0]

  key_name = var.key_name
  monitoring {
    enabled = var.monitoring
  }

  iam_instance_profile {
    arn = aws_iam_instance_profile.main["Agent"].arn
  }
  ebs_optimized = true

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_type           = "gp2"
      volume_size           = var.volume_size
      delete_on_termination = true
    }
  }

  user_data = base64encode(local.agent_cloud_config)

  vpc_security_group_ids = concat([aws_security_group.node.id], var.vpc_security_group_ids)

  tag_specifications {
    resource_type = "instance"
    tags          = local.agent_tags
  }
  tag_specifications {
    resource_type = "volume"
    tags          = local.agent_tags
  }
}

resource "aws_autoscaling_group" "agents" {
  name     = "${var.name} Agents"
  min_size = var.autoscaling_range[0]
  max_size = var.autoscaling_range[1]

  vpc_zone_identifier = var.subnets

  enabled_metrics = var.enabled_metrics

  mixed_instances_policy {
    instances_distribution {
      on_demand_base_capacity                  = var.on_demand_base_capacity
      on_demand_percentage_above_base_capacity = var.on_demand_percentage_above_base_capacity
      spot_instance_pools                      = 2
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.agent.id
        version            = "$Latest"
      }

      dynamic "override" {
        for_each = var.instance_types
        content {
          instance_type = override.value
        }
      }
    }
  }

  tags = [
    for key, value in local.tags : {
      key                 = key
      value               = value
      propagate_at_launch = true
    }
  ]

  health_check_grace_period = 0
  wait_for_capacity_timeout = "0"
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [target_group_arns]
  }
}
