# K3s in AWS

Creates a [K3s](https://k3s.io/) cluster using an autoscaling group for agents, where each instance installs K3s on launch and then joins the cluster.

```
module "testing-k3s" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/k3s"

  # required config
  name     = "Testing K3s"
  region   = "us-east-1"
  key_name = "somekey"
  subnets  = [
    "subnet-12345678",
    "subnet-9abcef12",
  ]
}

# recommended for high-availability
module "ha-k3s" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/k3s"

  name     = "HA K3s"
  region   = "us-east-1"
  key_name = "somekey"
  subnets  = [
    "subnet-12345678",
    "subnet-9abcef12",
  ]

  tls_san   = ["k3s.local"]
  datastore = "mysql://username:password@tcp(hostname:3306)/database-name"
  servers   = 2
}

# recommended for running rancher
module "rancher-k3s" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/k3s"

  name     = "Rancher K3s"
  region   = "us-east-1"
  key_name = "somekey"
  subnets  = [
    "subnet-12345678",
    "subnet-9abcef12",
  ]

  tls_san   = ["rancher.local"]
  datastore = "mysql://username:password@tcp(hostname:3306)/database-name"
  servers   = 2

  autoscaling_range = [0, 0]
}
```


## Instance Size

The defaults allocate 20G of disk space and 4G of ram (t3a.medium).
It can run with as little as 1G of ram (micro) but leaves very little resources for a workload.


## Autoscaling

Currently there is no automatic scaling built in but can be added using one of the following:
* A [cluster autoscaler](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler/cloudprovider/aws).
* A simple CPU [autoscaling policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) using the `aws_autoscaling_group` output.
