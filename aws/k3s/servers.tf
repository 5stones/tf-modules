locals {
  server_args = join(" ", concat(
    [
      local.agent_args,
      #"--disable servicelb",
      #"--disable traefik",
    ],
    formatlist("--tls-san %s", var.tls_san),
    var.external_cloud_provider ? ["--disable-cloud-controller"] : []
  ))

  server_join = var.datastore != null ? "--datastore-endpoint='${var.datastore}'" : "--server https://${aws_instance.initial[0].private_ip}:6443"

  server_instances = concat(aws_instance.initial, aws_instance.servers)
}

resource "aws_instance" "initial" {
  count = var.datastore != null ? 0 : 1

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_types[0]

  key_name   = var.key_name
  monitoring = var.monitoring

  iam_instance_profile = aws_iam_instance_profile.main["Server"].name
  ebs_optimized        = true

  root_block_device {
    volume_size = var.volume_size
  }

  subnet_id              = var.subnets[0]
  vpc_security_group_ids = concat([aws_security_group.node.id, aws_security_group.server.id], var.vpc_security_group_ids)

  tags = merge(local.tags, {
    Name = "${var.name} Server 0"
  })
  volume_tags = merge(local.tags, {
    Name = "${var.name} Server 0"
  })

  user_data = <<-USERDATA
  ${local.base_cloud_config}
  runcmd:
  ${local.base_runcmd}
  - curl -sfL https://get.k3s.io | ${local.k3s_env} sh -s - server ${local.server_args} --cluster-init}
  USERDATA

  lifecycle {
    ignore_changes = [ami, disable_api_termination, user_data]
  }
}

resource "aws_instance" "servers" {
  count = var.datastore != null ? var.servers : var.servers - 1

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_types[0]

  key_name   = var.key_name
  monitoring = var.monitoring

  iam_instance_profile = aws_iam_instance_profile.main["Server"].name
  ebs_optimized        = true

  root_block_device {
    volume_size = var.volume_size
  }

  subnet_id              = var.subnets[(var.datastore != null ? count.index : count.index + 1) % length(var.subnets)]
  vpc_security_group_ids = concat([aws_security_group.node.id, aws_security_group.server.id], var.vpc_security_group_ids)

  tags = merge(local.tags, {
    Name = "${var.name} Server ${count.index + 1}"
  })
  volume_tags = merge(local.tags, {
    Name = "${var.name} Server ${count.index + 1}"
  })

  user_data = <<-USERDATA
  ${local.base_cloud_config}
  runcmd:
  ${local.base_runcmd}
  - curl -sfL https://get.k3s.io | ${local.k3s_env} sh -s - server ${local.server_args} ${local.server_join}
  USERDATA

  lifecycle {
    ignore_changes = [ami, disable_api_termination, user_data]
  }
}
