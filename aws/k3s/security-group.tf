locals {
  ipv6_cidr_blocks = data.aws_vpc.main.ipv6_cidr_block == null ? [] : [data.aws_vpc.main.ipv6_cidr_block]
}

# control plane (servers)
resource "aws_security_group" "server" {
  name        = "${local.resource_name}-Server"
  vpc_id      = data.aws_vpc.main.id
  description = "Servers of a K3s cluster"

  # Management Traffic into the cluster
  ingress {
    protocol         = "tcp"
    from_port        = 6443
    to_port          = 6443
    cidr_blocks      = concat([data.aws_vpc.main.cidr_block], var.manage_from.cidr_blocks)
    ipv6_cidr_blocks = concat(local.ipv6_cidr_blocks, var.manage_from.ipv6_cidr_blocks)
    description      = "External access to API server"
  }


  # Traffic inside the cluster #
  ingress {
    protocol        = "tcp"
    from_port       = 6443
    to_port         = 6443
    security_groups = [aws_security_group.node.id]
    description     = "API server"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 2379
    to_port     = 2380
    self        = true
    description = "Embedded HA etcd"
  }

  # only one security group should have the cluster owned tag
  tags = merge(var.tags, {
    Name = "${var.name} Server"
  })
}

# all nodes (servers and agents)
resource "aws_security_group" "node" {
  name        = local.resource_name
  vpc_id      = data.aws_vpc.main.id
  description = "Nodes of a K3s cluster (Servers and Agents)"

  # Allow external traffic if ingress is enabled with external IPs

  ingress {
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = var.http_from.cidr_blocks
    ipv6_cidr_blocks = var.http_from.ipv6_cidr_blocks
    description      = "http"
  }
  ingress {
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = var.http_from.cidr_blocks
    ipv6_cidr_blocks = var.http_from.ipv6_cidr_blocks
    description      = "https"
  }

  ingress {
    protocol         = "tcp"
    from_port        = 22
    to_port          = 22
    cidr_blocks      = concat([data.aws_vpc.main.cidr_block], var.ssh_from.cidr_blocks)
    ipv6_cidr_blocks = concat(local.ipv6_cidr_blocks, var.ssh_from.ipv6_cidr_blocks)
    description      = "SSH"
  }

  # VPC Traffic to NodePorts (AWS Load Balancers)
  ingress {
    protocol         = "tcp"
    from_port        = 30000
    to_port          = 32767
    cidr_blocks      = [data.aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = local.ipv6_cidr_blocks
    description      = "NodePort Services"
  }


  # Traffic inside the cluster #

  ingress {
    protocol    = "tcp"
    from_port   = 10250
    to_port     = 10250
    self        = true
    description = "Kubelet metrics"
  }
  ingress {
    protocol    = "tcp"
    from_port   = 9796
    to_port     = 9796
    self        = true
    description = "Prometheus metrics"
  }
  ingress {
    protocol    = "udp"
    from_port   = 8472
    to_port     = 8472
    self        = true
    description = "Flannel VXLAN"
  }


  # Egress #

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = local.tags
}
