locals {
  resource_name = replace(var.name, "/[^\\w+=,.@]+/", "-")
  cluster_id    = var.cluster_id != "" ? var.cluster_id : lower(local.resource_name)
  base_tags = {
    Name = var.name
    # used by the aws cloud provider
    "kubernetes.io/cluster/${local.cluster_id}" = "owned"
  }
  tags = merge(local.base_tags, var.tags)

  is_arm = length(regexall("(?:a1|gd?)\\.", var.instance_types[0])) > 0

  k3s_env = join(" ", [
    "INSTALL_K3S_CHANNEL=${var.channel}",
    "K3S_TOKEN=${random_password.token.result}",
  ])
  kubelet_args = join(" ", formatlist("--kubelet-arg \"%s\"", var.external_cloud_provider ? [
    "cloud-provider=external",
    "provider-id=aws:///$(ec2metadata --availability-zone)/$(ec2metadata --instance-id)",
  ] : [
  ]))
  node_labels = join(" ", formatlist("--node-label \"%s\"", [
    "topology.kubernetes.io/region=$AWS_DEFAULT_REGION",
    "topology.kubernetes.io/zone=$(ec2metadata --availability-zone)",
    "node.kubernetes.io/instance-type=$(ec2metadata --instance-type)",
  ]))

  base_cloud_config = <<-USERDATA
  #cloud-config
  package_update: true
  package_upgrade: true
  ssh_authorized_keys:
  ${join("\n", formatlist("- %s", var.ssh_keys))}
  USERDATA

  base_runcmd = <<-RUNCMD
  - AWS_AZ=$(ec2metadata --availability-zone)
  - export AWS_DEFAULT_REGION=`echo $AWS_AZ | sed 's/[a-z]$//'`
  - echo "AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION" >> /etc/environment
  - ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
  RUNCMD

  discovery = <<-EOF
  packages:
  - awscli

  write_files:
  - path: /usr/local/bin/k3s-servers
    permissions: "0755"
    content: |
      SERVER_IPS=$(aws ec2 describe-instances \
        --filter "Name=instance.group-id,Values=${aws_security_group.server.id}" \
        --query 'Reservations[].Instances[].NetworkInterfaces[0].PrivateIpAddress' \
        --output text)
      echo "$SERVER_IPS" | xargs -n1 echo | grep -v "$(ec2metadata --local-ipv4)"
  EOF
}

resource "random_password" "token" {
  length  = 32
  number  = false
  special = false
}

data "aws_vpc" "main" {
  id = data.aws_subnet.main[0].vpc_id
}

data "aws_subnet" "main" {
  count = 1
  id    = var.subnets[count.index]
}

# find the most recent ubuntu image
data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  name_regex = "^ubuntu/images/hvm-ssd/ubuntu-[a-z]+-${var.ubuntu_version}[.0-9]*-${local.is_arm ? "arm64" : "amd64"}-server-"

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
