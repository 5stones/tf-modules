variable "name" {
  description = "The name for the aws_iam_user resource"
}

variable "path" {
  description = "The path for the aws_iam_user resource"
  default     = "/"
}

variable "tags" {
  description = "The tags for the aws_iam_user resource"
  default     = {}
}

variable "from_equals" {
  description = "Restrict the from to an email address or list of email addresses"
  default     = null
}

variable "from_like" {
  description = "Restrict the from to a pattern or list of patterns ('*' or '?' wildcards)"
  default = null
}

variable "recipients_like" {
  description = "Restrict the recipients to a pattern or list of patterns ('*' or '?' wildcards)"
  default = null
}
