# Create an AWS IAM user to send emails over SMTP

## Usage

```
module "smtp_user" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/ses/smtp-user"
  name   = "k8s.example-cluster.example-namespace.example-app"

  #path = "/"
  #tags = { tagName = "tagValue" }

  # restrict allowed email adddresses
  #from_equals     = ["user@example.com"]
  #from_like       = ["*@example.com"]
  #recipients_like = ["*@example.com"]
}

locals {
  smtp_username = module.smtp_user.auth.username
  smtp_password = module.smtp_user.auth.password
}
```
