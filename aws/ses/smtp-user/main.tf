locals {
  condition = merge(
    var.from_equals == null ? {} : {
      StringEquals = {
        "ses:FromAddress" = var.from_equals
      }
    },
    var.from_like == null ? {} : {
      StringLike = {
        "ses:FromAddress" = var.from_like
      }
    },
    var.recipients_like == null ? {} : {
      "ForAllValues:StringLike" = {
        "ses:Recipients" = var.recipients_like
      }
    },
  )
}

resource "aws_iam_user" "main" {
  name = var.name
  path = var.path
  tags = var.tags
}

resource "aws_iam_user_policy" "main" {
  name = "ses"
  user = aws_iam_user.main.name

  policy = <<-POLICY
  {
    "Version":"2012-10-17",
    "Statement":[
      {
        "Effect":"Allow",
        "Action":[
          "ses:SendEmail",
          "ses:SendRawEmail"
        ],
        "Resource":"*",
        "Condition": ${jsonencode(local.condition)}
      }
    ]
  }
  POLICY
}

resource "aws_iam_access_key" "main" {
  user = aws_iam_user.main.name
}
