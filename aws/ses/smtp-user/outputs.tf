output "aws_iam_user" {
  value = aws_iam_user.main
}

output "aws_iam_access_key" {
  value = aws_iam_access_key.main
}

output "auth" {
  value = {
    username = aws_iam_access_key.main.id
    password = aws_iam_access_key.main.ses_smtp_password_v4
  }
}
