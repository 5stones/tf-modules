#!/bin/bash
set -e

if [ -z "$1" ]; then
  RKE2_MINOR=$(rke2 --version | grep -o 'v[0-9]*\.[0-9]*')
  PROVIDER_VERSION="$RKE2_MINOR.0"
else
  PROVIDER_VERSION="v$1"
fi
[[ $(uname -m) == x86_64* ]] && ARCH=amd64 || ARCH=arm64

PROVIDER_DIR=/usr/local/bin/k8s-credential-providers
mkdir -p $PROVIDER_DIR

FILENAME=ecr-credential-provider-linux-$ARCH-$PROVIDER_VERSION
curl -sfL https://artifacts.k8s.io/binaries/cloud-provider-aws/$PROVIDER_VERSION/linux/$ARCH/ecr-credential-provider-linux-$ARCH > $PROVIDER_DIR/$FILENAME
chmod +x $PROVIDER_DIR/$FILENAME
ln -sf ./$FILENAME $PROVIDER_DIR/ecr-credential-provider

echo "Installed ecr-credential-provider $PROVIDER_VERSION for $ARCH to $FILENAME"
