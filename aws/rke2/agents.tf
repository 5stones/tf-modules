locals {
  agent_runcmd = chomp(<<-RUNCMD
  ${local.base_runcmd}
  - curl -sfL https://get.rke2.io | ${local.install_env} INSTALL_RKE2_TYPE=agent sh -
  - cp -f /usr/local/share/rke2/rke2-cis-sysctl.conf /etc/sysctl.d/60-rke2-cis.conf
  - /usr/local/bin/install-ecr-credential-provider.sh ${var.ecr_credential_provider_version}
  - systemctl restart --no-block systemd-sysctl
  - systemctl enable rke2-agent.service
  - systemctl start --no-block rke2-agent.service
  RUNCMD
  )

  agent_tags = merge(local.tags, {
    Name = "${var.name} Agent"
  })
}

resource "aws_launch_template" "agent" {
  name        = "${local.resource_name}-Agent"
  description = var.description

  #default_version = true

  image_id      = data.aws_ami.ubuntu[local.is_arm ? "arm64" : "amd64"].id
  instance_type = var.instance_types[0]

  key_name = var.key_name
  monitoring {
    enabled = var.monitoring
  }

  iam_instance_profile {
    arn = aws_iam_instance_profile.main["Agent"].arn
  }
  ebs_optimized = true

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_type           = "gp3"
      volume_size           = var.volume_size
      delete_on_termination = true
    }
  }

  user_data = base64encode(<<-USERDATA
  ${local.base_cloud_config}

  write_files:
  - path: /etc/rancher/rke2/config.yaml
    permissions: "0600"
    content: |
      server: https://${aws_instance.initial.private_ip}:9345
      token: ${random_password.token.result}
      profile: ${var.profile}
      cloud-provider-name: ${var.cloud_provider}
      kubelet-arg:
        - system-reserved-cgroup=/system.slice
        - system-reserved=cpu=170m,memory=700Mi,ephemeral-storage=2200Mi
        - eviction-hard=memory.available<50Mi
        - image-credential-provider-bin-dir=/usr/local/bin/k8s-credential-providers
        - image-credential-provider-config=/etc/rancher/rke2/ecr-config.yaml
  ${local.common_files}

  ${local.agent_runcmd}
  USERDATA
  )

  vpc_security_group_ids = concat([aws_security_group.node.id], var.vpc_security_group_ids)

  tag_specifications {
    resource_type = "instance"
    tags          = local.agent_tags
  }
  tag_specifications {
    resource_type = "volume"
    tags          = local.agent_tags
  }
}

resource "aws_autoscaling_group" "agents" {
  name     = "${var.name} Agents"
  min_size = var.agents[0]
  max_size = var.agents[1]

  vpc_zone_identifier = var.subnets

  enabled_metrics = var.enabled_metrics

  mixed_instances_policy {
    instances_distribution {
      on_demand_base_capacity                  = var.on_demand_base_capacity
      on_demand_percentage_above_base_capacity = var.on_demand_percentage_above_base_capacity
      spot_instance_pools                      = 2
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.agent.id
        version            = "$Latest"
      }

      dynamic "override" {
        for_each = var.instance_types
        content {
          instance_type = override.value
        }
      }
    }
  }

  tags = [
    for key, value in local.agent_tags : {
      key                 = key
      value               = value
      propagate_at_launch = true
    }
  ]

  health_check_grace_period = 0
  wait_for_capacity_timeout = "0"
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [target_group_arns]
  }
}
