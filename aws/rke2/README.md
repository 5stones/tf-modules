# RKE2 in AWS

Creates a [RKE2](https://rke2.io/) cluster using an autoscaling group for agents, where each instance installs RKE2 on launch and then joins the cluster.

```
module "basic-rke2" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/rke2"

  # required config
  name     = "Testing RKE2"
  region   = "us-east-1"
  key_name = "somekey"
  subnets  = [
    "subnet-12345678",
    "subnet-9abcef12",
  ]
}

# recommended for high-availability
module "ha-rke2" {
  source = "git::https://gitlab.com/5stones/tf-modules//aws/rke2"

  name     = "HA RKE2"
  region   = "us-east-1"
  key_name = "somekey"
  subnets  = [
    "subnet-12345678",
    "subnet-9abcef12",
  ]

  tls_san   = ["rke2.local"]
  servers   = 3
  agents    = [1, 3]

  dedicated_control_plane = true
}
```


## Instance Size

The defaults allocate 20G of disk space and 4G of ram (t3a.medium).
It can run with as little as 1G of ram (micro) but leaves very little resources for a workload.


## Autoscaling

If the in-tree cloud provider is used, the [cluster autoscaler](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler/cloudprovider/aws) will be configured.
