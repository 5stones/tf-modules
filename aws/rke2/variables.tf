variable "name" {
  description = "The name of the launch template, autoscaling group, role, and security group"
}

variable "cluster_id" {
  description = "The K8s cluster id used by the AWS cloud provider to identify AWS resources (defaults to the name with dashes)"
  default     = ""
}

variable "region" {
  description = "AWS region"
}

variable "key_name" {
  description = "ssh key name for access to instances"
}

variable "subnets" {
  description = "The VPC subnets to launch instances in"
}

variable "ubuntu_version" {
  default = "22.04"
}

variable "channel" {
  description = "Which channel of RKE2 to install"
  default     = "stable"
}

variable "rke2_version" {
  description = "Which version of RKE2 to install"
  default     = ""
}

variable "description" {
  description = "The description of the launch template"
  default     = ""
}

variable "instance_types" {
  description = ""
  default     = ["t3a.medium", "t3.medium"]
}

variable "volume_size" {
  default = 20
}

variable "tags" {
  default = {}
}

variable "vpc_security_group_ids" {
  default = []
}

variable "tls_san" {
  description = "Additional hostnames or IPs as a Subject Alternative Names in the TLS cert"
  default     = []
}

variable "ssh_keys" {
  description = "Additional ssh keys to add to authorized_keys"
  default     = []
}

variable "ssh_from" {
  default = {
    cidr_blocks      = []
    ipv6_cidr_blocks = []
  }
}

variable "manage_from" {
  default = {
    cidr_blocks      = []
    ipv6_cidr_blocks = []
  }
}

variable "http_from" {
  default = {
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

variable "monitoring" {
  description = "Launch template's enhanced monitoring"
  default     = false
}

variable "enabled_metrics" {
  default = [
    "GroupInServiceInstances",
  ]
}

variable "on_demand_base_capacity" {
  default = 0
}

variable "on_demand_percentage_above_base_capacity" {
  default = 0
}

variable "servers" {
  default = 1
  validation {
    condition     = var.servers % 2 == 1
    error_message = "An odd number of servers is required."
  }
}

variable "agents" {
  default = [0, 3]
}

variable "additional_agents" {
  description = "Create additional autoscaling groups of agents. ex: { Large = { size_range = [1, 3], instance_types = [...] } }"
  default     = {}
}

variable "dedicated_control_plane" {
  default = false
}

variable "cloud_provider" {
  default     = "external"
  description = "\"aws\" to use the old in-tree provider before 1.27"
}

variable "ecr_credential_provider_version" {
  default = "1.27.4"
  description = <<-DESCRIPTION
  What version of the ECR Credential Provider to install, which is necessary when using ECR images.

  This uses the binaries from
  https://console.cloud.google.com/storage/browser/k8s-artifacts-prod/binaries/cloud-provider-aws
  which is not yet documented but described in this comment:
  https://github.com/kubernetes/cloud-provider-aws/pull/568#issuecomment-1510407037
  DESCRIPTION
}

variable "apply_manifests" {
  default     = {}
  description = "Map of manifest name to base64gzip files to automatically kubectl apply on cluster creation"
}

variable "cluster_autoscaler_version" {
  default = "1.21.0"
}

variable "profile" {
  default = "cis-1.5"
}
