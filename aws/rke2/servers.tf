locals {
  server_taints = var.dedicated_control_plane ? ["CriticalAddonsOnly=true:NoExecute"] : []

  server_runcmd = <<-RUNCMD
  - curl -sfL https://get.rke2.io | ${local.install_env} sh -
  - cp -f /usr/local/share/rke2/rke2-cis-sysctl.conf /etc/sysctl.d/60-rke2-cis.conf
  - /usr/local/bin/install-ecr-credential-provider.sh ${var.ecr_credential_provider_version}
  - systemctl restart --no-block systemd-sysctl
  - systemctl enable rke2-server.service
  - systemctl start --no-block rke2-server.service
  - ln -s /var/lib/rancher/rke2/bin/kubectl /usr/local/bin/
  - echo "KUBECONFIG=/etc/rancher/rke2/rke2.yaml" >> /etc/environment
  RUNCMD

  manifests = merge(
    var.cloud_provider == "external" ? { "aws-cloud-controller-manager" = base64gzip(file("${path.module}/manifests/aws-cloud-controller-manager.yaml")) } : {},
    var.apply_manifests,
  )
  manifest_format = <<-FORMAT
  - path: /var/lib/rancher/rke2/server/manifests/%s.yaml
    encoding: gzip+b64
    content: %s
  FORMAT
  manifest_blocks = join("\n", formatlist(local.manifest_format, keys(local.manifests), values(local.manifests)))

  # TODO move to another module
  autoscaling_script = <<-SH
  mkdir -p /var/lib/rancher/rke2/server/manifests
  curl -s https://raw.githubusercontent.com/kubernetes/autoscaler/cluster-autoscaler-${var.cluster_autoscaler_version}/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml \
    | sed \
      -e 's|k8s.gcr.io/autoscaling/cluster-autoscaler:v[0-9.]*|k8s.gcr.io/autoscaling/cluster-autoscaler:v${var.cluster_autoscaler_version}|' \
      -e 's|replicas: .*|replicas: ${var.agents[0] == var.agents[1] ? 0 : 1}|' \
      -e 's|--node-group-auto-discovery=.*|--node-group-auto-discovery=asg:tag=kubernetes.io/cluster/${local.cluster_id}|' \
      -e 's|path: "*/etc/ssl/certs/ca-bundle.crt"*|path: /etc/ssl/certs/ca-certificates.crt|' \
      -e '$a\      nodeSelector: {"node-role.kubernetes.io/master": "true"}' \
      -e '$a\      tolerations: [{"key":"CriticalAddonsOnly", "operator":"Exists"}]' \
    > /var/lib/rancher/rke2/server/manifests/cluster-autoscaler.yaml
  SH

  server_instances = concat([aws_instance.initial], aws_instance.servers)
}

# Initial RKE2 server that includes automatically install manifests
resource "aws_instance" "initial" {
  ami           = data.aws_ami.ubuntu[local.is_arm ? "arm64" : "amd64"].id
  instance_type = var.instance_types[0]

  key_name   = var.key_name
  monitoring = var.monitoring

  iam_instance_profile = aws_iam_instance_profile.main["Server"].name
  ebs_optimized        = true

  root_block_device {
    volume_size = var.volume_size
  }

  subnet_id              = var.subnets[0]
  vpc_security_group_ids = concat([aws_security_group.node.id, aws_security_group.server.id], var.vpc_security_group_ids)

  tags = merge(local.tags, {
    Name = "${var.name} Server 0"
  })
  volume_tags = merge(local.tags, {
    Name = "${var.name} Server 0"
  })

  user_data = <<-USERDATA
  ${local.base_cloud_config}

  write_files:
  - path: /etc/rancher/rke2/config.yaml
    permissions: "0600"
    content: |
      token: ${random_password.token.result}
      profile: ${var.profile}
      write-kubeconfig-mode: "0644"
      cloud-provider-name: ${var.cloud_provider}
      tls-san: ${jsonencode(var.tls_san)}
      node-taint: ${jsonencode(local.server_taints)}
      kubelet-arg:
        - system-reserved-cgroup=/system.slice
        - system-reserved=cpu=170m,memory=700Mi,ephemeral-storage=2200Mi
        - eviction-hard=memory.available<100Mi
        - image-credential-provider-bin-dir=/usr/local/bin/k8s-credential-providers
        - image-credential-provider-config=/etc/rancher/rke2/ecr-config.yaml
  ${local.common_files}
  ${local.manifest_blocks}

  ${local.base_runcmd}
  ${var.cloud_provider != "" ? "- echo '${base64gzip(local.autoscaling_script)}' | base64 -d | gunzip | sh -" : ""}
  ${local.server_runcmd}
  USERDATA

  lifecycle {
    ignore_changes = [ami, disable_api_termination, user_data, source_dest_check, instance_type, root_block_device]
  }
}

# Additional servers which will connect to the inital server
resource "aws_instance" "servers" {
  count = var.servers - 1

  ami           = data.aws_ami.ubuntu[local.is_arm ? "arm64" : "amd64"].id
  instance_type = var.instance_types[0]

  key_name   = var.key_name
  monitoring = var.monitoring

  iam_instance_profile = aws_iam_instance_profile.main["Server"].name
  ebs_optimized        = true

  root_block_device {
    volume_size = var.volume_size
  }

  subnet_id              = var.subnets[(count.index + 1) % length(var.subnets)]
  vpc_security_group_ids = concat([aws_security_group.node.id, aws_security_group.server.id], var.vpc_security_group_ids)

  tags = merge(local.tags, {
    Name = "${var.name} Server ${count.index + 1}"
  })
  volume_tags = merge(local.tags, {
    Name = "${var.name} Server ${count.index + 1}"
  })

  user_data = <<-USERDATA
  ${local.base_cloud_config}

  write_files:
  - path: /etc/rancher/rke2/config.yaml
    permissions: "0600"
    content: |
      server: https://${aws_instance.initial.private_ip}:9345
      token: ${random_password.token.result}
      profile: ${var.profile}
      write-kubeconfig-mode: "0644"
      cloud-provider-name: ${var.cloud_provider}
      tls-san: ${jsonencode(var.tls_san)}
      node-taint: ${jsonencode(local.server_taints)}
      kubelet-arg:
        - system-reserved-cgroup=/system.slice
        - system-reserved=cpu=170m,memory=700Mi,ephemeral-storage=2200Mi
        - eviction-hard=memory.available<100Mi
        - image-credential-provider-bin-dir=/usr/local/bin/k8s-credential-providers
        - image-credential-provider-config=/etc/rancher/rke2/ecr-config.yaml
  ${local.common_files}

  ${local.base_runcmd}
  ${local.server_runcmd}
  USERDATA

  lifecycle {
    ignore_changes = [ami, disable_api_termination, user_data, source_dest_check, instance_type, root_block_device]
  }
}
