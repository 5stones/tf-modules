locals {
  resource_name = replace(var.name, "/[^\\w+=,.@]+/", "-")
  cluster_id    = var.cluster_id != "" ? var.cluster_id : lower(local.resource_name)
  base_tags = {
    Name = var.name
    # used by the aws cloud provider
    "kubernetes.io/cluster/${local.cluster_id}" = "owned"
  }
  tags = merge(local.base_tags, var.tags)

  is_arm = length(regexall("(?:a1|gd?)\\.", var.instance_types[0])) > 0

  install_env = join(" ", concat(
    ["INSTALL_RKE2_CHANNEL=${var.channel}"],
    var.rke2_version != "" ? ["INSTALL_RKE2_VERSION=${var.rke2_version}"] : [],
  ))

  node_labels = join(" ", formatlist("--node-label \"%s\"", [
  ]))

  base_cloud_config = <<-USERDATA
  #cloud-config
  package_update: true
  package_upgrade: true
  ssh_authorized_keys:
  ${join("\n", formatlist("- %s", var.ssh_keys))}
  USERDATA

  base_runcmd = <<-RUNCMD
  runcmd:
  - AWS_AZ=$(ec2metadata --availability-zone)
  - export AWS_DEFAULT_REGION=`echo $AWS_AZ | sed 's/[a-z]$//'`
  - echo "AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION" >> /etc/environment
  - 'echo "node-name: $(hostname -f)" >> /etc/rancher/rke2/config.yaml'
  - echo "vm.max_map_count=262144" > /etc/sysctl.d/60-elasticsearch.conf
  - useradd -r -c "etcd user" -s /sbin/nologin -M etcd
  RUNCMD

  common_files = <<-FILES
  - path: /etc/rancher/rke2/ecr-config.yaml
    permissions: "0644"
    content: |
      apiVersion: kubelet.config.k8s.io/v1
      kind: CredentialProviderConfig
      providers:
        - name: ecr-credential-provider
          matchImages:
            - "*.dkr.ecr.*.amazonaws.com"
            - "*.dkr.ecr.*.amazonaws.com.cn"
            - "*.dkr.ecr-fips.*.amazonaws.com"
            - "*.dkr.ecr.us-iso-east-1.c2s.ic.gov"
            - "*.dkr.ecr.us-isob-east-1.sc2s.sgov.gov"
          defaultCacheDuration: "12h"
          apiVersion: credentialprovider.kubelet.k8s.io/v1
  - path: /usr/local/bin/install-ecr-credential-provider.sh
    permissions: "0744"
    encoding: gzip+b64
    content: ${base64gzip(file("${path.module}/bin/install-ecr-credential-provider.sh"))}
  FILES
}

resource "random_password" "token" {
  length  = 32
  numeric = false
  special = false
}

data "aws_vpc" "main" {
  id = data.aws_subnet.main[0].vpc_id
}

data "aws_subnet" "main" {
  count = 1
  id    = var.subnets[count.index]
}

# find the most recent ubuntu image
data "aws_ami" "ubuntu" {
  for_each = toset(["amd64", "arm64"])

  most_recent = true
  owners      = ["099720109477"] # Canonical

  name_regex = "^ubuntu/images/hvm-ssd/ubuntu-[a-z]+-${var.ubuntu_version}[.0-9]*-${each.key}-server-"

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
