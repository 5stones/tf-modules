output "servers" {
  value = local.server_instances
}

output "aws_launch_template" {
  value = aws_launch_template.agent
}

output "aws_autoscaling_group" {
  value = aws_autoscaling_group.agents
}

output "aws_iam_roles" {
  value = aws_iam_role.main
}
