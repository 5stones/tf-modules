variable "autoscaling_group_name" {
}

variable "min_size" {
  default = 1
  description = "The min_size of the autoscaling group during online hours"
}

variable "online_size" {
  default = 1
}

variable "start_hour" {
  default = 12
  description = "UTC hour to set desired_capacity to the online_size"
}

variable "end_hour" {
  default = 0
  description = "UTC hour to set desired_capacity to 0"
}

variable "warmup" {
  default = false
  description = "Start one instance 5 minutes before the online hour, to help with placement of larger ECS tasks"
}
