resource "aws_autoscaling_schedule" "warmup" {
  count = var.warmup && var.online_size > 1 ? 1 : 0
  scheduled_action_name  = "Warm-up"
  autoscaling_group_name = var.autoscaling_group_name
  min_size               = var.min_size
  desired_capacity       = 1
  max_size               = -1
  recurrence             = "55 ${var.start_hour - 1} * * MON-FRI"
}

resource "aws_autoscaling_schedule" "online" {
  scheduled_action_name  = "Online Hours"
  autoscaling_group_name = var.autoscaling_group_name
  min_size               = var.min_size
  desired_capacity       = var.online_size
  max_size               = -1
  recurrence             = "0 ${var.start_hour} * * MON-FRI"
}

resource "aws_autoscaling_schedule" "offline" {
  scheduled_action_name  = "Offline Hours"
  autoscaling_group_name = var.autoscaling_group_name
  min_size               = 0
  desired_capacity       = 0
  max_size               = -1
  recurrence             = "0 ${var.end_hour} * * *"
}
