terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.45"
    }
  }
}

locals {
  install_env = join(" ", concat(
    ["INSTALL_RKE2_CHANNEL=${var.channel}"],
    var.rke2_version != "" ? ["INSTALL_RKE2_VERSION=${var.rke2_version}"] : [],
  ))

  manifests = merge(
    {
      "rke2-cilium-config" = templatefile("${path.module}/manifests/rke2-cilium-config.yaml", {
        cluster_cidrs = var.cluster_cidrs
      })
    },
    var.apply_manifests,
  )
  manifest_format = <<-FORMAT
  - path: /var/lib/rancher/rke2/server/manifests/%s.yaml
    encoding: gzip+b64
    content: %s
  FORMAT
  manifest_blocks = join("\n", formatlist(local.manifest_format, keys(local.manifests), values(local.manifests)))

  # ipv6 prefix for Unique Local Addresses (ula): fd##:####:####
  ipv6_prefix  = "fd${substr(random_id.ula.hex, 0, 2)}:${substr(random_id.ula.hex, 2, 4)}:${substr(random_id.ula.hex, 6, 4)}"
  # ipv6 service range (service-cidr)
  ipv6_network = "${local.ipv6_prefix}::/48"
}

resource "random_password" "token" {
  length  = 32
  numeric = false
  special = false
}

# for local ipv6 network
resource "random_id" "ula" {
  byte_length = 5
}

resource "hcloud_placement_group" "main" {
  name   = var.name
  labels = var.labels
  type   = "spread"
}
