variable "name" {
  description = "The name of the launch template, autoscaling group, role, and security group"
}

variable "labels" {
  default = {}
}

variable "location" {
  description = "The location name to create the server in. (ash for us-east, hil for us-west)"
}

variable "datacenter" {
  description = "The datacenter name to create the server in."
  default     = null
}

variable "subnet" {
  description = "Expects a network subnet or object with network_id and ip_range"
}

variable "cluster_cidrs" {
  default = ["10.42.0.0/16", "10.43.0.0/16"]
}

variable "ubuntu_version" {
  default = "24.04"
}

variable "channel" {
  description = "Which channel of RKE2 to install"
  default     = "stable"
}

variable "rke2_version" {
  description = "Which version of RKE2 to install"
  default     = ""
}

variable "profile" {
  default = "cis"
}

variable "tls_san" {
  description = "Additional hostnames or IPs as a Subject Alternative Names in the TLS cert"
  default     = []
}

variable "ssh_keys" {
  description = "ssh keys to add to authorized_keys"
  default     = []
}

variable "ssh_from" {
  default = ["0.0.0.0/0", "::/0"]
}

variable "manage_from" {
  default = []
}

variable "http_from" {
  default = ["0.0.0.0/0", "::/0"]
}

variable "firewall_ids" {
  description = "Additional firewall ids"
  default     = []
}

variable "servers" {
  default = {
    count       = 1
    server_type = "cpx21"
    node_labels = []
    node_taints = []
    dedicated   = false
  }
  validation {
    condition     = try(var.servers.count, 1) % 2 == 1
    error_message = "An odd number of servers is required."
  }
}

variable "agents" {
  description = "Groups of agent nodes"
  default = {
    #agent = { count = 1, server_type = "cpx21", cidr = "10.x.4.0/22"  }
  }
}

variable "apply_manifests" {
  default     = {}
  description = "Map of manifest filename to content, which rke2 will automatically apply"
}
