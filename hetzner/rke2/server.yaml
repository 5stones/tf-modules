#cloud-config
package_update: true
package_upgrade: true
packages:
  - jq

users:
  - name: etcd
    system: true
    shell: /usr/sbin/nologin

write_files:
  - path: /etc/environment
    append: true
    content: |
      KUBECONFIG=/etc/rancher/rke2/rke2.yaml
      CRI_CONFIG_FILE=/var/lib/rancher/rke2/agent/etc/crictl.yaml

  - path: /etc/sysctl.d/60-elasticsearch.conf
    content: |
      vm.max_map_count=262144

  - path: /etc/rancher/rke2/config.yaml
    permissions: "0600"
    content: |
      %{~ if server != null ~}
      server: "https://${server}:9345"
      %{~ endif ~}
      token: "${token}"
      profile: "${profile}"
      write-kubeconfig-mode: "0644"
      advertise-address: ${ipv4}
      tls-san: ${jsonencode(concat(tls_san, [ipv4]))}
      cloud-provider-name: external
      kube-apiserver-arg:
        - kubelet-preferred-address-types=Hostname,InternalDNS,InternalIP,ExternalDNS,ExternalIP
      kubelet-arg:
        # kube-reserved=cpu=70m,memory=500Mi,ephemeral-storage=300Mi
        - system-reserved=cpu=170m,memory=700Mi,ephemeral-storage=2000Mi
        - system-reserved-cgroup=/system.slice
        - eviction-hard=memory.available<100Mi
      disable:
        - cloud-controller
        - rke2-canal
      cni: cilium
      cluster-cidr: "${cluster_cidrs[0]},${ipv6_prefix}:42::/56"
      service-cidr: "${cluster_cidrs[1]},${ipv6_prefix}:43::/112"
      node-label: ${jsonencode(node_labels)}
      node-taint: ${jsonencode(node_taints)}

  - path: /usr/local/bin/etcdctl
    permissions: "0755"
    content: |
      #!/bin/bash
      etcdcontainer=$(crictl ps --label io.kubernetes.container.name=etcd --quiet)
      exec crictl exec "$etcdcontainer" sh -c "ETCDCTL_ENDPOINTS='https://127.0.0.1:2379' ETCDCTL_CACERT='/var/lib/rancher/rke2/server/tls/etcd/server-ca.crt' ETCDCTL_CERT='/var/lib/rancher/rke2/server/tls/etcd/server-client.crt' ETCDCTL_KEY='/var/lib/rancher/rke2/server/tls/etcd/server-client.key' ETCDCTL_API=3 etcdctl "'"$@"' etcdctl "$@"

  %{~ if server == null ~}
  - path: /root/hccm-setup.sh
    permissions: "0700"
    content: |
      #!/bin/bash
      read -p 'Hetzner read/write API Token: ' token
      kubectl -n kube-system create secret generic hcloud --from-literal=network='${network}' --from-literal=token="$token"
      kubectl apply -f /root/hccm.yaml

  - path: /root/hccm.yaml
    permissions: "0644"
    content: |
      apiVersion: helm.cattle.io/v1
      kind: HelmChart
      metadata:
        name: hccm
        namespace: kube-system
      spec:
        bootstrap: true
        repo: https://charts.hetzner.cloud
        chart: hcloud-cloud-controller-manager
        valuesContent: |
          env:
            HCLOUD_INSTANCES_ADDRESS_FAMILY:
              value: dualstack
          monitoring:
            enabled: true
          networking:
            enabled: true
            clusterCIDR: "${cluster_cidrs[0]},${ipv6_prefix}:42::/56"

  %{~ for filename, manifest in manifests ~}
  - path: /var/lib/rancher/rke2/server/manifests/${filename}.yaml
    encoding: gzip+b64
    content: "${base64gzip(manifest)}"

  %{~ endfor ~}
  %{~ endif ~}
runcmd:
  - "ipv4=$(ip -4 addr show | awk '/cilium/ {next} /inet 10\\./ {print $4; exit}')"
  - "ipv6=$(ip -6 -j addr show eth0 scope global | jq -r .[0].addr_info[0].local)"
  - "echo \"node-ip: $ipv4,$ipv6\" >> /etc/rancher/rke2/config.yaml"
  - "sed -i 's/^127.0.1.1 /${ipv4} /g' /etc/hosts /etc/cloud/templates/hosts.debian.tmpl"
  - curl -sfL https://get.rke2.io | ${install_env} sh -
  - ln -s /var/lib/rancher/rke2/bin/kubectl /var/lib/rancher/rke2/bin/crictl /var/lib/rancher/rke2/bin/ctr /usr/local/bin/
  - cp -f /usr/local/share/rke2/rke2-cis-sysctl.conf /etc/sysctl.d/60-rke2-cis.conf
  - systemctl restart --no-block systemd-sysctl
  - systemctl enable rke2-server.service
  - systemctl start --no-block rke2-server.service
