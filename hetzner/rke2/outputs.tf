output "primary_ips" {
  # without `nonsensitive(sensitive(` gives an error that it can't output sensitive data
  # with just `nonsensitive`, it gives an error that it's not sensitive :/
  value = nonsensitive(sensitive({
    ipv4 = hcloud_server.primary.ipv4_address
    ipv6 = hcloud_server.primary.ipv6_address
  }))
}

output "servers" {
  value = concat(
    [hcloud_server.primary],
    [for k, v in hcloud_server.servers : v],
  )
}

output "agents" {
  value = hcloud_server.agents
}

output "firewall" {
  value = hcloud_firewall.main
}
