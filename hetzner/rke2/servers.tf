locals {
  primary_ip = tolist(hcloud_server.primary.network)[0].ip
}

resource "hcloud_server" "primary" {
  name   = "${var.name}-server-1"
  labels = var.labels

  server_type = try(var.servers.server_type, "cpx21")
  image       = "ubuntu-${var.ubuntu_version}"

  location           = var.location
  datacenter         = var.datacenter
  placement_group_id = hcloud_placement_group.main.id

  firewall_ids = concat([hcloud_firewall.main.id], var.firewall_ids)
  ssh_keys     = var.ssh_keys

  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }

  network {
    network_id = var.subnet.network_id
    ip         = cidrhost(var.subnet.ip_range, 2)
  }

  user_data = templatefile("${path.module}/server.yaml", {
    server          = null
    profile         = var.profile
    tls_san         = var.tls_san
    install_env     = local.install_env
    token           = random_password.token.result
    network         = var.subnet.network_id
    ipv4            = cidrhost(var.subnet.ip_range, 2)
    ipv6_prefix     = local.ipv6_prefix
    cluster_cidrs   = var.cluster_cidrs
    manifest_blocks = local.manifest_blocks
    node_labels     = try(var.servers.node_labels, [])
    node_taints = concat(
      try(var.servers.dedicated, false) == true ? ["CriticalAddonsOnly=true:NoExecute"] : [],
      try(var.servers.node_taints, []),
    )
    manifests = var.apply_manifests
  })

  #delete_protection  = true
  #rebuild_protection = true

  depends_on = [var.subnet]

  lifecycle {
    # prevent recreation on version updates
    ignore_changes = [
      user_data,
    ]
  }
}

resource "hcloud_server" "servers" {
  for_each = { for n in range(2, try(var.servers.count, 1) + 1) : n => {
    name = "${var.name}-server-${n}"
    ip   = cidrhost(var.subnet.ip_range, 1 + n)
  }}

  name   = each.value.name
  labels = var.labels

  server_type = try(var.servers.server_type, "cpx21")
  image       = "ubuntu-${var.ubuntu_version}"

  location           = var.location
  datacenter         = var.datacenter
  placement_group_id = hcloud_placement_group.main.id

  firewall_ids = concat([hcloud_firewall.main.id], var.firewall_ids)
  ssh_keys     = var.ssh_keys

  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }

  network {
    network_id = var.subnet.network_id
    ip         = each.value.ip
  }

  user_data = templatefile("${path.module}/server.yaml", {
    server          = local.primary_ip
    profile         = var.profile
    tls_san         = var.tls_san
    install_env     = local.install_env
    token           = random_password.token.result
    network         = var.subnet.network_id
    ipv4            = each.value.ip
    ipv6_prefix     = local.ipv6_prefix
    cluster_cidrs   = var.cluster_cidrs
    manifest_blocks = local.manifest_blocks
    node_labels     = try(var.servers.node_labels, [])
    node_taints = concat(
      try(var.servers.dedicated, false) == true ? ["CriticalAddonsOnly=true:NoExecute"] : [],
      try(var.servers.node_taints, []),
    )
    manifests = {}
  })

  #delete_protection  = true
  #rebuild_protection = true

  lifecycle {
    # prevent recreation on version updates
    ignore_changes = [
      user_data,
    ]
  }
}
