resource "hcloud_server" "agents" {
  # loop over flattened map {"group1-1": { n: 1, ... }, ...}
  for_each = merge(
    [for k, v in var.agents :
      { for n in range(try(v.count, 1)) :
        "${k}-${n + 1}" => merge(v, { n = n + 1 })
      }
    ]...
  )

  name = "${var.name}-${each.key}"
  labels = merge(
    var.labels,
    try(each.value.labels, {}),
  )

  server_type = try(each.value.server_type, "cpx21")
  image       = "ubuntu-${var.ubuntu_version}"

  location           = try(each.value.location, var.location)
  datacenter         = try(each.value.datacenter, var.datacenter)
  placement_group_id = hcloud_placement_group.main.id

  firewall_ids = concat([hcloud_firewall.main.id], var.firewall_ids)
  ssh_keys     = var.ssh_keys

  public_net {
    ipv4_enabled = try(each.value.public_ipv4, true)
    ipv6_enabled = true
  }

  network {
    network_id = var.subnet.network_id
    ip         = try(cidrhost(each.value.cidr, each.value.n), null)
  }

  user_data = templatefile("${path.module}/agent.yaml", {
    server      = local.primary_ip
    profile     = var.profile
    install_env = local.install_env
    token       = random_password.token.result
    node_labels = try(each.value.node_labels, [])
    node_taints = concat(
      try(each.value.node_taints, []),
    )
  })

  lifecycle {
    # prevent recreation on version updates
    ignore_changes = [
      user_data,
    ]
  }
}
