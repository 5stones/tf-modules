# Hetzner RKE2 Cluster

## Basic Setup

```
# define network for rke2 cluster
resource "hcloud_network" "main" {
  name     = "my-cluster"
  ip_range = "10.x.0.0/16"
}

# define subnet for rke2 cluster nodes
resource "hcloud_network_subnet" "nodes" {
  type         = "cloud"
  network_id   = data.hcloud_network.main.id
  network_zone = "us-east"
  # 4094 ips for nodes (10.x.0.2 - 10.x.15.254)
  ip_range     = "10.x.0.0/20"
}

module "rke2" {
  source = "git::https://gitlab.com/5stones/tf-modules//hetzner/rke2"

  name     = "my-cluster"
  location = "ash"
  subnet   = hcloud_network_subnet.nodes
  ssh_from = [
    "x.x.x.x", # your external ip
  ]
  ssh_keys = "your-key-name"

  agents = {
    agent = {
      count = 0
    }
  }
}

# display public ip to ssh into
output "rke2_primary_ips" {
  value = module.rke2.primary_ips
}
```

note: this configuration uses the default cidrs 10.42.0.0/16 and 10.43.0.0/16,
which could conflict with other internal ips, and prevents having more than one cluster in the a Hetzner network.

### hcloud-cloud-controller-manager setup

After the primary server is created, nodes will be uninitialized until hccm is set up.
An API key needs to be manually created and added to a secret (there's no terraform resource to create one).

```bash
ssh root@{primary server's public ip}

# make sure the rke2 is up and running
kubectl get no

# add the token and install hccm using a Helm CRD
./hccm-setup.sh
```

## Oddities and the workarounds

- installing hccm through the helm has a few issues
  - 2 clusters can't use the same clusterCIDR in the same hetzner network, even with different subnets
    - with networking enabled, it automatically adds routes for node pods in clusterCIDR directly to hetzner's network
    - a different cidr or hetzner network needs to be used (see IPv4 Network Suggestion below)
  - with networking enabled and dual-stack, hccm tries to add ipv6 routes, and has errors, but it seems to still function
    - https://github.com/hetznercloud/hcloud-cloud-controller-manager/issues/659
    - with dual-stack disabled, nginx will not respond to ipv6 requests
  - networking has to be enabled, because the public ips go through Hetzner's firewall
    - there's no documented ipv4 or ipv6 range for an account/project so we can't allow necessary traffic between nodes on public ips
    - an alternative would be to disable Hetzner's firewall, and handle it with the OS, but that seems less secure
  - it sometimes has an issue with the metrics port, and we might need `monitoring.enabled=false`

- the ip situation for the default behavior of rke2/k8s is completely broken, requiring workarounds
  - the ip of the default route (public ip) is used as the only internal ip for the node and all internal configuration, causing various communication errors
  - once hccm is installed, it fixes the ips on the nodes, but by that point, rke2 has already locked in that ip for the apiserver and certificates
  - all ip and node name configuration must match what the cloud controller manager will set, before rke2 is first started
  - changing /etc/hosts so that the hostname points to the internal ip fixes the internal ip on the node, but not the certs
  - the config for `node-ip` seems to fix the kublet cert, but nothing else
  - the ip needs to be added to `tls-san` for the api server cert
  - the ip needs to be added to `advertise-address` so that the internal ip is used to talk to the api server

- rke2 uses a cgroup in /system.slice so we can't set kube-reserved separately from system-reserved


## IPv4 Network Suggestion

The cluster needs 3 cidrs, and since hccm does route ipv4s for pods when networking is enabled, it makes sense to include those in that network space.
A /16 CIDR will support up to 128 nodes and 16k services, with some unallocated space for special cases.
The `x` below should be a random number between 0 and 254, as unique in your organization as possible.

- 10.x.0.0/20 (4094 ips: 10.x.0.2 - 10.x.15.254): nodes
  - 10.x.0.2 - 10.x.0.7: control-plane (1, 3, or 5 servers)
  - 10.x.1.1/25 (126 ips: 10.x.1.1 - 10.x.1.126): workers (agent group 1)
  - 10.x...
  - 10.x.15.129/25 (126 ips: 10.x.15.129 - 10.x.15.254): workers (agent group 30)
- 10.x.16.0/20 (4094 ips: 10.x.16.2 - 10.x.31.254): unallocated
- 10.x.32.0/20 (4094 ips: 10.x.32.2 - 10.x.47.254): unallocated
- 10.x.48.0/20 (4094 ips: 10.x.48.2 - 10.x.63.254): unallocated
- 10.x.64.0/18 (16,382 ips: 10.x.64.2 - 10.x.127.254): service ips
- 10.x.128.0/17 (32,766 ips: 10.x.128.2 - 10.x.255.254): pod ips

```
# define network for rke2 cluster
resource "hcloud_network" "main" {
  name     = "my-cluster"
  ip_range = "10.x.0.0/16"
}

# define subnet for rke2 cluster nodes
resource "hcloud_network_subnet" "nodes" {
  type         = "cloud"
  network_id   = data.hcloud_network.main.id
  network_zone = "us-east"
  # 4094 ips for nodes (10.x.0.2 - 10.x.15.254)
  # 32 groups of 127 ips
  ip_range     = "10.x.0.0/20"
}

module "rke2" {
  source = "git::https://gitlab.com/5stones/tf-modules//hetzner/rke2?ref=v6.3.0"

  name     = "my-cluster"
  location = "ash"
  subnet   = hcloud_network_subnet.nodes
  ssh_from = [
    "x.x.x.x", # your external ip
  ]
  ssh_keys = "your-key-name"

  cluster_cidrs = [
    # pod ips: 32,766 (10.x.128.2 - 10.x.255.254)
    "10.x.128.0/17",
    # service ips: 16,382 (10.x.64.2 - 10.x.127.254)
    "10.x.64.0/18",
  ]

  agents = {
    agent = {
      count = 1
      # 126 agent ips: (10.x.1.1 - 10.x.1.126)
      cidr  = "10.x.1.0/25"
    }
    # up to 29 other agent groups
  }
}

# display public ip to ssh into
output "rke2_primary_ips" {
  value = module.rke2.primary_ips
}
```


## TODO

- configure agents without public ipv4
  - we need a nat to even install rke2 (uses github.com)
  - should we use the control-plane as a nat?

- load balancing?

- fix dns warnings because of too many name servers
  `resolv-conf: /run/systemd/resolve/resolv.conf`

- verify that ipv6 traffic is working, because the only ipv6 address is a public ip and is restricted by the firewall rules
  - the hetzner network doesn't apply to ipv6 and there is no option for an ipv6 network
  - we may be able to add each ipv6 cidr to the firewall when adding servers, but wouldn't work for future autoscaling

- ipv6 pod ranges (cluster-cidr)
  - can/should we use the node's ipv6 range for its pods? (but not services)
  - rke2 agents don't have a config for the pods ip range
  - https://github.com/tibordp/terraform-hcloud-dualstack-k8s
  - hccm attempts to add routes for ipv6 and can't
