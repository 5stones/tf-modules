# Deploy an oauth2-proxy and ingress to securely expose a service with authentication

```HCL
module "app" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/oauth2-proxy"

  # your fqdn
  ingress_host = "something.example.com"

  # The kubernetes service to proxy
  upstream = kubernetes_service.main
  # or
  #upstream = {
  #  namespace = "your-namespace"
  #  name      = "your-service"
  #}

  # if your service uses a port other than 80, like 3000
  #port = 3000

  # your identity provider config (https://oauth2-proxy.github.io/oauth2-proxy/configuration/providers/)
  config = {
    # keycloak example (https://oauth2-proxy.github.io/oauth2-proxy/configuration/providers/keycloak_oidc)
    PROVIDER        = "keycloak-oidc"
    OIDC_ISSUER_URL = "https://<keycloak host>/realms/<your realm>"
    CLIENT_ID       = "<your client's id>"
    CLIENT_SECRET   = "<your client's secret>"

    # Optional, require a client role when logging in
    ALLOWED_ROLES   = "<client id>:<client role name>"
  }
}
```
