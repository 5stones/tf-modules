variable "ingress_host" {
}

variable "upstream" {
  description = "A kubernetes_service resource, or an object with name and namespace"
}
variable "port" {
  description = "Which port of the upstream service to proxy"
  default     = 80
}

variable "config" {
  description = "Env variables to configure the IDP, without the OAUTH2_PROXY_ prefix, which will be saved in a secret"
}
variable "options" {
  description = "Command line options to pass in"
  default = [
    "--reverse-proxy",
    "--request-logging=false",
    "--session-cookie-minimal",
    "--silence-ping-logging",
    "--skip-provider-button",
  ]
}

variable "name" {
  description = "Name of the deployment, service, ingress, and secret (by default, it will be the service name + \"-oauth2-proxy\""
  default     = null
}

variable "oauth2_proxy_repo" {
  default = "quay.io/oauth2-proxy/oauth2-proxy"
}

variable "oauth2_proxy_version" {
  default = "7.7.1"
}

variable "image_pull_secret" {
  default = ""
}

variable "arch" {
  description = "What archtectures the image supports"
  default     = ["amd64", "arm64"]
  type        = list(string)
  nullable    = true
}

variable "autoscaling_range" {
  description = "The number of replicas to autoscale between"
  default     = [1, 1]
}

variable "resources" {
  default = {
    requests = {
      cpu    = "10m"
      memory = "10Mi"
    }
    limits = {}
  }
}

variable "additional_labels" {
  default = {}
}

variable "ingress_annotations" {
  default = {}
}

variable "pod_annotations" {
  description = "annotations for pods such as `fluentbit.io/parser = ...`"
  default     = {}
}

variable "env" {
  description = "Env variables to add to the container"
  default     = {}
}

variable "secrets" {
  description = "Map of additional secrets for env variables: { name = prefix }"
  default     = {}
}

variable "node_selectors" {
  description = "Required labels for nodes (list of { key, operator, values })"
  default     = []
}

variable "tolerations" {
  description = "What node taints to tolerate (list of toleration objects)"
  default     = []
}

variable "topology_spread_constraints" {
  default = [{
    topology_key       = "kubernetes.io/hostname"
    max_skew           = 1
    when_unsatisfiable = "ScheduleAnyway"
  }]
}

variable "cert_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation for a namespaced issuer."
  default     = ""
}

variable "cert_cluster_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation for a cluster-wide issuer."
  default     = ""
}
