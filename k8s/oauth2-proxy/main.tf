locals {
  metadata  = try(var.upstream.metadata.0, var.upstream)
  namespace = local.metadata.namespace
  name      = local.metadata.name
  labels    = try(local.metadata.labels, {})

  proxy_name = var.name != null ? var.name : "${local.name}-oauth2-proxy"
  proxy_labels = merge(
    {
      "app.kubernetes.io/name"       = "oauth2-proxy"
      "app.kubernetes.io/version"    = var.oauth2_proxy_version
      "app.kubernetes.io/part-of"    = try(local.labels["app.kubernetes.io/part-of"], local.name)
      "app.kubernetes.io/managed-by" = "terraform"
    },
    var.additional_labels
  )
}

module "app" {
  source = "../app"

  namespace = local.namespace
  name      = local.proxy_name

  additional_labels = local.proxy_labels
  pod_annotations   = var.pod_annotations

  image_pull_secret = var.image_pull_secret

  image = "${var.oauth2_proxy_repo}:v${var.oauth2_proxy_version}"
  arch  = var.arch

  autoscaling_range = var.autoscaling_range

  port       = 4180
  probe_path = "/ping"

  args = concat([
    "--http-address=0.0.0.0:4180",
    "--redirect-url=https://${var.ingress_host}/oauth2/callback",
    "--upstream=http://${local.name}.${local.namespace}.svc.cluster.local:${var.port}/",
  ], var.options)

  env = var.env
  secrets = merge(
    { (local.proxy_name) = "OAUTH2_PROXY_" },
    var.secrets,
  )

  resources = var.resources

  node_selectors              = var.node_selectors
  tolerations                 = var.tolerations
  topology_spread_constraints = var.topology_spread_constraints

  ingress_host        = var.ingress_host
  ingress_annotations = var.ingress_annotations
  cert_issuer         = var.cert_issuer
  cert_cluster_issuer = var.cert_cluster_issuer
}

resource "kubernetes_secret" "main" {
  metadata {
    namespace = local.namespace
    name      = local.proxy_name
    labels    = local.proxy_labels
  }
  data = merge(
    {
      COOKIE_SECRET = random_password.cookie_secret.result,
      EMAIL_DOMAINS = "*"
    },
    var.config,
  )
}

resource "random_password" "cookie_secret" {
  length           = 32
  override_special = "-_"
}
