variable "namespace" {
  default = "default"
}

variable "name" {
}

variable "image" {
}

variable "arch" {
  description = "What archtectures the image supports (ex: amd64, arm64)"
  default     = null
  type        = list(string)
  nullable    = true
}

variable "command" {
  description = "Overrides the command/entrypoint of the image"
  default     = null
}

variable "args" {
  description = "Overrides the command arguments"
  default     = null
}

variable "additional_labels" {
  default = {}
}

variable "autoscaling_range" {
  description = "The number of replicas to autoscale between"
  default     = [1, 1]
}

variable "autoscaler" {
  description = "How the horizontal pod autoscaler behaves (if any)"
  default = {
    target = 80
    scale_up = {
      # changed from the default to add some stabilization (3 data points instead of 1)
      stabilization_window_seconds = 45
      period_seconds               = 15
      percent                      = 100
      pods                         = 4
    }
    scale_down = {
      stabilization_window_seconds = 300
      period_seconds               = 15
      percent                      = 100
    }
  }
}

variable "resources" {
  default = {
    requests = {
      cpu    = "125m"
      memory = "128Mi"
    }
    limits = {
      memory = "256Mi"
    }
  }
}

variable "service_type" {
  default = "ClusterIP"
}

variable "service_labels" {
  default = {}
}

variable "port" {
  description = "The http port"
  default     = 3000
}

variable "probe_path" {
  description = "Uri path for readiness_probe and liveness_probe"
  default     = "/"
}

variable "env" {
  description = "Env variables to add to the container"
  default     = {}
}

variable "config_maps" {
  description = "Map of additional config maps for env variables: { name = prefix }"
  default     = {}
}

variable "secrets" {
  description = "Map of additional secrets for env variables: { name = prefix }"
  default     = {}
}

variable "change_cause" {
  default = null
}

variable "security_context" {
  default = {
    run_as_non_root = true
    seccomp_profile = {
      type = "RuntimeDefault"
    }
    capabilities = {
      drop = ["ALL"]
    }
  }
}

variable "run_as" {
  description = "Set to override the user and group of the container using a security context"
  default = {
    user  = null
    group = null
  }
}

variable "image_pull_secret" {
  default = ""
}

variable "node_selectors" {
  description = "Required labels for nodes (list of { key, operator, values })"
  default     = []
}

variable "tolerations" {
  description = "What node taints to tolerate (list of toleration objects)"
  default     = []
}

variable "topology_spread_constraints" {
  default = [{
    topology_key       = "kubernetes.io/hostname"
    max_skew           = 1
    when_unsatisfiable = "ScheduleAnyway"
  }]
}

variable "pod_annotations" {
  description = "annotations for pods such as `fluentbit.io/parser = nestjs`"
  default     = {}
}

variable "storage" {
  description = "Volumes to mount to all pods"
  default     = {}
  type = map(object({
    mount_path         = string
    storage            = string
    storage_class_name = optional(string, null)
    access_modes       = optional(list(string), ["ReadWriteMany"])
  }))
}

variable "ingress_host" {
  description = "If set, will create a basic ingress for the host."
  default     = ""
}

variable "ingress_annotations" {
  default = {}
}

variable "cert_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation for a namespaced issuer."
  default     = ""
}

variable "cert_cluster_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation for a cluster-wide issuer."
  default     = ""
}

variable "before_deploy" {
  description = "Jobs to run on deploy as a for_each with overrides (ex: { migrate = { args = [...], env = {...} } }"
  default     = {}
}

variable "cron_jobs" {
  description = "Cron jobs to schedule as a for_each with overrides (ex: { clear-cache = { schedule = \"0 0 * * *\", args = [...], env = {...} } }"
  default     = {}
}
