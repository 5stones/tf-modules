locals {
  ingress_annotations = merge(
    var.ingress_annotations,
    var.cert_issuer == "" ? {} : { "cert-manager.io/issuer" = var.cert_issuer },
    var.cert_cluster_issuer == "" ? {} : { "cert-manager.io/cluster-issuer" = var.cert_cluster_issuer }
  )
  tls = var.cert_issuer != "" || var.cert_cluster_issuer != ""
}

resource "kubernetes_ingress_v1" "main" {
  count = var.ingress_host == "" ? 0 : 1

  metadata {
    name      = var.name
    namespace = var.namespace
    labels    = local.labels

    annotations = local.ingress_annotations
  }
  spec {
    rule {
      host = var.ingress_host
      http {
        path {
          backend {
            service {
              name = var.name
              port {
                name = "http"
              }
            }
          }
        }
      }
    }
    dynamic "tls" {
      for_each = local.tls ? [true] : []
      content {
        hosts       = [var.ingress_host]
        secret_name = "${var.name}-tls"
      }
    }
  }
  lifecycle {
    ignore_changes = [
      metadata[0].annotations["field.cattle.io/publicEndpoints"],
    ]
  }
}

resource "kubernetes_network_policy" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
  spec {
    pod_selector {
      match_labels = local.selector
    }

    ingress {
      ports {
        port     = "http"
        protocol = "TCP"
      }
    }

    policy_types = ["Ingress"]
  }
}
