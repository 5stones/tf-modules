resource "kubernetes_job_v1" "before_deploy" {
  for_each = var.before_deploy
  metadata {
    namespace = var.namespace
    name      = "${var.name}-${each.key}"
    labels = merge(local.labels, {
      "app.kubernetes.io/instance" = "${var.name}-${each.key}"
    })
  }
  spec {
    backoff_limit = lookup(each.value, "backoff_limit", 2)
    template {
      metadata {
        labels = merge(local.labels, {
          "app.kubernetes.io/instance" = "${var.name}-${each.key}"
        })
        annotations = var.pod_annotations
      }
      spec {
        restart_policy = "Never"

        service_account_name = kubernetes_service_account.main.metadata.0.name
        security_context {
          run_as_non_root = lookup(var.security_context, "run_as_non_root", true)
          run_as_user     = lookup(var.security_context, "run_as_user", var.run_as.user)
          run_as_group    = lookup(var.security_context, "run_as_group", var.run_as.group)
          fs_group        = lookup(var.security_context, "fs_group", var.run_as.group)
          dynamic "seccomp_profile" {
            for_each = can(var.security_context.seccomp_profile) ? [true] : []
            content {
              type              = lookup(var.security_context.seccomp_profile, "type", "RuntimeDefault")
              localhost_profile = lookup(var.security_context.seccomp_profile, "localhost_profile", null)
            }
          }
        }

        container {
          name  = "app"
          image = lookup(each.value, "image", var.image)

          args    = lookup(each.value, "args", var.args)
          command = lookup(each.value, "command", var.command)

          security_context {
            allow_privilege_escalation = lookup(var.security_context, "allow_privilege_escalation", false)
            run_as_non_root            = lookup(var.security_context, "run_as_non_root", true)
            capabilities {
              drop = try(var.security_context.capabilities.drop, ["ALL"])
              add  = try(var.security_context.capabilities.add, null)
            }
          }

          resources {
            requests = lookup(each.value, "resources", var.resources).requests
            limits   = lookup(each.value, "resources", var.resources).limits
          }

          dynamic "env" {
            for_each = merge(var.env, lookup(each.value, "env", {}))
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic "env_from" {
            for_each = merge(var.config_maps, lookup(each.value, "config_maps", {}))
            content {
              prefix = env_from.value
              config_map_ref {
                name = env_from.key
              }
            }
          }
          dynamic "env_from" {
            for_each = merge(var.secrets, lookup(each.value, "secrets", {}))
            content {
              prefix = env_from.value
              secret_ref {
                name = env_from.key
              }
            }
          }
        }
      }
    }
  }

  wait_for_completion = true
}
