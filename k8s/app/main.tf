locals {
  labels = merge(
    {
      app                            = var.name
      "app.kubernetes.io/managed-by" = "terraform"
      "app.kubernetes.io/name"       = var.name
      "app.kubernetes.io/instance"   = var.name
    },
    var.additional_labels
  )
  selector = {
    "app.kubernetes.io/instance" = local.labels["app.kubernetes.io/instance"]
  }
  node_selectors = concat(
    try(length(var.arch), 0) == 0 ? [] : [
      { key = "kubernetes.io/arch", values = var.arch },
    ],
    var.node_selectors,
  )
}

resource "kubernetes_deployment" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
    annotations = {
      "kubernetes.io/change-cause" = var.change_cause
    }
  }
  spec {
    replicas = var.autoscaling_range[0] != var.autoscaling_range[1] ? null : var.autoscaling_range[0]

    selector {
      match_labels = local.selector
    }

    template {
      metadata {
        labels      = local.labels
        annotations = var.pod_annotations
      }

      spec {
        service_account_name = kubernetes_service_account.main.metadata.0.name
        security_context {
          run_as_non_root = lookup(var.security_context, "run_as_non_root", true)
          run_as_user     = lookup(var.security_context, "run_as_user", var.run_as.user)
          run_as_group    = lookup(var.security_context, "run_as_group", var.run_as.group)
          fs_group        = lookup(var.security_context, "fs_group", var.run_as.group)
          dynamic "seccomp_profile" {
            for_each = can(var.security_context.seccomp_profile) ? [true] : []
            content {
              type              = lookup(var.security_context.seccomp_profile, "type", "RuntimeDefault")
              localhost_profile = lookup(var.security_context.seccomp_profile, "localhost_profile", null)
            }
          }
        }

        container {
          name    = "app"
          image   = var.image
          args    = var.args
          command = var.command
          port {
            name           = "http"
            container_port = var.port
          }

          security_context {
            allow_privilege_escalation = lookup(var.security_context, "allow_privilege_escalation", false)
            run_as_non_root            = lookup(var.security_context, "run_as_non_root", true)
            capabilities {
              drop = try(var.security_context.capabilities.drop, ["ALL"])
              add  = try(var.security_context.capabilities.add, null)
            }
          }

          resources {
            requests = var.resources.requests
            limits   = var.resources.limits
          }

          dynamic "env" {
            for_each = var.env
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic "env_from" {
            for_each = var.config_maps
            content {
              prefix = env_from.value
              config_map_ref {
                name = env_from.key
              }
            }
          }
          dynamic "env_from" {
            for_each = var.secrets
            content {
              prefix = env_from.value
              secret_ref {
                name = env_from.key
              }
            }
          }

          liveness_probe {
            http_get {
              port = var.port
              path = var.probe_path
            }
            initial_delay_seconds = 60
          }
          readiness_probe {
            http_get {
              port = var.port
              path = var.probe_path
            }
          }

          dynamic "volume_mount" {
            for_each = var.storage
            content {
              name       = volume_mount.key
              mount_path = volume_mount.value["mount_path"]
            }
          }
        }

        dynamic "volume" {
          for_each = var.storage
          content {
            name = volume.key
            persistent_volume_claim {
              claim_name = "${var.name}-${volume.key}"
            }
          }
        }

        dynamic "affinity" {
          for_each = length(local.node_selectors) > 0 ? ["required"] : []
          content {
            node_affinity {
              required_during_scheduling_ignored_during_execution {
                node_selector_term {
                  dynamic "match_expressions" {
                    for_each = toset(local.node_selectors)
                    content {
                      key      = match_expressions.value.key
                      operator = lookup(match_expressions.value, "operator", "In")
                      values   = lookup(match_expressions.value, "values", null)
                    }
                  }
                }
              }
            }
          }
        }

        dynamic "toleration" {
          for_each = toset(concat(
            !try(contains(var.arch, "arm64"), false) ? [] : [
              { key = "kubernetes.io/arch", value = "arm64" },
            ],
            var.tolerations,
          ))
          content {
            key                = lookup(toleration.value, "key", null)
            operator           = lookup(toleration.value, "operator", "Equal")
            value              = lookup(toleration.value, "value", null)
            effect             = lookup(toleration.value, "effect", null)
            toleration_seconds = lookup(toleration.value, "toleration_seconds", null)
          }
        }

        dynamic "topology_spread_constraint" {
          for_each = var.topology_spread_constraints
          content {
            max_skew           = topology_spread_constraint.value.max_skew
            topology_key       = topology_spread_constraint.value.topology_key
            when_unsatisfiable = topology_spread_constraint.value.when_unsatisfiable
            label_selector {
              match_labels = local.selector
            }
          }
        }
      }
    }
  }
  lifecycle {
    ignore_changes = [
      metadata[0].annotations["field.cattle.io/publicEndpoints"],
    ]
  }
  depends_on = [
    kubernetes_job_v1.before_deploy
  ]
}
