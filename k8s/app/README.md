# Deploy a basic http application to K8s

```HCL
module "app" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/app"


  # Required Configuration

  namespace = "default"
  name      = "my-app"
  image     = module.ci.docker_image  # or "some-image:latest"


  # Common Configuration Examples:

  # supported architectures to select and tolerate nodes
  #arch = ["amd64", "arm64"]

  # http port for the container
  #port = 8080
  # use a different path for health and ready checks
  #probe_path = "/healthz"

  # override the command or arguments
  #command = ["/bin/..."]
  #args = ["npm", "run", ...]

  # set up a cpu based hpa instead of a single replica
  #autoscaling_range = [2, 4]

  # adjust resources for the container
  #resources = {
  #  requests = {
  #    cpu    = "125m"
  #    memory = "128Mi"
  #  }
  #  limits = {
  #    memory = "256Mi"
  #  }
  #}

  # use node port or load balancer for the service instead of ClusterIP
  #service_type = "NodePort"

  # create a basic ingress
  #ingress_host = "api.example.com"
  # configure ssl on ingress through cert-manager with a cluster-wide issuer
  #cert_cluster_issuer = "letsencrypt"
  # configure ssl on ingress through cert-manager with a namespaced issuer
  #cert_issuer = "letsencrypt"

  # environment variables and references to configmaps and secrets
  #env = {
  #  SOME_ENV = "something"
  #}
  #config_maps = {
  #  "some-k8s-config-map-name" = "ENV_PREFIX_"
  #}
  #secrets = {
  #  "some-k8s-secret-name" = "ENV_PREFIX_"
  #}

  # before updating the deployment, run these commands as jobs with the same image
  #before_deploy = {
  #  migrate = { args = ["npm", "run", "migrate"] }
  #}

  # add cron job with overrides
  #cron_jobs = {
  #  clear-cache = { schedule = \"0 0 * * *\", args = [...], env = {...} }
  #}

  # Use Baseline Pod Security Standard (otherwise security context is hardened for Restricted)
  #security_context = {
  #  run_as_non_root = false
  #  capabilities = {
  #    add = [
  #      "AUDIT_WRITE",
  #      "CHOWN",
  #      "DAC_OVERRIDE",
  #      "FOWNER",
  #      "FSETID",
  #      "KILL",
  #      "MKNOD",
  #      "NET_BIND_SERVICE",
  #      "SETFCAP",
  #      "SETGID",
  #      "SETPCAP",
  #      "SETUID",
  #      "SYS_CHROOT",
  #    ]
  #  }
  #}
}

# can be used to build and push the container image
module "ci" {
  source      = "git::https://gitlab.com/5stones/tf-modules//ci?ref=v6.3.0"
  docker_repo = "###.dkr.ecr.us-east-1.amazonaws.com/some-repo"
}
```
