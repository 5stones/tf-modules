resource "kubernetes_service" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = merge(local.labels, var.service_labels)
  }
  spec {
    selector = local.selector

    type = var.service_type
    port {
      name        = "http"
      port        = 80
      target_port = var.port
    }
  }
  lifecycle {
    ignore_changes = [metadata[0].annotations]
  }
}
