resource "kubernetes_cron_job_v1" "main" {
  for_each = var.cron_jobs
  metadata {
    namespace = var.namespace
    name      = "${var.name}-${each.key}"
    labels = merge(local.labels, {
      "app.kubernetes.io/instance" = "${var.name}-${each.key}"
    })
  }
  spec {
    schedule                      = each.value.schedule
    timezone                      = lookup(each.value, "timezone", null)
    concurrency_policy            = lookup(each.value, "concurrency_policy", null)
    starting_deadline_seconds     = lookup(each.value, "starting_deadline_seconds", null)
    failed_jobs_history_limit     = lookup(each.value, "failed_jobs_history_limit", null)
    successful_jobs_history_limit = lookup(each.value, "successful_jobs_history_limit", null)

    job_template {
      metadata {
        labels = merge(local.labels, {
          "app.kubernetes.io/instance" = "${var.name}-${each.key}"
        })
      }
      spec {
        backoff_limit = lookup(each.value, "backoff_limit", 2)
        template {
          metadata {
            labels = merge(local.labels, {
              "app.kubernetes.io/instance" = "${var.name}-${each.key}"
            })
            annotations = var.pod_annotations
          }
          spec {
            restart_policy = "Never"

            service_account_name = kubernetes_service_account.main.metadata.0.name
            security_context {
              run_as_non_root = lookup(var.security_context, "run_as_non_root", true)
              run_as_user     = lookup(var.security_context, "run_as_user", var.run_as.user)
              run_as_group    = lookup(var.security_context, "run_as_group", var.run_as.group)
              fs_group        = lookup(var.security_context, "fs_group", var.run_as.group)
              dynamic "seccomp_profile" {
                for_each = can(var.security_context.seccomp_profile) ? [true] : []
                content {
                  type              = lookup(var.security_context.seccomp_profile, "type", "RuntimeDefault")
                  localhost_profile = lookup(var.security_context.seccomp_profile, "localhost_profile", null)
                }
              }
            }

            container {
              name  = "app"
              image = lookup(each.value, "image", var.image)

              args    = lookup(each.value, "args", var.args)
              command = lookup(each.value, "command", var.command)

              security_context {
                allow_privilege_escalation = false
                run_as_non_root            = lookup(var.security_context, "run_as_non_root", true)
                capabilities {
                  drop = ["ALL"]
                }
              }

              resources {
                requests = lookup(each.value, "resources", var.resources).requests
                limits   = lookup(each.value, "resources", var.resources).limits
              }

              dynamic "env" {
                for_each = merge(var.env, lookup(each.value, "env", {}))
                content {
                  name  = env.key
                  value = env.value
                }
              }
              dynamic "env_from" {
                for_each = merge(var.config_maps, lookup(each.value, "config_maps", {}))
                content {
                  prefix = env_from.value
                  config_map_ref {
                    name = env_from.key
                  }
                }
              }
              dynamic "env_from" {
                for_each = merge(var.secrets, lookup(each.value, "secrets", {}))
                content {
                  prefix = env_from.value
                  secret_ref {
                    name = env_from.key
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
