resource "kubernetes_persistent_volume_claim_v1" "main" {
  for_each = var.storage

  metadata {
    namespace = var.namespace
    name      = "${var.name}-${each.key}"
    labels    = local.labels
  }
  spec {
    storage_class_name = lookup(each.value, "storage_class_name", null)
    access_modes       = lookup(each.value, "access_modes", ["ReadWriteMany"])
    resources {
      requests = {
        storage = each.value["storage"]
      }
    }
  }
}
