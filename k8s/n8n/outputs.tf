output "kubernetes_service" {
  value = {
    main     = kubernetes_service.main,
    webhooks = kubernetes_service.webhooks,
  }
}

output "node_ports" {
  value = var.service_type == "NodePort" ? {
    main     = kubernetes_service.main.spec.0.port.0.node_port
    webhooks = kubernetes_service.webhooks.spec.0.port.0.node_port
  } : null
}
