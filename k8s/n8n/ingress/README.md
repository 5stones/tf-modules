Defines a K8s ingress and optional [oauth2-proxy](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview) for authentication.

```
module "n8n_ingress" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/n8n/ingress"

  host     = local.fqdn
  services = module.n8n.kubernetes_service

  # keycloak auth example
  oauth2_proxy = {
    PROVIDER        = "keycloak-oidc"
    OIDC_ISSUER_URL = "https://<keycloak host>/auth/realms/<your realm>"
    CLIENT_ID       = "n8n"
    CLIENT_SECRET   = "<your client's secret>"
    # Optional, require client role
    ALLOWED_ROLES   = "n8n:role-name"
  }
}
```
