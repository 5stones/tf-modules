locals {
  proxy_name = "${local.name}-oauth2-proxy"
  proxy_labels = {
    "app.kubernetes.io/name"       = "oauth2-proxy"
    "app.kubernetes.io/managed-by" = "terraform"
    "app.kubernetes.io/part-of"    = local.name
    "app.kubernetes.io/version"    = var.oauth2_proxy_version
  }
}

module "proxy" {
  source = "../../app"
  count  = var.oauth2_proxy == null ? 0 : 1

  namespace = local.namespace
  name      = local.proxy_name
  image     = "${var.oauth2_proxy_repo}:v${var.oauth2_proxy_version}"

  additional_labels = local.proxy_labels

  port       = 4180
  probe_path = "/ping"

  args = [
    "--http-address=0.0.0.0:4180",
    "--redirect-url=https://${var.host}/oauth2/callback",
    "--request-logging=false",
    "--reverse-proxy",
    "--session-cookie-minimal",
    "--silence-ping-logging",
    "--skip-provider-button",
    "--upstream=http://${var.services["main"].metadata.0.name}.${local.namespace}.svc.cluster.local/",
  ]
  secrets = {
    (local.proxy_name) = "OAUTH2_PROXY_"
  }

  resources = {
    requests = {
      cpu    = "10m"
      memory = "10Mi"
    }
    limits = {}
  }
}

resource "kubernetes_secret" "proxy" {
  count = var.oauth2_proxy == null ? 0 : 1

  metadata {
    namespace = local.namespace
    name      = local.proxy_name
    labels    = local.proxy_labels
  }
  data = merge(
    {
      COOKIE_SECRET = random_password.cookie_secret.result,
      EMAIL_DOMAINS = "*"
    },
    var.oauth2_proxy,
  )
}

resource "random_password" "cookie_secret" {
  length           = 32
  override_special = "-_"
}
