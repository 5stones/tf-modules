output "ingress" {
  value = kubernetes_ingress_v1.main
}

output "proxy" {
  value = var.oauth2_proxy == null ? null : module.proxy[0]
}
