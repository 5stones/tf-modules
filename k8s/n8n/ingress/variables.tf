variable "host" {
}

variable "services" {
  description = "services output from n8n module { main, webhooks }"
}

variable "additional_labels" {
  default = {}
}

variable "annotations" {
  default = {}
}

variable "cert_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation."
  default     = ""
}

variable "oauth2_proxy_repo" {
  default = "quay.io/oauth2-proxy/oauth2-proxy"
}

variable "oauth2_proxy_version" {
  default = "7.4.0"
}

variable "oauth2_proxy" {
  description = "Secrets to configure the oauth2 proxy (without the OAUTH2_PROXY_ prefix)"
  default     = null
}
