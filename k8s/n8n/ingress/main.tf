locals {
  namespace = var.services["main"].metadata.0.namespace
  name      = var.services["main"].metadata.0.name

  labels = merge(
    {
      app                            = local.name
      "app.kubernetes.io/managed-by" = "terraform"
      "app.kubernetes.io/name"       = "n8n"
      "app.kubernetes.io/part-of"    = local.name
    },
    var.additional_labels
  )
}

resource "kubernetes_ingress_v1" "main" {
  metadata {
    namespace = local.namespace
    name      = local.name
    labels    = local.labels
    annotations = merge(
      var.cert_issuer == "" ? {} : { "cert-manager.io/issuer" = var.cert_issuer },
      var.annotations,
    )
  }
  spec {
    # main process
    rule {
      host = var.host
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = var.oauth2_proxy == null ? var.services["main"].metadata.0.name : module.proxy[0].service.metadata.0.name
              port {
                name = "http"
              }
            }
          }
        }
      }
    }
    # webhook processes
    rule {
      host = var.host
      http {
        path {
          path      = "/webhook"
          path_type = "Prefix"
          backend {
            service {
              name = var.services["webhooks"].metadata.0.name
              port {
                name = "http"
              }
            }
          }
        }
      }
    }

    dynamic "tls" {
      for_each = var.cert_issuer == "" ? [] : [true]
      content {
        hosts       = var.hosts
        secret_name = "${local.name}-tls"
      }
    }
  }
  lifecycle {
    ignore_changes = [
      metadata[0].annotations["field.cattle.io/publicEndpoints"],
    ]
  }
}
