resource "kubernetes_deployment" "n8n" {
  for_each = local.roles

  metadata {
    namespace = var.namespace
    name      = each.value.name
    labels    = each.value.labels
  }

  spec {
    replicas = each.value.replicas[0] != each.value.replicas[1] ? null : each.value.replicas[0]
    strategy {
      type = each.value.strategy
    }

    selector {
      match_labels = {
        app  = var.name
        role = each.key
      }
    }

    template { # pod template
      metadata {
        labels = each.value.labels
      }
      spec {
        service_account_name = kubernetes_service_account.main.metadata.0.name

        container {
          name  = "n8n"
          image = var.image
          args  = each.value.args

          resources {
            requests = var.resources.requests
            limits   = var.resources.limits
          }

          env {
            name  = "NODE_ENV"
            value = var.node_env
          }
          env {
            name  = "EXECUTIONS_MODE"
            value = "queue"
          }
          env {
            name  = "QUEUE_BULL_REDIS_HOST"
            value = module.redis.primary.host
          }
          env {
            name  = "N8N_SKIP_WEBHOOK_DEREGISTRATION_SHUTDOWN"
            value = "true"
          }
          env {
            name  = "N8N_METRICS"
            value = var.monitoring_labels != {} ? "true" : ""
          }

          env_from {
            config_map_ref {
              name = kubernetes_config_map.main.metadata.0.name
            }
          }
          env_from {
            secret_ref {
              name = kubernetes_secret.main.metadata.0.name
            }
          }
          env_from {
            secret_ref {
              name = var.secret_ref_name
            }
          }

          dynamic "port" {
            for_each = each.key == "worker" ? {} : { http = true }
            content {
              name           = "http"
              container_port = 5678
            }
          }
          dynamic "liveness_probe" {
            for_each = each.key == "worker" ? {} : { http = true }
            content {
              http_get {
                port = "http"
                path = "/healthz"
              }
            }
          }
          dynamic "readiness_probe" {
            for_each = each.key == "worker" ? {} : { http = true }
            content {
              http_get {
                port = "http"
                path = "/healthz"
              }
            }
          }
        }
        dynamic "topology_spread_constraint" {
          for_each = each.value.spread_selector == null ? [] : var.topology_spread_constraints
          content {
            max_skew           = topology_spread_constraint.value.max_skew
            topology_key       = topology_spread_constraint.value.topology_key
            when_unsatisfiable = topology_spread_constraint.value.when_unsatisfiable
            label_selector {
              match_labels = each.value.spread_selector
            }
          }
        }
      }
    }
  }
  lifecycle {
    ignore_changes = [
      metadata[0].annotations["field.cattle.io/publicEndpoints"],
      spec[0].template[0].metadata[0].annotations,
    ]
  }
}
