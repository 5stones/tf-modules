module "redis" {
  source = "../redis"

  name      = "${var.name}-redis"
  namespace = var.namespace

  nodes   = var.redis_ha ? 3 : 1
  haproxy = var.redis_ha

  resources = {
    requests = {
      cpu    = "20m"
      memory = "12Mi"
    }
    limits = {
      memory = "12Mi"
    }
  }
}
