variable "fqdn" {
}

variable "secret_ref_name" {
  description = "The name of a k8s secret that contains env variables for the DB config"
}

variable "name" {
  default = "n8n"
}

variable "namespace" {
  default = "default"
}

variable "image" {
  default = "n8nio/n8n"
}

variable "worker_replicas" {
  default = [1, 1]
}

variable "webhook_replicas" {
  default = [0, 0]
}

variable "autoscaler" {
  description = "How the horizontal pod autoscaler behaves (if any)"
  default = {
    target = 80
    scale_up = {
      # changed from the default to add some stabilization (3 data points instead of 1)
      stabilization_window_seconds = 45
      period_seconds               = 15
      percent                      = 100
      pods                         = 4
    }
    scale_down = {
      stabilization_window_seconds = 300
      period_seconds               = 15
      percent                      = 100
    }
  }
}

variable "service_type" {
  default = "NodePort"
}

variable "node_env" {
  default = "production"
}

variable "n8n_cmd" {
  default = ["n8n"]
}

variable "main_accepts_webhooks" {
  default = true
}

variable "resources" {
  default = {
    requests = {
      cpu    = "300m"
      memory = "200Mi"
    }
    limits = {
      cpu    = "1000m"
      memory = "400Mi"
    }
  }
}

variable "monitoring_labels" {
  default = {}
}

variable "topology_spread_constraints" {
  default = [{
    topology_key       = "kubernetes.io/hostname"
    max_skew           = 1
    when_unsatisfiable = "ScheduleAnyway"
  }]
}

variable "redis_ha" {
  default = false
}
