output "alb" {
  value = data.aws_alb.selected
}

output "target_group" {
  value = aws_alb_target_group.n8n
}
