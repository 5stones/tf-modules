# define the dns A record (ipv4)
resource "aws_route53_record" "main" {
  for_each = toset(
    data.aws_alb.selected.ip_address_type == "dualstack" ? ["A", "AAAA"] : ["A"]
  )

  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.hostname
  type    = each.value

  alias {
    name                   = data.aws_alb.selected.dns_name
    zone_id                = data.aws_alb.selected.zone_id
    evaluate_target_health = false
  }
}

data "aws_route53_zone" "selected" {
  name = var.zone
}
