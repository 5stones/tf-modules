variable "hostname" {
  default = "n8n"
}

variable "target_group_name_prefix" {
}

variable "zone" {
}

variable "alb" {
}

variable "node_ports" {
  description = "node_ports output from n8n module"
}

variable "autoscaling_group_name" {
}

variable "oidc" {
  description = "The authenticate_oidc block of a listener rule; otherwise, this will be open to the world. Basic auth can be added to n8n secrets."
  default     = null
}
