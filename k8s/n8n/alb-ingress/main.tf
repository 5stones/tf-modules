locals {
  vpc_id = data.aws_alb.selected.vpc_id # get vpc_id from alb
  fqdn   = "${var.hostname}${var.hostname == "" ? "" : "."}${var.zone}"
}

# define the target group for the service
resource "aws_alb_target_group" "n8n" {
  for_each = var.node_ports

  name                 = "${var.target_group_name_prefix}-${each.key}"
  port                 = each.value
  protocol             = "HTTP"
  vpc_id               = local.vpc_id
  deregistration_delay = 30
  health_check {
    path                = "/healthz"
    matcher             = "200"
    healthy_threshold   = 3
    unhealthy_threshold = 2
  }
}

# Create a the autoscaling target group attachment
resource "aws_autoscaling_attachment" "n8n" {
  for_each = aws_alb_target_group.n8n

  autoscaling_group_name = var.autoscaling_group_name
  alb_target_group_arn   = each.value.arn
}

# define a https listener rule for webhooks
resource "aws_alb_listener_rule" "webhooks" {
  for_each = aws_alb_target_group.n8n

  listener_arn = data.aws_alb_listener.selected.arn

  action {
    type             = "forward"
    target_group_arn = each.value.arn
  }

  condition {
    host_header {
      values = [local.fqdn]
    }
  }
  condition {
    path_pattern {
      values = [
        each.key == "main" ? "/webhook-test/*" : "/webhook/*",
      ]
    }
  }
}

# define a https listener rule for other urls with oidc auth
resource "aws_alb_listener_rule" "main" {
  listener_arn = data.aws_alb_listener.selected.arn

  dynamic "action" {
    for_each = var.oidc != null ? { oidc = var.oidc } : {}
    content {
      type = "authenticate-oidc"
      authenticate_oidc {
        issuer                 = var.oidc.issuer
        authorization_endpoint = var.oidc.authorization_endpoint
        token_endpoint         = var.oidc.token_endpoint
        user_info_endpoint     = var.oidc.user_info_endpoint
        client_id              = var.oidc.client_id
        client_secret          = var.oidc.client_secret
      }
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.n8n["main"].arn
  }

  condition {
    host_header {
      values = [local.fqdn]
    }
  }
}

data "aws_alb" "selected" {
  name = var.alb
}

data "aws_alb_listener" "selected" {
  load_balancer_arn = data.aws_alb.selected.arn
  port              = 443
}
