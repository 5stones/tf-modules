locals {
  labels = {
    "app.kubernetes.io/name"       = "n8n"
    "app.kubernetes.io/managed-by" = "terraform"
    "app.kubernetes.io/part-of"    = var.name
  }
  roles = {
    main = {
      name     = var.name
      replicas = [1, 1]
      strategy = "Recreate"
      args     = []
      labels = merge(local.labels, {
        app      = var.name
        role     = "main"
        webhooks = var.main_accepts_webhooks ? "true" : "false"
      })
      spread_selector = !var.main_accepts_webhooks ? null : {
        app      = var.name
        webhooks = "true"
      }
    }
    worker = {
      name     = "${var.name}-worker"
      replicas = var.worker_replicas
      strategy = "RollingUpdate"
      args     = concat(var.n8n_cmd, ["worker"])
      labels = merge(local.labels, {
        app  = var.name
        role = "worker"
      })
      spread_selector = {
        app  = var.name
        role = "worker"
      }
    }
    webhook = {
      name     = "${var.name}-webhook"
      replicas = var.webhook_replicas
      strategy = "RollingUpdate"
      args     = concat(var.n8n_cmd, ["webhook"])
      labels = merge(local.labels, {
        app      = var.name
        role     = "webhook"
        webhooks = "true"
      })
      spread_selector = {
        app      = var.name
        webhooks = "true"
      }
    }
  }
}

resource "kubernetes_config_map" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
  }
  data = {
    N8N_HOST           = var.fqdn
    N8N_PROTOCOL       = "https"
    N8N_PORT           = "5678"
    WEBHOOK_URL        = "https://${var.fqdn}/"
    WEBHOOK_TUNNEL_URL = "https://${var.fqdn}/"
    GENERIC_TIMEZONE   = "Etc/UTC"

    EXECUTIONS_DATA_MAX_AGE = "168"
  }
  lifecycle {
    ignore_changes = [data]
  }
}

resource "kubernetes_secret" "main" {
  metadata {
    namespace = var.namespace
    name      = "${var.name}-generated"
    labels    = local.labels
  }
  data = {
    N8N_ENCRYPTION_KEY = random_password.encryption_key.result
  }
}

resource "random_password" "encryption_key" {
  length  = 64
  special = false
}
