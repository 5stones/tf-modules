resource "kubernetes_service" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels = merge(local.labels, var.monitoring_labels, {
      app  = var.name
      role = "main"
    })
  }
  spec {
    selector = {
      app  = var.name
      role = "main"
    }

    type = var.service_type
    port {
      name        = "http"
      port        = 80
      target_port = 5678
    }
  }
  lifecycle {
    ignore_changes = [metadata[0].annotations]
  }
}

resource "kubernetes_service" "webhooks" {
  metadata {
    namespace = var.namespace
    name      = "${var.name}-webhooks"
    labels = merge(local.labels, {
      app  = var.name
      role = "webhooks"
    })
  }
  spec {
    selector = {
      app      = var.name
      webhooks = "true"
    }

    type = var.service_type
    port {
      name        = "http"
      port        = 80
      target_port = 5678
    }
  }
  lifecycle {
    ignore_changes = [metadata[0].annotations]
  }
}

# this allows external access to http port, and assumes unrestricted egress and namespace traffic
resource "kubernetes_network_policy" "n8n" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
  }

  spec {
    pod_selector {
      match_labels = {
        app = var.name
      }
    }

    ingress {
      ports {
        port     = "http"
        protocol = "TCP"
      }
    }

    policy_types = ["Ingress"]
  }
}
