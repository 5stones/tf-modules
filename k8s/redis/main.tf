locals {
  set = merge({
    "architecture" = var.nodes > 1 ? "replication" : "standalone"
    # needs at least 3 sentinels (sidecar) for high-availability
    "sentinel.enabled" = var.nodes > 2 ? true : false
    # when sentinels are enabled, the master statefulset isn't created
    "replica.replicaCount" = var.nodes > 2 ? var.nodes : var.nodes - 1
  }, var.set)
}

resource "helm_release" "redis" {
  namespace = var.namespace
  name      = var.name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"
  version    = var.helm_version

  values = concat(
    [templatefile("${path.module}/values.yaml", {
      name             = var.name
      resources        = var.resources
      persistence      = var.persistence_size != null ? "true" : "false"
      persistence_size = var.persistence_size != null ? var.persistence_size : ""
    })],
    var.values
  )

  dynamic "set" {
    for_each = local.set
    content {
      name  = set.key
      value = set.value
    }
  }
}

module "haproxy" {
  source = "./haproxy-helm"
  count  = var.haproxy && var.nodes > 2 ? 1 : 0

  namespace = var.namespace
  name      = helm_release.redis.name
  nodes     = var.nodes
}
