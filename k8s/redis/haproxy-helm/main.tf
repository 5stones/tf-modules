locals {
  name = "${var.name}-haproxy"
}

resource "helm_release" "haproxy" {
  namespace = var.namespace
  name      = local.name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "haproxy"
  version    = var.helm_version

  values = concat(
    [templatefile("${path.module}/values.yaml", {
      namespace = var.namespace
      name      = local.name
      redis     = var.name
      nodes     = var.nodes
      password  = var.password

      resources = var.resources
    })],
    var.values
  )

  dynamic "set" {
    for_each = merge(
      {
        "replicaCount" = var.replicas
      },
      var.set
    )
    content {
      name  = set.key
      value = set.value
    }
  }
}
