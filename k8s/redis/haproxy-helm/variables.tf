variable "namespace" {
  default = null
}

variable "name" {
  description = "The name of the installed bitnami redis"
  default     = "redis"
}

variable "password" {
  description = "redis password, if any"
  default     = null
}

variable "helm_version" {
  default = null
}

variable "nodes" {
  default = 3
}

variable "replicas" {
  default = 2
}

variable "resources" {
  default = {
    requests = {
      cpu    = "10m"
      memory = "5Mi"
    }
    limits = {
      memory = "10Mi"
    }
  }
}

variable "values" {
  description = "Additional values.yaml files to pass to helm"
  default     = []
}

variable "set" {
  description = "Overrides with helm --set"
  default     = {}
}
