output "primary" {
  value = {
    service_name = helm_release.haproxy.name
    host         = "${helm_release.haproxy.name}.${var.namespace}.svc.cluster.local"
    port         = 6379
  }
}

output "replicas" {
  value = {
    service_name = helm_release.haproxy.name
    host         = "${helm_release.haproxy.name}.${var.namespace}.svc.cluster.local"
    port         = 6380
  }
}
