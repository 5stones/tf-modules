Defines an HAProxy that routes redis traffic to the primary or replicas.

Based on [How to setup HAProxy for Redis Sentinel on Kubernetes](https://yaniv-bhemo.medium.com/how-to-setup-haproxy-for-redis-sentinel-on-kubernetes-37ee70e44464)
