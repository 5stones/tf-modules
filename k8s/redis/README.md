# Deploys Redis and optional HAProxy using the Bitnami Helm charts.

## Basic Deployment

```HCL
module "redis" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/redis"

  #namespace    = "default"
  name         = "my-redis"

  # uncomment for a replica (no high-availability or failover)
  #nodes = 2

  # uncomment to enable persistent storage
  #persistence_size = "1Gi"

  # resources per redis pod
  #resources = {
  #  requests = {
  #    cpu    = "20m"
  #    memory = "100Mi"
  #  }
  #  limits = {
  #    memory = "100Mi"
  #  }
  #}
}

locals {
  redis_primary_url = "redis://${module.redis.primary["host"]}:${module.redis.primary["port"]}/0"
  #redis_replica_url = "redis://${module.redis.replicas["host"]}:${module.redis.replicas["port"]}/0"
}
```


## High-Availability Deployment

High-availability with Redis is acheived by deploying at least 3 sentinels to manage the failover of nodes.
Bitnami's helm chart supports this, but unfortunately, connecting to the single service will connect to 1 of the 3+ nodes, which may or may not be the primary.
Your application needs to connect to a Sentinel to determine which node you need to connect to for writes.
Alternatively, you can connect through an HAProxy that tracks which node is the primary, which is included by enabling `haproxy`.

```HCL
module "redis" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/redis"

  #namespace    = "default"
  name         = "my-redis"

  # at least 3 nodes are required (3 sentinels)
  nodes = 3

  # uncomment if your client doesn't support sentinels
  #haproxy = true

  # uncomment for persistent storage
  #persistence_size = "1Gi"

  # resources per redis pod
  #resources = {
  #  requests = {
  #    cpu    = "20m"
  #    memory = "100Mi"
  #  }
  #  limits = {
  #    memory = "100Mi"
  #  }
  #}
}

locals {
  # if your app supports sentinels
  sentinels_host = module.redis.sentinels["host"]
  sentinels_port = module.redis.sentinels["port"]

  # if you enabled the haproxy
  #redis_primary_url = "redis://${module.redis.primary["host"]}:${module.redis.primary["port"]}/0"
  #redis_replica_url = "redis://${module.redis.replicas["host"]}:${module.redis.replicas["port"]}/0"
}
```
