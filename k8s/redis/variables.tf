variable "name" {
  default = "redis"
}

variable "namespace" {
  default = null
}

variable "helm_version" {
  default = null
}

variable "nodes" {
  default = 1
}

variable "resources" {
  default = {
    requests = {
      cpu    = "20m"
      memory = "100Mi"
    }
    limits = {
      memory = "100Mi"
    }
  }
}

variable "persistence_size" {
  description = "PVC volume size in each pod, if persistence is needed (ex: 8Gi)"
  default     = null
}

variable "values" {
  description = "Additional values.yaml files to pass to helm"
  default     = []
}

variable "set" {
  description = "Overrides with helm --set"
  default     = {}
}

variable "haproxy" {
  description = "Add an HAProxy to load balance primary/replica when Redis Sentinels are running (nodes > 3)"
  default     = false
}
