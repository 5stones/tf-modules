locals {
  service_name    = local.set["sentinel.enabled"] ? helm_release.redis.name : "${helm_release.redis.name}-master"
  primary_service = local.set["sentinel.enabled"] ? null : "${helm_release.redis.name}-master"
  replica_service = var.nodes == 1 || local.set["sentinel.enabled"] ? null : "${helm_release.redis.name}-replica"

  haproxy_primary  = length(module.haproxy) > 0 ? module.haproxy[0].primary : null
  haproxy_replicas = length(module.haproxy) > 0 ? module.haproxy[0].replicas : null
}

output "service_name" {
  value = local.service_name
}

output "primary" {
  value = local.primary_service == null ? local.haproxy_primary : {
    service_name = local.primary_service
    host         = "${local.primary_service}.${var.namespace}.svc.cluster.local"
    port         = 6379
  }
}

output "replicas" {
  value = local.replica_service == null ? local.haproxy_replicas : {
    service_name = local.replica_service
    host         = "${local.replica_service}.${var.namespace}.svc.cluster.local"
    port         = 6379
  }
}

output "sentinels" {
  value = !local.set["sentinel.enabled"] ? null : {
    service_name = helm_release.redis.name
    host         = "${helm_release.redis.name}.${var.namespace}.svc.cluster.local"
    port         = 26379
  }
}

output "haproxy" {
  value = length(module.haproxy) > 0 ? module.haproxy[0] : null
}
