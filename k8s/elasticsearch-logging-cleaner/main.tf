locals {
  script = <<SCRIPT
cleanup() {
  retainDate=`date -d @$(( $(date +%s) - $2 * 24 * 60 * 60 )) +%Y.%m.%d`
  for index in `curl -s "$ES_ORIGIN/_cat/indices/$1?h=i"`; do
    indexDate=`echo "$index" | grep -o '[0-9][0-9][0-9][0-9]\.[0-9][0-9]\.[0-9][0-9]' | head -n1`
    if [ "$indexDate" \< "$retainDate" ]; then
      echo "Deleting $index"
      curl -s -X DELETE "$ES_ORIGIN/$index"
    fi
  done
}
${join("\n", local.cleanup_cmds)}
SCRIPT

  cleanup_cmds = [
    for index, retain in var.rules :
    "cleanup '${index}' '${retain}'"
  ]
}

resource "kubernetes_cron_job" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
  spec {
    schedule                  = var.schedule
    concurrency_policy        = "Replace"
    starting_deadline_seconds = 3600
    job_template {
      metadata {}
      spec {
        template {
          metadata {}
          spec {
            service_account_name = kubernetes_service_account.main.metadata.0.name
            security_context {
              run_as_non_root = true
              run_as_user     = 100
              run_as_group    = 101
              fs_group        = 101
            }

            container {
              name    = "job"
              image   = "curlimages/curl"
              command = ["/bin/sh", "-c", local.script]
              env {
                name  = "ES_ORIGIN"
                value = var.elasticsearch_origin
              }
            }
            restart_policy = "Never"
          }
        }
      }
    }
  }
}

resource "kubernetes_service_account" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
}
