variable "namespace" {
  default = "kube-system"
}

variable "name" {
  default = "elasticsearch-logging-cleaner"
}

variable "elasticsearch_origin" {
  default = "http://elasticsearch-logging:9200"
}

variable "schedule" {
  default = "0 8 * * *"
}

variable "rules" {
  description = "A map of index pattern to number of days to retain"
  default = {
    "logstash-*" = 7
  }
}
