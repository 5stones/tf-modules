# Install the AWS EBS CSI Driver using helm and an access key

```
module "aws-ebs-csi-driver" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/aws-ebs-csi-driver"

  iam_name = "k8s.production.aws-ebs-csi-driver"
}
```
