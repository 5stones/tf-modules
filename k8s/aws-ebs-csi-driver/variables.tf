variable "iam_name" {
}

variable "namespace" {
  default = "kube-system"
}

variable "secret_name" {
  default = "aws-secret"
}

variable "helm_version" {
  default = null
}

variable "iam_path" {
  default = "/"
}

variable "iam_tags" {
  default = {}
}

variable "helm_set" {
  default = {}
}

variable "helm_values" {
  default = null
}

variable "storage_classes" {
  description = "Whether or not to create default storage and snapshot classes"
  default     = false
}

variable "default_storage_class" {
  default = "ebs"
}

variable "default_snapshot_class" {
  default = "ebs"
}
