resource "helm_release" "main" {
  namespace = var.namespace
  name      = "aws-ebs-csi-driver"

  repository = "https://kubernetes-sigs.github.io/aws-ebs-csi-driver"
  chart      = "aws-ebs-csi-driver"
  version    = var.helm_version
  values     = var.helm_values

  dynamic "set" {
    for_each = merge({
    }, var.helm_set)
    content {
      name  = set.key
      value = set.value
    }
  }
}

resource "kubernetes_secret" "main" {
  metadata {
    namespace = var.namespace
    name      = var.secret_name
  }
  data = {
    key_id     = aws_iam_access_key.main.id
    access_key = aws_iam_access_key.main.secret
  }
}

resource "kubernetes_storage_class_v1" "main" {
  for_each = !var.storage_classes ? {} : {
    ebs = {
      // use default parameters
    }
    ebs-xfs = {
      "csi.storage.k8s.io/fstype" = "xfs"
    }
  }

  metadata {
    name = each.key
    annotations = each.key != var.default_storage_class ? {} : {
      "storageclass.kubernetes.io/is-default-class" = "true"
    }
  }
  storage_provisioner    = "ebs.csi.aws.com"
  volume_binding_mode    = "WaitForFirstConsumer"
  allow_volume_expansion = true
  reclaim_policy         = "Delete"
  parameters             = each.value
}

resource "kubernetes_manifest" "snapshot_class" {
  for_each = !var.storage_classes ? {} : {
    ebs = {}
  }

  manifest = yamldecode(<<-YAML
    apiVersion: snapshot.storage.k8s.io/v1
    kind: VolumeSnapshotClass
    metadata:
      name: ${each.key}
      annotations: ${jsonencode(
        each.key != var.default_snapshot_class ? {} : {
          "snapshot.storage.kubernetes.io/is-default-class" = "true"
        }
      )}
    driver: ebs.csi.aws.com
    deletionPolicy: Delete
    parameters: ${jsonencode(each.value)}
    YAML
)
}
