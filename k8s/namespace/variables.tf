variable "namespace" {
  description = "The name of the namespace"
}

variable "cluster_id" {
  description = "Rancher cluster id, if any (ex: c-abcde)"
  default     = ""
}

variable "project_id" {
  description = "Rancher project id, if any (ex: p-abcde)"
  default     = ""
}

variable "labels" {
  description = "Additional labels for the namespace"
  default     = {}
}

variable "pod_security" {
  description = "The Pod Security Admission labels to apply (default: prevent known escalations; warn and audit if not hardened)"
  default     = {
    enforce = "baseline"
    audit   = "restricted"
    warn    = "restricted"
  }
}

variable "network_policy" {
  description = "When true, add a default network policy that mirrors an internal network and prevents connecting into the namespace"
  default     = true
}
