locals {
  labels = merge(
    { "app.kubernetes.io/managed-by" = "terraform" },
    var.labels,
  )
}

resource "kubernetes_namespace" "main" {
  metadata {
    name = var.namespace
    labels = merge(
      { "kubernetes.io/metadata.name" = var.namespace },
      var.project_id != "" ? { "field.cattle.io/projectId" = var.project_id } : {},
      {for mode, level in var.pod_security : "pod-security.kubernetes.io/${mode}" => level},
      local.labels,
    )
    annotations = merge(
      var.cluster_id != "" && var.project_id != "" ? { "field.cattle.io/projectId" = "${var.cluster_id}:${var.project_id}" } : {},
    )
  }
  lifecycle {
    ignore_changes = [
      metadata[0].annotations
    ]
  }
}

resource "kubernetes_network_policy" "main" {
  count = var.network_policy ? 1 : 0

  metadata {
    name      = "default-network-policy"
    namespace = kubernetes_namespace.main.metadata[0].name
    labels    = local.labels
  }
  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        pod_selector {}
      }
    }
  }
}
