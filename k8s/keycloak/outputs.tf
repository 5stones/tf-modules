output "labels" {
  value = local.labels
}

output "deployment" {
  value = kubernetes_deployment.main
}

output "service" {
  value = kubernetes_service.main
}

output "ingress" {
  value = module.ingress.ingress
}

output "service_account" {
  value = kubernetes_service_account_v1.main
}

output "node_port" {
  value = var.service_type == "NodePort" ? kubernetes_service.main.spec.0.port.0.node_port : null
}
