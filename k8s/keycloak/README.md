# Deploy Keycloak to K8s

```HCL
module "keycloak" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/keycloak"

  namespace        = local.env
  fqdn             = local.fqdn
  image            = module.ci.docker_image
  secret_ref_names = [kubernetes_secret.main.metadata.0.name]
  change_cause     = module.ci.version
}
```
