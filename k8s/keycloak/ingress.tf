module "ingress" {
  source = "./ingress"

  namespace         = var.namespace
  name              = var.name
  service_name      = var.service_name
  relative_path     = var.relative_path
  additional_labels = var.additional_labels

  hosts       = concat([var.fqdn], var.additional_hosts)
  annotations = var.ingress_annotations
  cert_issuer = var.cert_issuer
}
