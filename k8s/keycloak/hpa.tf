resource "kubernetes_horizontal_pod_autoscaler_v2" "main" {
  count = var.autoscaling_range[0] != var.autoscaling_range[1] ? 1 : 0

  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
  }

  spec {
    min_replicas = var.autoscaling_range[0]
    max_replicas = var.autoscaling_range[1]

    scale_target_ref {
      api_version = "apps/v1"
      kind        = "Deployment"
      name        = var.name
    }

    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type                = "Utilization"
          average_utilization = try(var.autoscaler.target, 70)
        }
      }
    }

    behavior {
      scale_up {
        stabilization_window_seconds = try(var.autoscaler.scale_up.stabilization_window_seconds, 45)
        select_policy                = "Max"
        policy {
          period_seconds = try(var.autoscaler.scale_up.period_seconds, 15)
          type           = "Percent"
          value          = try(var.autoscaler.scale_up.percent, 100)
        }
        policy {
          period_seconds = try(var.autoscaler.scale_up.period_seconds, 15)
          type           = "Pods"
          value          = try(var.autoscaler.scale_up.pods, 4)
        }
      }
      scale_down {
        stabilization_window_seconds = try(var.autoscaler.scale_down.stabilization_window_seconds, 300)
        policy {
          period_seconds = try(var.autoscaler.scale_down.period_seconds, 15)
          type           = "Percent"
          value          = try(var.autoscaler.scale_down.percent, 100)
        }
      }
    }
  }

  lifecycle {
    ignore_changes = [metadata[0].annotations]
  }
}
