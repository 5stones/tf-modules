variable "namespace" {
  default = "default"
}

variable "name" {
  default = "keycloak"
}

variable "service_name" {
  default = "keycloak"
}

variable "relative_path" {
  description = "Keycloak's http-relative-path config (was '/auth/' before version 17)"
  default     = "/"
}

variable "additional_labels" {
  default = {}
}

variable "hosts" {
  default = []
}

variable "annotations" {
  default = {}
}

variable "cert_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation."
  default     = ""
}
