locals {
  labels = merge(
    {
      app                            = var.name
      "app.kubernetes.io/managed-by" = "terraform"
      "app.kubernetes.io/name"       = "keycloak"
    },
    var.additional_labels
  )
}

resource "kubernetes_ingress_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.service_name
    labels    = local.labels
    annotations = merge(
      {
        "nginx.ingress.kubernetes.io/proxy-buffer-size" = "128k"

        # https://www.keycloak.org/server/reverseproxy#_configure_the_reverse_proxy
        "nginx.ingress.kubernetes.io/server-snippet" = <<-SNIPPET
        ${var.relative_path == "/" ? "" : (
        <<-BLOCK
          location = / {
            return 301 ${var.relative_path};
          }
          location = /robots.txt {
            rewrite ^/robots\.txt ${var.relative_path}robots.txt last;
          }
          BLOCK
    )}
        location ${var.relative_path}metrics {
          internal;
        }
        location ${var.relative_path}health {
          internal;
        }
        SNIPPET
  },
  var.cert_issuer == "" ? {} : { "cert-manager.io/issuer" = var.cert_issuer },
  var.annotations,
)
}
spec {
  dynamic "rule" {
    for_each = var.hosts
    content {
      host = rule.value
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = var.service_name
              port {
                name = "http"
              }
            }
          }
        }
      }
    }
  }
  dynamic "tls" {
    for_each = var.cert_issuer == "" ? [] : [true]
    content {
      hosts       = var.hosts
      secret_name = "${var.name}-tls"
    }
  }
}
lifecycle {
  ignore_changes = [
    metadata[0].annotations["field.cattle.io/publicEndpoints"],
  ]
}
}
