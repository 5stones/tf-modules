locals {
  labels = merge(
    {
      app                            = var.name
      "app.kubernetes.io/managed-by" = "terraform"
      "app.kubernetes.io/name"       = "keycloak"
    },
    var.additional_labels
  )

  selector = {
    app = var.name
  }
  config_map_ref_names = setunion(
    var.config_map_ref_names,
    [kubernetes_config_map.main.metadata.0.name],
  )
}

resource "kubernetes_config_map" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
  }
  data = {
    KC_PROXY              = "edge"
    KC_HOSTNAME           = var.fqdn
    KC_CACHE_STACK        = "kubernetes"
    KC_LOG_LEVEL          = "info"
    KC_HTTP_ENABLED       = "true"
    KC_HTTP_RELATIVE_PATH = var.relative_path
    KC_HEALTH_ENABLED     = "true"
  }
}
