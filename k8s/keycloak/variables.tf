variable "namespace" {
  default = "default"
}

variable "name" {
  default = "keycloak"
}

variable "fqdn" {
}

variable "additional_hosts" {
  default = []
}

variable "service_name" {
  default = "keycloak"
}

variable "additional_labels" {
  default = {}
}

variable "service_type" {
  default = "ClusterIP"
}

variable "image" {
}

variable "image_pull_secret" {
  default = ""
}

variable "pod_annotations" {
  default = {
    "fluentbit.io/parser" = "keycloak"
  }
}

variable "start_cmd" {
  default = ["start", "--optimized"]
}

variable "ingress_annotations" {
  default = {}
}

variable "cert_issuer" {
  description = "If set, will enable tls on the ingress and add the cert-manager annotation."
  default     = ""
}

variable "autoscaling_range" {
  default = [1, 1]
}

variable "autoscaler" {
  description = "How the horizontal pod autoscaler behaves (if any)"
  default = {
    target = 70
    scale_up = {
      # changed from the default to add some stabilization (3 data points instead of 1)
      stabilization_window_seconds = 45
      period_seconds               = 15
      percent                      = 100
      pods                         = 4
    }
    scale_down = {
      stabilization_window_seconds = 300
      period_seconds               = 15
      percent                      = 100
    }
  }
}

variable "secret_ref_names" {
  description = "The names of the secrets containing the DB config, initial user, and another other config."
}

variable "config_map_ref_names" {
  description = "The names of any config_maps for keycloak env variables"
  default     = []
}

variable "relative_path" {
  description = "Keycloak's http-relative-path config (was '/auth/' before version 17)"
  default     = "/"
}

variable "welcome_theme" {
  default = "keycloak"
}

variable "change_cause" {
  default = null
}

variable "resources" {
  default = {
    requests = {
      cpu    = "100m"
      memory = "750Mi"
    }
    limits = {
      #cpu    = "1"
      #memory = "1024Mi"
    }
  }
}

variable "topology_spread_constraints" {
  default = [{
    topology_key       = "kubernetes.io/hostname"
    max_skew           = 1
    when_unsatisfiable = "ScheduleAnyway"
  }]
}
