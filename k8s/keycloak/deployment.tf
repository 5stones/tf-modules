resource "kubernetes_deployment" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
    annotations = {
      "kubernetes.io/change-cause" = var.change_cause
    }
  }

  spec {
    replicas = var.autoscaling_range[0] != var.autoscaling_range[1] ? null : var.autoscaling_range[0]

    selector {
      match_labels = local.selector
    }

    template {
      metadata {
        labels      = local.labels
        annotations = var.pod_annotations
      }

      spec {
        service_account_name = kubernetes_service_account_v1.main.metadata.0.name

        container {
          name  = "keycloak"
          image = var.image
          args = concat(var.start_cmd, [
            "--spi-theme-welcome-theme=${var.welcome_theme}",
            "-Djgroups.dns.query=${var.name}-nodes.${var.namespace}.svc.cluster.local",
          ])

          port {
            name           = "http"
            container_port = 8080
          }
          port {
            name           = "jgroups"
            container_port = 7800
          }

          resources {
            requests = var.resources.requests
            limits   = var.resources.limits
          }

          dynamic "env_from" {
            for_each = local.config_map_ref_names
            content {
              config_map_ref {
                name = env_from.value
              }
            }
          }
          dynamic "env_from" {
            for_each = var.secret_ref_names
            content {
              secret_ref {
                name = env_from.value
              }
            }
          }

          liveness_probe {
            initial_delay_seconds = 60
            http_get {
              port = "http"
              path = "${var.relative_path}health/live"
            }
          }
          readiness_probe {
            http_get {
              port = "http"
              path = "${var.relative_path}health/ready"
            }
          }
        }
        dynamic "topology_spread_constraint" {
          for_each = var.topology_spread_constraints
          content {
            max_skew           = topology_spread_constraint.value.max_skew
            topology_key       = topology_spread_constraint.value.topology_key
            when_unsatisfiable = topology_spread_constraint.value.when_unsatisfiable
            label_selector {
              match_labels = { app = var.name }
            }
          }
        }
      }
    }
  }
  lifecycle {
    ignore_changes = [
      metadata[0].annotations["field.cattle.io/publicEndpoints"],
    ]
  }
}

