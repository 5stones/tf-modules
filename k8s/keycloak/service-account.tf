resource "kubernetes_service_account_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
  }
  dynamic "image_pull_secret" {
    for_each = var.image_pull_secret == "" ? [] : [true]
    content {
      name = var.image_pull_secret
    }
  }
}
