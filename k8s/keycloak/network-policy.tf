resource "kubernetes_network_policy" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
    labels    = local.labels
  }

  spec {
    pod_selector {
      match_labels = local.selector
    }

    ingress {
      ports {
        port     = "http"
        protocol = "TCP"
      }
    }

    policy_types = ["Ingress"]
  }
}
