resource "kubernetes_service" "main" {
  metadata {
    namespace = var.namespace
    name      = var.service_name
    labels    = local.labels
  }
  spec {
    selector = local.selector

    type = var.service_type
    port {
      name        = "http"
      port        = 80
      target_port = 8080
    }
  }
  lifecycle {
    ignore_changes = [metadata[0].annotations]
  }
}

resource "kubernetes_service" "headless" {
  metadata {
    namespace = var.namespace
    name      = "${var.name}-nodes"
    labels    = local.labels
  }
  spec {
    selector   = local.selector
    cluster_ip = "None"
    port {
      name = "http"
      port = 8080
    }
    port {
      name = "jgroups"
      port = 7800
    }
  }
  lifecycle {
    ignore_changes = [metadata[0].annotations]
  }
}
