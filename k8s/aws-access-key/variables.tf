variable "namespace" {
  default = "default"
}

variable "secret_name" {
}

variable "iam_name" {
}

variable "iam_policy" {
}

variable "aws_region" {
  default = ""
}

variable "iam_path" {
  default = "/"
}

variable "iam_tags" {
  default = {}
}
