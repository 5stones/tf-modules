# Create an AWS IAM user and K8s secret for accessing aws resources

```
module "access_key" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/aws-access-key"

  namespace   = "default"
  secret_name = "example-app-access-key"
  aws_region  = "us-east-1"
  iam_name    = "k8s.example-cluster.example-namespace.example-app"
  iam_policy  = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "s3:GetObject"
        ],
        "Resource": "*"
      }
    ]
  }
  POLICY
}
```

The created secret can be passed to a container to give it those permissions.

```
  env_from {
    secret_ref {
      name = module.access_key.secret_name
    }
  }
```
