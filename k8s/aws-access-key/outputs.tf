output "user" {
  value = aws_iam_user.main
}

output "secret" {
  value = kubernetes_secret.main
}

output "secret_name" {
  value = var.secret_name
}
