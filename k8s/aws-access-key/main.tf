resource "aws_iam_user" "main" {
  name = var.iam_name
  path = var.iam_path
  tags = var.iam_tags
}

resource "aws_iam_user_policy" "main" {
  name = "inline"
  user = aws_iam_user.main.name

  policy = var.iam_policy
}

resource "aws_iam_access_key" "main" {
  user = aws_iam_user.main.name
}

resource "kubernetes_secret" "main" {
  metadata {
    namespace = var.namespace
    name      = var.secret_name
  }
  data = merge({
    AWS_ACCESS_KEY_ID     = aws_iam_access_key.main.id
    AWS_SECRET_ACCESS_KEY = aws_iam_access_key.main.secret
    }, var.aws_region == "" ? {} : {
    AWS_DEFAULT_REGION = var.aws_region
    AWS_REGION         = var.aws_region
  })
}
