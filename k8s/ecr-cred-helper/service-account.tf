resource "kubernetes_service_account_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
}

resource "kubernetes_role_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
  rule {
    api_groups     = [""]
    resources      = ["secrets"]
    resource_names = [local.access_key_secret]
    verbs          = ["get", "list", "watch"]
  }
  rule {
    api_groups     = [""]
    resources      = ["secrets"]
    resource_names = [var.secret_name]
    verbs          = ["create", "get", "update", "patch"]
  }
  rule {
    api_groups     = [""]
    resources      = ["serviceaccount"]
    resource_names = ["default"]
    verbs          = ["patch"]
  }
}

resource "kubernetes_role_binding_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = var.name
  }
  subject {
    kind      = "ServiceAccount"
    name      = var.name
    namespace = "" # if namespace is left out, terraform adds "default" here
  }
}
