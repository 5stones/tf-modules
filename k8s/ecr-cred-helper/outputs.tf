output "secret_name" {
  value = var.secret_name
}

output "secret" {
  value = kubernetes_secret_v1.main
}

output "cron_job" {
  value = kubernetes_cron_job_v1.main
}
