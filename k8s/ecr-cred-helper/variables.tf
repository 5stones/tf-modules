variable "namespace" {
  default = "default"
}

variable "name" {
  default = "ecr-cred-helper"
}

variable "schedule" {
  default = "0 */6 * * *"
}

variable "image" {
  description = "An image with kubectl and the aws cli"
  default     = "odaniait/aws-kubectl:latest"
}

variable "secret_name" {
  default = "ecr-login"
}

variable "aws_region" {
}

variable "aws_iam_user" {
  description = "The name of the iam user to create"
}
