locals {
  access_key_secret = "${var.name}-access-key"
  env = {
    NAMESPACE   = var.namespace
    SECRET_NAME = var.secret_name
  }
  script = <<-SCRIPT
  AWS_ACCOUNT=$(aws sts get-caller-identity --query 'Account' --output text)
  SERVER="https://$AWS_ACCOUNT.dkr.ecr.$AWS_REGION.amazonaws.com"
  TOKEN=$(aws ecr get-login-password)
  kubectl -n "$NAMESPACE" create secret docker-registry "$SECRET_NAME" --dry-run=client -o yaml \
    --docker-server="$SERVER" \
    --docker-username=AWS \
    --docker-password="$TOKEN" \
    | kubectl -n "$NAMESPACE" apply -f -
  echo "docker-registry secret $SECRET_NAME updated"
  SCRIPT
}

resource "kubernetes_secret_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.secret_name
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = "{}"
  }
  lifecycle {
    ignore_changes = [data]
  }
}

resource "kubernetes_cron_job_v1" "main" {
  metadata {
    namespace = var.namespace
    name      = var.name
  }
  spec {
    schedule                  = var.schedule
    concurrency_policy        = "Replace"
    starting_deadline_seconds = 3600

    job_template {
      metadata {}
      spec {
        template {
          metadata {}
          spec {
            service_account_name = kubernetes_service_account_v1.main.metadata.0.name
            security_context {
              run_as_non_root = true
              run_as_user     = 100
              seccomp_profile {
                type = "RuntimeDefault"
              }
            }
            dns_policy = "Default"
            container {
              name              = "job"
              image             = var.image
              image_pull_policy = "IfNotPresent"
              dynamic "env" {
                for_each = local.env
                content {
                  name  = env.key
                  value = env.value
                }
              }
              env_from {
                secret_ref {
                  name = local.access_key_secret
                }
              }
              command = ["/bin/sh", "-c", local.script]
              security_context {
                allow_privilege_escalation = false
                run_as_non_root            = true
                capabilities {
                  drop = ["ALL"]
                }
              }
            }
            restart_policy = "Never"
          }
        }
      }
    }
  }
}

module "access_key" {
  source = "../aws-access-key"

  namespace   = var.namespace
  secret_name = local.access_key_secret
  aws_region  = var.aws_region
  iam_name    = var.aws_iam_user
  iam_policy  = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:GetAuthorizationToken",
          "ecr:GetDownloadUrlForLayer"
        ],
        "Resource": "*"
      }
    ]
  }
  POLICY
}
