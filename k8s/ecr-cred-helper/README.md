# Maintain a docker registry secret for AWS ECR with a cronjob

This module allows pods to pull images from ECR based on the blog post
[How to configure and use AWS ECR with kubernetes & Rancher2.0](https://medium.com/@damitj07/how-to-configure-and-use-aws-ecr-with-kubernetes-rancher2-0-6144c626d42c).

```
module "ecr-cred-helper" {
  source = "git::https://gitlab.com/5stones/tf-modules//k8s/ecr-cred-helper"

  namespace    = "some-namespace"
  aws_iam_user = "k8s.cluster-name.some-namespace.ecr-cred-helper"
  aws_region   = "us-east-1"
}
```

Image pull secrets will need to be added to the service account for the pods.
That can be done with this command for the default serviceaccount, or if using the k8s/app module, passed as a variable.

```bash
kubectl -n "$NAMESPACE" patch serviceaccount default -p '{"imagePullSecrets":[{"name":"ecr-login"}]}'
```

The helper runs via a cron schedule. If you want to run it manually for an
initial token generation you can run the following:

```bash
kubectl config use-context [your context]
kubectl create job --from=cronjob/ecr-cred-helper ecr-cred-helper-initial -n [namespace]
```
