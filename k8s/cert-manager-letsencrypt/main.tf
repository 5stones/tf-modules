resource "kubernetes_manifest" "issuers" {
  for_each = {
    letsencrypt         = "https://acme-v02.api.letsencrypt.org"
    letsencrypt-staging = "https://acme-staging-v02.api.letsencrypt.org"
  }
  manifest = yamldecode(<<-YAML
    apiVersion: cert-manager.io/v1
    kind: Issuer
    metadata:
      name: ${each.key}
      namespace: ${var.namespace}
      labels:
        app.kubernetes.io/managed-by: terraform
    spec:
      acme:
        email: ${var.email}
        preferredChain: ""
        privateKeySecretRef:
          name: ${each.key}-account-key
        server: ${each.value}/directory
        solvers:
          - http01:
              ingress: {}
    YAML
  )
}

resource "kubernetes_network_policy" "solver" {
  metadata {
    namespace = var.namespace
    name      = "cert-manager-solver"
    labels = {
      "app.kubernetes.io/managed-by" = "terraform"
    }
  }
  spec {
    pod_selector {
      match_labels = {
        "acme.cert-manager.io/http01-solver" = "true"
      }
    }
    ingress {
      ports {
        port     = "http"
        protocol = "TCP"
      }
    }
    policy_types = ["Ingress"]
  }
}
