resource "kubernetes_deployment" "main" {
  metadata {
    namespace = var.provisioner_namespace
    name      = "efs-provisioner"
    labels = {
      app = "efs-provisioner"
    }
  }

  spec {
    replicas = 1

    strategy {
      type = "Recreate"
    }

    selector {
      match_labels = {
        app = "efs-provisioner"
      }
    }

    template {
      metadata {
        labels = {
          app = "efs-provisioner"
        }
      }

      spec {
        service_account_name = "efs-provisioner"

        #automount_service_account_token = true
        container {
          image = "quay.io/external_storage/efs-provisioner:${var.version}"
          name  = "efs-provisioner"

          env {
            name  = "FILE_SYSTEM_ID"
            value = var.efs_id
          }
          env {
            name  = "AWS_REGION"
            value = var.aws_region
          }
          env {
            name  = "PROVISIONER_NAME"
            value = var.provisioner_name
          }

          volume_mount {
            name       = "pv-volume"
            mount_path = "/persistentvolumes"
          }
        }

        volume {
          name = "pv-volume"
          nfs {
            server = "${var.efs_id}.efs.${var.aws_region}.amazonaws.com"
            path   = var.path
          }
        }
      }
    }
  }

  lifecycle {
    # strategy is currently broken in terraform
    ignore_changes = ["spec.0.strategy"]
  }
}

resource "kubernetes_storage_class" "main" {
  metadata {
    name = "aws-efs"
  }
  storage_provisioner = var.provisioner_name
}

