variable "namespace" {
  description = "The namespace to create the persistent volume claim in"
  default = "default"
}

variable "name" {
  description = "The name of the persistent volume claim"
  default = "efs"
}
