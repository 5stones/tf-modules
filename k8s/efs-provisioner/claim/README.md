Creates a Persistent Volumes Claim in a Namespace.

In the pod spec, you would define a persistent volume with
```
  volumes {
    name = "${var.pod_volume_name}"
    persistent_volume_claim {
      claim_name = "${module.claim.name}"
    }
  }
```
