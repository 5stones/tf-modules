resource "kubernetes_persistent_volume_claim" "main" {
  metadata {
    namespace = "${var.namespace}"
    name = "${var.name}"
  }
  spec {
    # choosing a volume name breaks the mount
    #volume_name = "${var.namespace}"
    storage_class_name = "aws-efs"
    access_modes = ["ReadWriteMany"]
    resources {
      requests {
        storage = "1Mi"
      }
    }
  }
}
