resource "kubernetes_service_account" "main" {
  metadata {
    namespace = var.provisioner_namespace
    name      = "efs-provisioner"
    labels = {
      app = "efs-provisioner"
    }
  }

  lifecycle {
    # secrets are auto created
    ignore_changes = [secret]
  }
}

# FIXME cluster roles aren't released yet and has to be created manually with cluster-role.yaml
resource "null_resource" "kubernetes_cluster_role" {
  triggers = {
    content = var.cluster
  }

  provisioner "local-exec" {
    command = "kubectl apply --context=${var.cluster} -f ${path.module}/cluster-role.yaml"
  }

  provisioner "local-exec" {
    command = "kubectl delete --context=${var.cluster} -f ${path.module}/cluster-role.yaml"
    when    = destroy
  }
}

#resource "kubernetes_cluster_role" "main" {
#  metadata {
#    name = "efs-provisioner"
#    labels {
#      app = "efs-provisioner"
#    }
#  }
#
#  rule {
#    api_groups = [""]
#    resources = ["persistentvolumes"]
#    verbs = ["get", "list", "watch", "create", "delete"]
#  }
#  rule {
#    api_groups = [""]
#    resources = ["persistentvolumeclaims"]
#    verbs = ["get", "list", "watch", "update"]
#  }
#  rule {
#    api_groups = ["storage.k8s.io"]
#    resources = ["storageclasses"]
#    verbs = ["get", "list", "watch"]
#  }
#  rule {
#    api_groups = [""]
#    resources = ["events"]
#    verbs = ["list", "watch", "create", "update", "patch"]
#  }
#  rule {
#    api_groups = [""]
#    resources = ["endpoints"]
#    verbs = ["get", "list", "watch", "create", "update", "patch"]
#  }
#}

resource "kubernetes_cluster_role_binding" "main" {
  metadata {
    name = "efs-provisioner"
  }

  subject {
    kind      = "ServiceAccount"
    namespace = var.provisioner_namespace
    name      = "efs-provisioner"
    api_group = ""
  }

  role_ref {
    kind      = "ClusterRole"
    name      = "efs-provisioner"
    api_group = "rbac.authorization.k8s.io"
  }
}

