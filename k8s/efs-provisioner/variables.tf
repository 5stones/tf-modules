variable "efs_id" {
  description = "The id of an existing efs volume"
}

variable "cluster" {
  description = "The K8s cluster to add the cluster-role to"
}

variable "path" {
  description = "Base EFS folder"
  default     = "/"
}

variable "aws_region" {
  description = "The region of the efs volume"
  default     = "us-east-1"
}

variable "provisioner_namespace" {
  description = "The namespace to run the provisioner in"
  default     = "kube-system"
}

variable "version" {
  default = "latest"
}

variable "provisioner_name" {
  default = "quay.io/aws-efs"
}

